
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ChangePass</title>

        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">


        <link rel="stylesheet" href="./css/login.css">
      
    </head>

    <body >
        
        
        
        <section>
            <div  class="form-box">
                <div style="padding: 30px;text-align:  center" class="form-value">
                    <form   action="changePass" method="post">
                        <h2>ChangePass</h2>

                        <div class="inputbox"> <ion-icon name="mail-outline"></ion-icon> 
                            <input type="text" name="username" readonly value="${user}" required>
                            <label>UserName</label>
                        </div>
                            
                            <div class="inputbox"> <ion-icon name="mail-outline"></ion-icon> 
                            <input  type="password" value="${param.password}" name="password"  required>
                            <label>Password</label>
                        </div>


                           
                            
                        <div style="border-bottom:none;display: flex" class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <img id="captcha" style="width: 256px" class="captcha" alt="captcha" src="/swp391/captcha">
                            <button id="reloadCaptcha" class="reload-btn" style="height: 44px; width: 44px; margin-top: 8px ;margin-left: 8px"><i class="fas fa-redo-alt"></i></button>
                        </div>


                        <div class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <input  type="text" name="captcha" > 
                            <label>Input captcha code</label>
                        </div>
                            
                            
                        <h3 id="error" style="color : red ; margin-bottom: 10px"> ${error} </h3>
                        
                        <button id="dmm" type="submit">Confirm </button>


                    </form>
                </div>
            </div>
        </section>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.3.js"
        integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>


        <script>







            $(document).ready(function () {
                // Gắn sự kiện click cho nút reload captcha
                $('#reloadCaptcha').click(function (e) {
                    e.preventDefault(); // Ngăn chặn hành động mặc định của nút
                    reloadCaptcha();
                });

                // Hàm để tải lại captcha bằng AJAX
                function reloadCaptcha() {
                    // Gửi yêu cầu AJAX để tải lại captcha
                    $.ajax({
                        type: 'GET',
                        url: '/swp391/captcha', // Đường dẫn tới endpoint để lấy captcha mới
                        success: function (data) {
                            // Cập nhật hình ảnh captcha với dữ liệu mới

                            var newImg = new Image();
                            // Đặt src của hình ảnh mới bằng đường dẫn tới endpoint để lấy captcha mới
                            newImg.src = '/swp391/captcha?' + new Date().getTime(); // Thêm timestamp để tránh cache
                            // Gán hình ảnh mới cho src của hình ảnh hiện tại
                            $('.captcha').attr('src', newImg.src);



                        },
                        error: function (xhr, status, error) {
                            // Xử lý lỗi nếu có
                            console.error('Lỗi tải lại captcha:', error);
                        }
                    });
                }
            });


        </script>
        
       
        <body>

</html>
