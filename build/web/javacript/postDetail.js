
document.addEventListener("DOMContentLoaded", function () {
    const commentsContainer = document.getElementById('comments');
    const commentOptions = document.getElementById('comment-options');
    const comments = document.querySelectorAll('.comment');

    // Function to toggle visibility of comments
    function toggleComments(commentsToShow) {
        let index = 0;
        comments.forEach((comment) => {
            if (index < commentsToShow) {
                comment.classList.remove('hidden');
            } else {
                comment.classList.add('hidden');
            }
            index++;
        });
    }

    // Initial toggle based on default option
    toggleComments(2); // Start with 2 comments shown

    // Event listener for select option change
    commentOptions.addEventListener('change', () => {
        let selectedOption = commentOptions.value;
        if (selectedOption === 'all') {
            toggleComments(comments.length);
        } else {
            let numToShow = parseInt(selectedOption);
            toggleComments(numToShow);
        }
    });

    // Event listener for "Show more" options
    commentOptions.addEventListener('click', (event) => {
        if (event.target.tagName === 'OPTION') {
            let currentShown = parseInt(event.target.value);
            let numToShow = currentShown + parseInt(event.target.value);
            toggleComments(numToShow);
        }
    });

});





document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.slider_post').forEach((slider, index) => {
        let currentIndex = 0; // Khởi tạo chỉ số hiện tại cho mỗi slider

        const slides = slider.querySelectorAll('.slide_post');
        const counter = slider.querySelector('.counter_post');
        const total = slides.length;
        const updateCounter = () => {
            counter.textContent = `${currentIndex + 1}/${total}`;
        };

        const showSlide = (index) => {
            if (index >= total)
                currentIndex = 0;
            if (index < 0)
                currentIndex = total - 1;
            slider.querySelector('.slides_post').style.transform = `translateX(${-currentIndex * 100}%)`;
            updateCounter();
        };

        slider.querySelector('#next_post').addEventListener('click', () => {
            currentIndex++;
            showSlide(currentIndex);
        });

        slider.querySelector('#prev_post').addEventListener('click', () => {
            currentIndex--;
            showSlide(currentIndex);
        });

        updateCounter(); // Cập nhật bộ đếm khi trang tải xong cho mỗi slider
    });
});




const heartIcons = document.querySelector('#heartIcon');
const bookmarkIcons = document.querySelector('#bookmarkIcon');
heartIcons.addEventListener('click', function () {
    const heartIconInner = heartIcons.querySelector('i');

    // Kiểm tra và chuyển đổi class
    if (heartIconInner.classList.contains('uil-heart')) {
        heartIconInner.classList.remove('uil-heart');
        heartIconInner.classList.add('fa', 'fa-solid', 'fa-heart');
    } else {
        heartIconInner.classList.remove('fa', 'fa-solid', 'fa-heart');
        heartIconInner.classList.add('uil-heart');
    }
});

bookmarkIcons.addEventListener('click', function () {
    const bookmarkIconInner = bookmarkIcons.querySelector('i');
    const bookmarkIcon = document.querySelector('.bookmark'); // Chọn thẻ bookmark
    const productId = bookmarkIcon.getAttribute('data-product-id'); // Lấy giá trị của thuộc tính data-product-id
    console.log(productId); // In ra giá trị của productId

    // Kiểm tra và chuyển đổi class
    if (bookmarkIconInner.classList.contains('uil-bookmark')) {
        bookmarkIconInner.classList.remove('uil-bookmark');
        bookmarkIconInner.classList.add('fa', 'fa-solid', 'fa-bookmark');
        createToast('success', 'Thêm vào yêu thích thành công');
        updateBookmarkStatus(productId, 'add'); // Thêm sản phẩm vào danh sách yêu thích

    } else {
        bookmarkIconInner.classList.remove('fa', 'fa-solid', 'fa-bookmark');
        bookmarkIconInner.classList.add('uil-bookmark');
        createToast('success', 'Đã xóa khỏi yêu thích');
        updateBookmarkStatus(productId, 'remove'); // Xóa sản phẩm khỏi danh sách yêu thích
    }
});


function updateBookmarkStatus(productId, action) {
    // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
    // Ví dụ:
    fetch('bookmark?action=' + action + '&productId=' + productId)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                // Xử lý kết quả trả về từ máy chủ (nếu cần)
            })
            .catch(error => {
                console.error('There was a problem with the fetch operation:', error);
            });
}








function toggleContent() {
    var content = document.querySelector('.post-content .post_caption');
    var button = document.querySelector('.post-content  button');

    if (content.classList.contains('show-all')) {
        content.classList.remove('show-all');
        button.innerText = 'Show more';
    } else {
        content.classList.add('show-all');
        button.innerText = 'Show less';
    }
}




document.addEventListener('DOMContentLoaded', function () {
    // Lấy thời gian hiện tại
    const now = new Date();

    // Lấy tất cả các phần tử chứa thời gian đăng bài
    const timePostInputs = document.querySelector('.post-info input[type="hidden"]');
    const timePostElements = document.querySelector('.post-info p');

    // Hàm xử lý thời gian đăng bài
    const updateTimeOfPost = () => {
        for (let i = 0; i < timePostInputs.length; i++) {
            const postedDate = new Date(timePostInputs[i].value); // Giả sử giá trị là thời gian hợp lệ
            const timeDifference = Math.abs(now - postedDate); // Khoảng thời gian tính bằng milliseconds
            const timeInSeconds = timeDifference / 1000; // Chuyển đổi sang giây

            if (timeInSeconds < 60) {
                timePostElements[i].innerHTML = Math.floor(timeInSeconds) + " seconds ago";
            } else if (timeInSeconds < 3600) {
                const minutes = Math.floor(timeInSeconds / 60);
                timePostElements[i].innerHTML = minutes + " minutes ago";
            } else if (timeInSeconds < 86400) {
                const hours = Math.floor(timeInSeconds / 3600);
                timePostElements[i].innerHTML = hours + " hours ago";
            } else {
                const days = Math.floor(timeInSeconds / 86400);
                timePostElements[i].innerHTML = days + " days ago";
            }
        }
    };

    // Gọi hàm để cập nhật thời gian
    updateTimeOfPost();
});




const notifications_ok = document.querySelector(".notifications_ok");

const toastDetails = {
    timer: 3000,
    success: {
        icon: 'fa-circle-check',
        text: 'Success: '// Cập nhật thành công
    },
    error: {
        icon: 'fa-circle-xmark',
        text: 'Error: ' // Cập nhật có lỗi
    },
    warning: {
        icon: 'fa-triangle-exclamation',
        text: 'Warning: ' // Cập nhật cảnh báo
    },
    info: {
        icon: 'fa-circle-info',
        text: 'Info: ' // Cập nhật thông tin
    }
};

const removeToast = (toast) => {
    toast.classList.add("hide");
    if (toast.timeoutId)
        clearTimeout(toast.timeoutId);
    setTimeout(() => toast.remove(), 500);
};

// Cập nhật hàm createToast để nhận thêm một tham số message
const createToast = (id, message) => {
    const {icon, text} = toastDetails[id];
    const toast = document.createElement("li");
    toast.className = `toast ${id}`;
    toast.innerHTML = `<div class="column">
                         <i class="fa-solid ${icon}"></i>
                         <span style="font-size:15px;color:green">${text + message}</span> <!-- Thêm message vào đây -->
                      </div>
                      <i class="fa-solid fa-xmark" onclick="removeToast(this.parentElement)"></i>`;
    notifications_ok.appendChild(toast);
    toast.timeoutId = setTimeout(() => removeToast(toast), toastDetails.timer);
};

function changeContent(message) {
    const contentConfirm = document.querySelector('.content_confirm');
    contentConfirm.textContent = message;
}



popup = document.querySelector(".popup123"),
        close = popup.querySelector(".close");
field = popup.querySelector(".field");
input = field.querySelector("input");
copy = field.querySelector("button");


popup.classList.toggle("show");

copy.onclick = () => {
    input.select(); //select input value
    if (document.execCommand("copy")) { //if the selected text copy
        field.classList.add("active");
        copy.innerText = "Copied";
        setTimeout(() => {
            window.getSelection().removeAllRanges(); //remove selection from document
            field.classList.remove("active");
            copy.innerText = "Copy";
        }, 3000);
    }
};


var shareButtons = document.querySelector('.share_media_copy');

// Duyệt qua từng button và thêm sự kiện click

shareButtons.addEventListener('click', function () {
    // Lấy phần tử có class="popup123"
    var popup = document.querySelector('.popup123');

    // Hiển thị popup
    if (popup.style.display === 'block') {
        popup.style.display = 'none';
    } else {
        popup.style.display = 'block';
    }

    var shareButton = document.querySelector('.shareBtn_product');
    if (shareButton) {
        shareButton.addEventListener('click', shareOnSocial);
    }


});

var popup1 = document.querySelector('.close');
popup1.addEventListener('click', function () {
    popup.style.display = 'none';
});



function shareOnSocial(event) {
    var postUrl = event.target.getAttribute('data-url');
    var socialNetwork = event.target.getAttribute('data-social');
    var shareUrl;

    switch (socialNetwork) {
        case 'facebook':
            shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(postUrl);
            break;
        case 'twitter':
            shareUrl = 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(postUrl);
            break;
        case 'instagram':
            // Instagram không hỗ trợ chia sẻ trực tiếp từ web thông qua URL như các mạng xã hội khác
            alert('Instagram không hỗ trợ chia sẻ trực tiếp từ trang web. Vui lòng sử dụng ứng dụng di động.');
            return;
        case 'line':
            shareUrl = 'https://lineit.line.me/share/ui?url=' + encodeURIComponent(postUrl);
            break;
        case 'telegram':
            shareUrl = 'https://telegram.me/share/url?url=' + encodeURIComponent(postUrl);
            break;
        default:
            shareUrl = '';
    }

    if (shareUrl !== '') {
        window.open(shareUrl, socialNetwork + '-share-dialog', 'width=626,height=436');
    }
}