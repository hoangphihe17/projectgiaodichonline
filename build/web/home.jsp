<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kibyhunter</title>

        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
              integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <link rel="stylesheet" href="./css/home1.css">

        <link rel="stylesheet" href="./css/styleHome.css">


        <style>



            :root {
                --dark: #34495E;
                --light: #ffffff;
                --success: #0ABF30;
                --error: #E24D4C;
                --warning: #E9BD0C;
                --info: #3498DB;
            }

            .notifications_ok {
                position: fixed;
                top: 70px;
                right: 20px;
            }
            .notifications_ok .toast {
                display: flex;
                align-items: center;
            }

            .notifications_ok .column_toast {
                display: flex;
                align-items: center;
            }
            .notifications_ok .toast {
                width: 400px;
                position: relative;
                overflow: hidden;
                list-style: none;
                border-radius: 4px;
                padding: 16px 17px;
                margin-bottom: 10px;
                background: var(--light);
                justify-content: space-between;
                animation: show_toast 0.3s ease forwards;
            }

            @keyframes show_toast {
                0% {
                    transform: translateX(100%);
                }
                40% {
                    transform: translateX(-5%);
                }
                80% {
                    transform: translateX(0%);
                }
                100% {
                    transform: translateX(-10px);
                }
            }
            .notifications_ok .toast.hide {
                animation: hide_toast 0.3s ease forwards;
            }
            @keyframes hide_toast {
                0% {
                    transform: translateX(-10px);
                }
                40% {
                    transform: translateX(0%);
                }
                80% {
                    transform: translateX(-5%);
                }
                100% {
                    transform: translateX(calc(100% + 20px));
                }
            }
            .toast::before {
                position: absolute;
                content: "";
                height: 3px;
                width: 100%;
                bottom: 0px;
                left: 0px;
                animation: progress 3s linear forwards;
            }

            @keyframes progress {
                100% {
                    width: 0%;
                }
            }
            .toast.success::before, .btn_noti#success {
                background: var(--success);
            }
            .toast.error::before, .btn_noti#error {
                background: var(--error);
            }
            .toast.warning::before, .btn_noti#warning {
                background: var(--warning);
            }
            .toast.info::before, .btn_noti#info {
                background: var(--info);
            }
            .toast .column_toast i {
                font-size: 1.75rem;
            }
            .toast.success .column_toast i {
                color: var(--success);
            }
            .toast.error .column_toast i {
                color: var(--error);
            }
            .toast.warning .column_toast i {
                color: var(--warning);
            }
            .toast.info .column_toast i {
                color: var(--info);
            }
            .toast .column_toast span {
                font-size: 1.07rem;
                margin-left: 12px;
            }
            .toast i:last-child {
                color: #aeb0d7;
                cursor: pointer;
            }
            .toast i:last-child:hover {
                color: var(--dark);
            }
            .buttons .btn_noti {
                border: none;
                outline: none;
                cursor: pointer;
                margin: 0 5px;
                color: var(--light);
                font-size: 1.2rem;
                padding: 10px 20px;
                border-radius: 4px;
            }

            @media screen and (max-width: 530px) {
                .notifications_ok {
                    width: 95%;
                }
                .notifications_ok .toast {
                    width: 100%;
                    font-size: 1rem;
                    margin-left: 20px;
                }
                .buttons .btn_noti {
                    margin: 0 1px;
                    font-size: 1.1rem;
                    padding: 8px 15px;
                }
            }





            .alert_box_123confirm,
            .show_button_123confirm{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50% , -50%);
            }
            .show_button_123confirm{
                height: 55px;
                padding: 0 30px;
                font-size: 20px;
                font-weight: 400;
                cursor: pointer;
                outline: none;
                border: none;
                color: #fff;
                line-height: 55px;
                background: #2980b9;
                border-radius: 5px;
                transition: all 0.3s ease;
            }
            .show_button_123confirm:hover{
                background: #2573a7;
            }
            .background_123confirm{
                position: absolute;
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
                background: rgba(0, 0, 0, 0.5);
                opacity: 0;
                pointer-events: none;
                transition: all 0.3s ease;
            }
            .alert_box_123confirm{
                border : 2px dashed green;
                padding: 19px;
                display: flex;
                background: rgb(255, 255, 255);
                flex-direction: column;
                align-items: center;
                text-align: center;
                max-width: 390px;
                width: 100%;
                border-radius: 5px;
                z-index: 5;
                opacity: 0;
                pointer-events: none;
                transform: translate(-50%, -50%) scale(0.97);
                transition: all 0.3s ease 0s;
            }
            .input-checkbox_123confirm:checked ~ .alert_box_123confirm{
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50% , -50%) scale(1);
            }
            .input-checkbox_123confirm:checked ~ .background_123confirm{
                opacity: 1;
                pointer-events: auto;
            }


            .input-checkbox_123confirm1:checked ~ .alert_box_123confirm{
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50% , -50%) scale(1);
            }
            .input-checkbox_123confirm1:checked ~ .background_123confirm{
                opacity: 1;
                pointer-events: auto;
            }


            .input-checkbox_123confirm:checked ~ .alert_box_123confirm {
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50%, -50%) scale(1);
            }

            .input-checkbox_123confirm:checked ~ .background_123confirm {
                opacity: 1;
                pointer-events: auto;
            }


            .input-checkbox_123confirm1:checked ~ .alert_box_123confirm {
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50%, -50%) scale(1);
            }

            .input-checkbox_123confirm1:checked ~ .background_123confirm {
                opacity: 1;
                pointer-events: auto;
            }




            .input-checkbox_123confirm{
                display: none;
            }
            .input-checkbox_123confirm1{
                display: none;
            }
            .alert_box_123confirm .icon_123confirm{
                height: 60px;
                width: 60px;
                color: #f23b26;
                border: 3px solid #f23b26;
                border-radius: 50%;
                line-height: 97px;
                font-size: 30px;
            }
            .alert_box_123confirm header{
                font-size: 22px;
                font-weight: 600;

            }
            .alert_box_123confirm p{
                font-size: 15px;
            }
            .alert_box_123confirm .btns_123confirm{
                margin-top: 10px;
            }
            .btns_123confirm label{
                width: 111px;
                display: inline-flex;
                height: 45px;

                padding: 0 30px;
                /* font-size: 20px; */
                font-weight: 400;
                cursor: pointer;
                line-height: 49px;
                outline: none;
                margin: 0 10px;
                border: none;
                color: #fff;


                border-radius : 30px;
                transition: all 0.3s ease;
            }
            .btns_123confirm label:first-child{
                background: #2980b9;
            }
            .btns_123confirm label:first-child:hover{
                background: #2573a7;
            }
            .btns_123confirm label:last-child{
                background: #f23b26;
            }
            .btns_123confirm label:last-child:hover{
                background: #d9210d;
            }







            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: "Poppins", sans-serif;
            }
            /* body {
              background: #E3F2FD;
            } */
            .chatbot-toggler {
                position: fixed;
                bottom: 30px;
                right: 35px;
                outline: none;
                border: none;
                height: 50px;
                width: 50px;
                display: flex;
                cursor: pointer;
                align-items: center;
                justify-content: center;
                border-radius: 50%;
                background: #724ae8;
                transition: all 0.2s ease;
            }
            body.show-chatbot .chatbot-toggler {
                transform: rotate(90deg);
            }
            .chatbot-toggler span {
                color: #fff;
                position: absolute;
            }
            .chatbot-toggler span:last-child,
            body.show-chatbot .chatbot-toggler span:first-child  {
                opacity: 0;
            }
            body.show-chatbot .chatbot-toggler span:last-child {
                opacity: 1;
            }
            .chatbot_ai {
                position: fixed;
                right: 35px;
                bottom: 90px;
                width: 330px;
                height: 400px;
                background: #fff;
                border-radius: 15px;
                overflow: hidden;
                opacity: 0;
                pointer-events: none;
                transform: scale(0.5);
                transform-origin: bottom right;
                box-shadow: 0 0 128px 0 rgba(0,0,0,0.1),
                    0 32px 64px -48px rgba(0,0,0,0.5);
                transition: all 0.1s ease;
            }
            body.show-chatbot .chatbot_ai {
                opacity: 1;
                pointer-events: auto;
                transform: scale(1);
            }
            .chatbot_ai header {
                padding: 16px 0;
                position: relative;
                text-align: center;
                color: #fff;
                background: #724ae8;
                box-shadow: 0 2px 10px rgba(0,0,0,0.1);
            }
            .chatbot_ai header span {
                position: absolute;
                right: 15px;
                top: 50%;
                display: none;
                cursor: pointer;
                transform: translateY(-50%);
            }
            header h2 {
                font-size: 1.4rem;
            }
            .chatbot_ai .chatbox_ai {
                overflow-y: auto;
                height: 360px;
                padding: 30px 20px 100px;
            }
            .chatbot_ai .chatbox_ai::-webkit-scrollbar,
            .chatbot_ai .chat-input_ai textarea::-webkit-scrollbar {
                width: 6px;
            }

            .chatbot_ai .chatbox_ai::-webkit-scrollbar-track,
            .chatbot_ai .chat-input_ai textarea::-webkit-scrollbar-track {
                background: #fff;
                border-radius: 25px;
            }

            .chatbot_ai .chatbox_ai::-webkit-scrollbar-thumb,
            .chatbot_ai .chat-input_ai textarea::-webkit-scrollbar-thumb {
                background: #ccc;
                border-radius: 25px;
            }

            .chatbox_ai .chat {
                display: flex;
                list-style: none;
            }
            .chatbox_ai .outgoing {
                margin: 20px 0;
                justify-content: flex-end;
            }
            .chatbox_ai .incoming span {
                width: 32px;
                height: 32px;
                color: #fff;
                cursor: default;
                text-align: center;
                line-height: 32px;
                align-self: flex-end;
                background: #724ae8;
                border-radius: 4px;
                margin: 0 10px 7px 0;
            }
            .chatbox_ai .chat p {
                white-space: pre-wrap;
                padding: 12px 16px;
                border-radius: 10px 10px 0 10px;
                max-width: 75%;
                color: #fff;
                font-size: 0.95rem;
                background: #724ae8;
            }
            .chatbox_ai .incoming p {
                border-radius: 10px 10px 10px 0;
            }
            .chatbox_ai .chat p.error {
                color: #721c24;
                background: #f8d7da;
            }
            .chatbox_ai .incoming p {
                color: #000;
                background: #f2f2f2;
            }
            .chatbot_ai .chat-input {
                display: flex;
                gap: 5px;
                position: absolute;
                bottom: 0;
                width: 100%;
                background: #fff;
                padding: 3px 20px;
                border-top: 1px solid #ddd;
            }
            .chat-input textarea {
                height: 55px;
                width: 100%;
                border: none;
                outline: none;
                resize: none;
                max-height: 180px;
                padding: 15px 15px 15px 0;
                font-size: 0.95rem;
            }
            .chat-input span {
                align-self: flex-end;
                color: #724ae8;
                cursor: pointer;
                height: 55px;
                display: flex;
                align-items: center;
                visibility: hidden;
                font-size: 1.35rem;
            }
            .chat-input textarea:valid ~ span {
                visibility: visible;
            }

            @media (max-width: 490px) {
                .chatbot-toggler {
                    right: 20px;
                    bottom: 20px;
                }
                .chatbot_ai {
                    right: 0;
                    bottom: 0;
                    height: 100%;
                    border-radius: 0;
                    width: 100%;
                }
                .chatbot_ai .chatbox_ai {
                    height: 90%;
                    padding: 25px 15px 100px;
                }
                .chatbot_ai .chat-input {
                    padding: 5px 15px;
                }
                .chatbot_ai header span {
                    display: block;
                }
            }



        </style>

    </head>

    <body class="body">
        <%--<jsp:include page="chatCommunity.jsp"></jsp:include>--%>

        <nav>
            <div class="container headd">
                <h2 class="logo">Kibyhunter Social</h2>

                <!--<div class="search-bar">-->
                <!--                    <i class="uil uil-search"> </i>
                                    <input type="search" placeholder="Search for your favourite">-->

                <jsp:include page="search.jsp"></jsp:include>

                    <!--</div>-->




                    <div style="margin-left: 50px" class="create">
                        <label class="btn btn-primary" for="create-post">Create</label>
                        <div class="profile-photo">
                            <img src="image/akaza1.jpg">
                        </div>


                          <div style="display: flex">
                        <img style="width: 30px" src="image/tien.png">
                    <c:if test="${moneyCustomer !=null}">
                        <fmt:formatNumber value="${moneyCustomer.address}" pattern="###,###.##" />
                    </c:if>
                </div>

                    </div>
                  
            </div>
        </nav>





        <!----------------main--------------->


        <main>
            <div class="container">



                <!----------------left--------------->
                <div class="left">
                    <a class="profile">

                        <div class="profile-photo">
                            <img src="image/truongcun.png">
                        </div>

                        <div class="handle">
                            <h4>Truong Cun</h4>
                            <p class="text-muted"> @Kibyhunter</p>
                        </div>
                    </a>


                    <!----------------sidebar--------------->
                    <div class="sidebar">


                        <a class="menu-item active">
                            <span> <i class="uil uil-home"></i></span>
                            <h3>Home</h3>

                        </a>



                        <a class="menu-item ">
                            <span> <i class="uil uil-compass"></i></span>
                            <h3>Explore</h3>


                        </a>



                        <a class="menu-item" id="notifications">
                            <span> <i class="uil uil-bell"><small class="notification-count">9+</small></i></span>
                            <h3>notifications</h3>


                            <div class="notification-popup">
                                <div>
                                    <div class="profile-photo">
                                        <img src="image/akaza1.jpg">
                                    </div>

                                    <div class="notification-body">
                                        <b>Akaza</b> accepted your friend request
                                        <small class="text-muted">2 hourse age</small>
                                    </div>
                                </div>

                                <div>
                                    <div class="profile-photo">
                                        <img src="image/akaza1.jpg">
                                    </div>
                                    <div class="notification-body">
                                        <b>Akaza</b> like and comment your picture
                                        <small class="text-muted">2 day age</small>
                                    </div>
                                </div>
                            </div>



                        </a>



                        <a id="messagse-notification" onclick="messagesClick()" class="menu-item ">
                            <span> <i class="uil uil-envelope-alt"> <small class="notification-count">9+</small></i></span>
                            <h3>Messages</h3>

                        </a>




                        <c:if test="${sessionScope.account !=null}">

                            <a style="  color: var(--color-dark);" class="menu-item" href="displayBookmark" target="_blank">
                                <span> <i class="uil uil-bookmark"></i></span>
                                <h3>Bookmarks</h3>

                            </a>
                        </c:if>



                        <c:if test="${sessionScope.account ==null}">

                            <a class="menu-item ">
                                <span> <i class="uil uil-bookmark"></i></span>
                                <h3>Bookmarks</h3>

                            </a>
                        </c:if>





                        <c:if test="${sessionScope.account !=null}">

                            <a style="  color: var(--color-dark);" class="menu-item" href="analyze?id=${sessionScope.account.accountid}" target="_blank">
                                <span> <i class="uil uil-chart-line"></i></span>
                                <h3>Analytic</h3>

                            </a>
                        </c:if>

                        <c:if test="${sessionScope.account ==null}">

                            <a class="menu-item" >
                                <span> <i class="uil uil-chart-line"></i></span>
                                <h3>Analytic</h3>

                            </a>
                        </c:if>



                        <a id="theme" class="menu-item">
                            <span> <i class="uil uil-palette"></i></span>
                            <h3>Theme</h3>

                        </a>



                        <a class="menu-item ">
                            <span> <i class="uil uil-setting"></i></span>
                            <h3>Setting</h3>

                        </a>

                    </div>

                    <label class="btn btn-primary" for="create post">Create post </label>

                </div>



                <!----------------middle--------------->

                <div class="middle">





                    <!----------------story--------------->
                    <div class="stories">
                        <div class="story">
                            <div class="profile-photo">
                                <img style=" border-style: solid;border-radius: 50%;border-color: rgb(61, 61, 210)"
                                     src="./image/truongcun.png">

                            </div>
                            <p class="name">Your Story</p>
                        </div>

                        <div class="story">
                            <div class="profile-photo">
                                <img style=" border-style: solid;border-radius: 50%;border-color: rgb(61, 61, 210)"
                                     src="./image/phuc.png">

                            </div>
                            <p class="name">Nguyễn Phúc</p>
                        </div>

                        <div class="story">
                            <div class="profile-photo">
                                <img style=" border-style: solid;border-radius: 50%;border-color: rgb(61, 61, 210)"
                                     src="./image/khang.png">

                            </div>
                            <p class="name">Khang Nguyen</p>
                        </div>

                        <div class="story">
                            <div class="profile-photo">
                                <img style=" border-style: solid;border-radius: 50%;border-color: rgb(61, 61, 210)"
                                     src="./image/hai.jpg">

                            </div>
                            <p class="name">Minh Hai</p>
                        </div>



                    </div>
                    <!----------------end òf the story--------------->
                    <form class="create-post">
                        <div class="profile-photo">
                            <img src="image/akaza1.jpg">
                        </div>
                        <input id="create-post" type="text" placeholder="What's on your mind ?">
                        <input type="submit" value="Post" class="btn btn-primary">
                    </form>






                    <!----------------news feed--------------->

                    <ul class="notifications_ok"></ul>
                    <div class="buttons_ok">
                        <button class="btn_noti" id="success" onclick="createToast_123('success', 'Xin chào')">Success</button>
                        <button class="btn_noti" id="error" onclick="createToast_123('error', 'Có lỗi')">Error</button>
                        <button class="btn_noti" id="warning" onclick="createToast_123('warning', 'Cảnh báo')">Warning</button>
                        <button class="btn_noti" id="info" onclick="createToast_123('info', 'Thông tin')">Info</button>
                    </div>

                    <div  class="feeds">


                        <c:forEach  items="${listFeed}" var="f" >

                            <div   class="feed"   id="feed-${f.id}">


                                <div class="head">
                                    <div class="user">
                                        <div class="profile-photo">
                                            <img src="${f.accountId.avatar}">


                                        </div>

                                        <div class="ingo">
                                            <h3>${f.accountId.userName}</h3>
                                            <small>Vietnam ,</small>
                                            <small class="dumaaa">${f.createAt}</small>

                                            <input type="hidden" value="${f.createAt}">

                                        </div>

                                    </div>
                                    <span style="position: relative"  id="editdm" class="edit">
                                        <i class="uil uil-ellipsis-h"></i>



                                        <div style="z-index: 1;" class="wrapper_111">
                                            <div class="content_111">
                                                <ul class="menu">
                                                    <li class="item">
                                                        <i class="uil uil-eye"></i>
                                                        <span>Preview</span>
                                                    </li>
                                                    <li class="item share">
                                                        <div>
                                                            <i class="uil uil-share"></i>
                                                            <span>Share</span>
                                                        </div>
                                                        <i class="uil uil-angle-right"></i>
                                                        <ul class="share-menu">
                                                            <li class="item">
                                                                <i class="uil uil-twitter-alt"></i>
                                                                <span>Twitter</span>
                                                            </li>
                                                            <li class="item">
                                                                <i class="uil uil-instagram"></i>
                                                                <span>Instagram</span>
                                                            </li>
                                                            <li class="item">
                                                                <i class="uil uil-dribbble"></i>
                                                                <span>Dribble</span>
                                                            </li>
                                                            <li class="item">
                                                                <i class="uil uil-telegram-alt"></i>
                                                                <span>Telegram</span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <input hidden name="copyProduct" value="http://localhost:9999/swp391/postDetail?id=${f.id}">
                                                    <li onclick="createToast_123('success', 'Sao chép link thành công!.')" class="item copyProduct">
                                                        <i class="uil uil-link-alt"></i>
                                                        <span>Get Link</span>
                                                    </li>
                                                    <li  class="item editPost">
                                                        <i class="uil uil-edit"></i>
                                                        <span>Edit</span>
                                                    </li>


                                                    <label for="check1_${f.id}">
                                                        <li class="item">
                                                            <i class="uil uil-trash-alt"></i>
                                                            <span>Delete</span>
                                                        </li>

                                                    </label>


                                                </ul>
                                                <div class="setting">
                                                    <li class="item">
                                                        <i class="uil uil-setting"></i>
                                                        <span>Settings</span>
                                                    </li>
                                                </div>
                                            </div>
                                        </div>
                                    </span>


                                </div>



                                <div class="slider_post">
                                    <div class="slides_post">


                                        <c:forEach var="i" items="${ListImage}"  varStatus="loop">
                                            <c:if test="${i.productId.id==f.id }">

                                                <c:if test="${i.path !=null}">

                                                    <img src="${i.path}" class="slide_post">
                                                    <c:set var="imageCount" value="${loop.index + 1}" />
                                                </c:if>


                                            </c:if>
                                        </c:forEach>  

                                    </div>
                                    <c:if test="${imageCount >1}">
                                        <div class="slider-controls-post">
                                            <button id="prev_post"> <img style="border-radius: 50%; width: 40px; height: 40px;" src="image/left.jpg"> </button>  
                                            <button id="next_post"> <img style="border-radius: 50%; width: 40px; height: 40px;" src="image/right.jpg"> </button>   
                                        </div>

                                    </c:if>

                                    <div class="counter_post" id="counter_post">1/1</div> 
                                </div>



                                <div class="action-buttons">
                                    <div style="position: relative" class="interaction-buttons">

                                        <span  class="heartIcon" >
                                            <i class="uil uil-heart"></i>
                                        </span>

                                        <span>
                                            <i class="uil uil-comment-dots"></i>
                                        </span>

                                        <span class="share_media view_modal_shareP">
                                            <i class="uil uil-share-alt"></i>
                                        </span>

                                        <label for="check_${f.id}">
                                            <span >
                                                <i class="fas fa-shopping-cart cart-icon"></i>
                                            </span>

                                        </label>

                                        <span>
                                            <i style="font-weight: 100" class="fas fa-chart-bar analysis-icon"></i>

                                        </span>

                                        <!--                                        <div class="popup123">
                                                                                    <header>
                                                                                        <span>Share Product</span>
                                                                                        <div class="close"><i class="uil uil-times"></i></div>
                                                                                    </header>
                                                                                    <div class="content_123">
                                                                                        <p>Share this link via</p>
                                                                                        <ul class="icons">
                                                                                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                                                                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                                                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                                                                            <a href="#"><i class="fab fa-whatsapp"></i></a>
                                                                                            <a href="#"><i class="fab fa-telegram-plane"></i></a>
                                                                                        </ul>
                                                                                        <p>Or copy link</p>
                                                                                        <div class="field">
                                                                                            <i class="url-icon uil uil-link"></i>
                                                                                            <input type="text" readonly value="example.com/share-link">
                                                                                            <button>Copy</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->

                                        <div style="z-index: 999999999" class="popup123">
                                            <header>
                                                <span>Share Modal</span>
                                                <div class="close"><i class="uil uil-times"></i></div>
                                            </header>
                                            <div class="content_123">
                                                <p>Share this link via</p>
                                                <ul class="icons share_product">
                                                    <a class="shareBtn_product" data-url="https://www.facebook.com/kibyhunters" data-social="facebook"><i
                                                            class="fab fa-facebook-f"></i></a>
                                                    <a class="shareBtn_product" data-url="https://example.com/article" data-social="twitter"><i
                                                            class="fab fa-twitter"></i></a>
                                                    <a class="shareBtn_product" data-url="https://example.com/article" data-social="instagram"><i
                                                            class="fab fa-instagram"></i></a>
                                                    <a class="shareBtn_product" data-url="https://example.com/article" data-social="line"><i
                                                            class="fab fa-whatsapp"></i></a>
                                                    <a class="shareBtn_product" data-url="https://example.com/article" data-social="telegram"><i
                                                            class="fab fa-telegram-plane"></i></a>
                                                </ul>
                                                <p>Or copy link</p>
                                                <div class="field">
                                                    <i class="url-icon uil uil-link"></i>
                                                    <input type="text" readonly value="huhu">
                                                    <button>Copy</button>
                                                </div>
                                            </div>
                                        </div>





                                    </div>
                                    <!--favoriteProducts-->
                                    <!--                                    <div>
                                                                            <span class="bookmark"  data-product-id="${f.id}"> <i class="uil uil-bookmark"></i></span>
                                                                        </div>-->


                                    <c:set var="bookmarkDisplayed" value="false" />

                                    <c:forEach var="p" items="${favoriteProducts}">
                                        <c:if test="${f.id ==p.id}">
                                            <c:set var="bookmarkDisplayed" value="true" />

                                        </c:if>
                                    </c:forEach>

                                    <c:if test="${bookmarkDisplayed==true}">
                                        <div>
                                            <span class="bookmark" data-product-id="${f.id}"><i class="fa fa-solid fa-bookmark"></i></span>
                                        </div>
                                    </c:if>

                                    <c:if test="${bookmarkDisplayed==false}">
                                        <div>
                                            <span class="bookmark" data-product-id="${f.id}"><i class="uil uil-bookmark"></i></span>
                                        </div>
                                    </c:if>





                                </div>


                                <div class="liked-by">
                                    <span> <img src="image/phuc.png"></span>
                                    <span> <img src="image/khang.png"></span>
                                    <span> <img src="image/hai.jpg"></span>

                                    <p> Like by <b>Nguyen Phuc</b> and <b> 999 others</b></p>
                                </div>

                                <div class="caption">
                                    <div class="caption_post">

                                        <p>${f.productName}</p>
                                        <p>${f.describe}</p>
                                        <p>${f.information}</p>
                                        <p>${f.price}</p>

                                    </div>
                                    <button style="padding: 2px; border-radius: 9px;" class="toggle-btn_caption">Show More</button>
                                </div>


                                <div class="text-muted">

                                    <form id="postDetailForm" action="postDetail" method="post" target="_blank">
                                        <input type="hidden" name="id" value="${f.id}">
                                        <button type="submit" id="viewCommentsBtn" style="font-size: 12px;
                                                color: var(--color-dark);
                                                padding: 3px;
                                                width: fit-content;
                                                height: fit-content;
                                                margin-top: 6px;
                                                color: black; cursor: pointer">
                                            View all the comments
                                        </button>
                                    </form>

<!--<a  style="text-decoration: none; color: var(--color-dark); " href="postDetail?id=${f.id}" target="_blank">View all the comments </a>-->




                                </div>






                                <div id="modal-overlay-${f.id}" style="left: 29% ; top: 13% ;overflow: none" class="modal-overlay">

                                    <div class="postBox_123 modal-content">
                                        <button style="z-index: 888" class="close-btn"> <img style="width: 25px" src="image/exit.ico"> </button>

                                        <div class="container_123">
                                            <div class="wrapper_123">
                                                <section class="post_123">
                                                    <header style="width: inherit;" class="btn btn-primary">Edit Post</header>
                                                    <form action="updatePost?id=${f.id}" enctype="multipart/form-data" method="post" style="    overflow-y: scroll;

                                                          height: 550px;
                                                          margin-right: 0px;" >

                                                        <div class="content_123 helpme">
                                                            <img style="border-radius: 50%" src="${f.accountId.avatar}" alt="logo">
                                                            <div class="details_123 public_123">
                                                                <p> ${f.accountId.userName} </p>

                                                                <div class="privacy_123">

                                                                    <i class="fas fa-globe"></i>



                                                                    <span class="public_Private_Span">${f.publicPrivate}</span>


                                                                    <input type="hidden" class="public_Private_Input" name="uPublicPrivate" value="Public">
                                                                    <i class="fas fa-caret-down"></i>

                                                                </div>

                                                            </div>
                                                        </div>


                                                        <div style="margin-top: 6px;"> <strong>Title</strong> </div>

                                                        <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                                                  placeholder="Post and become a real seller" name="uTitle" spellcheck="false" required>${f.productName}</textarea>


                                                        <div style="margin-bottom: 13px;"> <strong> Fee of tranction </strong> </div>

                                                        <div class="radio-group_123">
                                                            <label>
                                                                <input type="radio" ${f.fee=='seller' ? 'checked' : ''} name="uFee" value="seller">Seller
                                                            </label>
                                                            <label>
                                                                <input type="radio" ${f.fee=='both' ? 'checked' : ''} name="uFee" value="both"> Both
                                                            </label>
                                                            <label>
                                                                <input type="radio" ${f.fee=='buyer' ? 'checked' : ''} name="uFee" value="buyer"> Buyer
                                                            </label>
                                                        </div>



                                                        <div style="margin-bottom: 6px;"> <strong> Price </strong> </div>

                                                        <textarea class="price-input" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                                                  name="uPrice" placeholder="Input your price ..." spellcheck="false" required>
                                                            ${f.price}

                                                        </textarea>


                                                        <div style="margin-bottom: 6px;"> <strong> Contact </strong> </div>

                                                        <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                                                  name="uContact" placeholder="Post and become a real seller" spellcheck="false" required>${f.information}</textarea>

                                                        <div style="margin-bottom: 6px;"> <strong> Discription </strong> </div>


                                                        <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                                                  name="uDiscription"    placeholder="Post and become a real seller" spellcheck="false" required>${f.describe}</textarea>


                                                        <div style="margin-bottom: 6px;margin-top: 10px;"> <strong> Information private </strong> </div>

                                                        <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                                                  name="uPrivateInfo"   placeholder="Post and become a real seller" spellcheck="false" required>${f.privateInfo}</textarea>


                                                        <div style="margin-bottom: 6px;"> <strong> Image discription </strong> </div>



                                                        <div class='wrapper1234 upload-container'>
                                                            <div class="upload_123">
                                                                <div class="upload-wrapper_123">

                                                                    <div class="upload-img_123">
                                                                        <!-- image here -->
                                                                        <c:forEach var="i" items="${ListImage}">
                                                                            <c:if test="${i.productId.id==f.id }">

                                                                                <c:if test="${i.path !=null}">

                                                                                    <div class="uploaded-img">
                                                                                        <img src="${i.path}">
                                                                                        <input type="hidden" name="uFileImage_normal" value="${i.path}">

                                                                                        <button style="color:yellow" type="button" class="remove-btn">
                                                                                            <i class="fas fa-times"></i>
                                                                                        </button>
                                                                                    </div>

                                                                                </c:if>

                                                                            </c:if>
                                                                        </c:forEach>  
                                                                    </div>

                                                                    <div class="upload-info_123">
                                                                        <p>
                                                                            <span class="upload-info-value_123">0</span> file(s) uploaded.
                                                                        </p>
                                                                    </div>
                                                                    <div class="upload-area_123"  >
                                                                        <div class="upload-area-img_123">
                                                                            <img style="max-width: 80px;" src="image/upload.png" alt="">
                                                                        </div>
                                                                        <p class="upload-area-text_123">Select images or <span>browse</span>.</p>
                                                                    </div>
                                                                    <input type="file" name="uFileImage" class="visually-hidden upload-input_123" multiple>
                                                                </div>
                                                            </div>
                                                        </div>








                                                        <div class="theme-emoji">
                                                            <!-- <img src="icons/theme.svg" alt="theme"> -->
                                                            <img src="icons/smile.svg" alt="smile">
                                                        </div>
                                                        <div class="options">
                                                            <p>Add to Your Post</p>
                                                            <ul class="list">
                                                                <li><img src="icons/gallery.svg" alt="gallery"></li>
                                                                <li><img src="icons/tag.svg" alt="gallery"></li>
                                                                <li><img src="icons/emoji.svg" alt="gallery"></li>
                                                                <li><img src="icons/mic.svg" alt="gallery"></li>
                                                                <li><img src="icons/more.svg" alt="gallery"></li>
                                                            </ul>
                                                        </div>
                                                        <button>Post</button>
                                                    </form>
                                                </section>
                                                <section class="audience_123">
                                                    <header>
                                                        <div class="arrow-back_123"><i class="fas fa-arrow-left"></i></div>
                                                        <p>Select Audience</p>
                                                    </header>
                                                    <div class="content_123">
                                                        <p>Who can see your post?</p>
                                                        <span>Your post will show up in News Feed, on your profile and in search results.</span>
                                                    </div>


                                                    <ul class="list_123">
                                                        <li class="active">
                                                            <div class="column_123">
                                                                <div class="icon"><i style="margin-left: 14px;
                                                                                     margin-top: 13px;" class="fas fa-globe-asia"></i></div>
                                                                <div class="details_123">
                                                                    <p>Public</p>
                                                                    <span>Anyone on or off Facebook</span>
                                                                </div>
                                                            </div>
                                                            <div class="radio_123"></div>
                                                        </li>

                                                        <li>
                                                            <div class="column_123">
                                                                <div class="icon"><i style="margin-left: 14px;
                                                                                     margin-top: 13px;" class="fas fa-user-friends"></i></div>
                                                                <div class="details_123">
                                                                    <p>Friends</p>
                                                                    <span>Your friends on Facebook</span>
                                                                </div>
                                                            </div>
                                                            <div class="radio_123"></div>
                                                        </li>

                                                        <li>
                                                            <div class="column_123">
                                                                <div class="icon"><i style="margin-left: 14px;
                                                                                     margin-top: 13px;" class="fas fa-user"></i></div>
                                                                <div class="details_123">
                                                                    <p>Specific</p>
                                                                    <span>Only show to some friends</span>
                                                                </div>
                                                            </div>
                                                            <div class="radio_123"></div>
                                                        </li>

                                                        <li>
                                                            <div class="column_123">
                                                                <div class="icon"><i style="margin-left: 14px;
                                                                                     margin-top: 13px;" class="fas fa-lock"></i></div>
                                                                <div class="details_123">
                                                                    <p>Only me</p>
                                                                    <span>Only you can see your post</span>
                                                                </div>
                                                            </div>
                                                            <div class="radio_123"></div>
                                                        </li>

                                                        <li>
                                                            <div class="column_123">
                                                                <div class="icon"><i style="margin-left: 14px;
                                                                                     margin-top: 13px;" class="fas fa-cog"></i></div>
                                                                <div class="details_123">
                                                                    <p>Custom</p>
                                                                    <span>Include and exclude friends</span>
                                                                </div>
                                                            </div>
                                                            <div class="radio_123"></div>
                                                        </li>

                                                    </ul>

                                                </section>
                                            </div>
                                        </div>


                                    </div>
                                </div>




                                <div class="container_12345_123confirm">
                                    <input type="checkbox"  id="check_${f.id}" class="input-checkbox_123confirm">
                                    <div class="background_123confirm"></div>
                                    <div  class="alert_box_123confirm">
                                        <div class="icon_123confirm">
                                            <i style="position: absolute; top: 32px; right: 176px;" class="fas fa-exclamation-triangle"></i>
                                        </div>
                                        <header style="font-weight: 700; color: black;">Confirm</header>
                                        <p style=" color: black;" class="content_confirm_123confirm">Are you sure want to buy this product?</p>
                                        <div class="btns_123confirm" style="font-size: medium;">

                                            <label for="check_${f.id}" onclick="createToast_123('success', 'Bạn đã xác nhận mua đơn hàng mã ${f.id}.')"> Okey </label>
                                            <label for="check_${f.id}">Cancel</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="container_12345_123confirm">
                                    <input type="checkbox"  id="check1_${f.id}" class="input-checkbox_123confirm1">
                                    <div class="background_123confirm"></div>
                                    <div  class="alert_box_123confirm">
                                        <div class="icon_123confirm">
                                            <i style="position: absolute; top: 32px; right: 176px;" class="fas fa-exclamation-triangle"></i>
                                        </div>
                                        <header style="font-weight: 700; color: black;">Confirm</header>
                                        <p style=" color: black;" class="content_confirm_123confirm">Are you sure want to <span style="font-weight: bolder">delete</span> this product?</p>
                                        <div class="btns_123confirm" style="font-size: medium;">
                                            <label for="check1_${f.id}" onclick="createToast_123('success', 'Bạn đã xóa thành công !')"> <a style="color: white" href="deleteProduct?id=${f.id}">Okey</a>  </label>
<!--                                            <label for="check1_${f.id}" onclick="deleteProduct(event,`${f.id}`)" class="delete-product-btn" data-product-id="${f.id}">
    <span style="color: white; cursor: pointer;">Okey</span>
</label>-->


                                            <label for="check1_${f.id}">Cancel</label>
                                        </div>
                                    </div>
                                </div>






                            </div>
                        </c:forEach>









                    </div>



                </div>


                <!----------------right--------------->

                <div class="right">

                    <div class="messages">
                        <div class="heading">
                            <h4> Messages</h4>
                            <i class="uil uil-edit"> </i>

                        </div>



                        <!----------------search Bar--------------->
                        <div class="search-bar">

                            <i class="uil uil-search"></i>
                            <input type="search" placeholder="Search messages" id="message-search">

                        </div>



                        <!----------------category --------------->

                        <div class="category">

                            <h6 class="active">Primary</h6>
                            <h6>General</h6>
                            <h6 class="message-requests">Requests(7)</h6>

                        </div>


                        <!----------------Messages --------------->



                        <c:set var="acc" value="${requestScope.account}"></c:set>
                            <div id="listMessage2Container">
                            <c:forEach var="m" items="${listMessage2}" varStatus="loopStatus">
                                <div class="message khang" onclick="sendMessageId(${m.boxChatId.id})">
                                    <div class="profile-photo">


                                        <c:choose>
                                            <c:when test="${acc.accountid != m.senderId.accountid}">
                                                <c:choose>
                                                    <c:when test="${m.senderId.accountid == m.boxChatId.user1.accountid}">
                                                        <img src="${m.boxChatId.user2.avatar}">
                                                    </c:when>
                                                    <c:when test="${m.senderId.accountid == m.boxChatId.user2.accountid}">
                                                        <img src="${m.boxChatId.user1.avatar}">
                                                    </c:when>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                            </c:otherwise>
                                        </c:choose>



                                        <div class="active"></div>
                                    </div>
                                    <div class="message-body">
                                        <h5>



                                            <c:choose>
                                                <c:when test="${acc.accountid != m.senderId.accountid}">
                                                    <c:choose>
                                                        <c:when test="${m.senderId.accountid == m.boxChatId.user1.accountid}">
                                                            ${m.boxChatId.user2.userName}

                                                        </c:when>
                                                        <c:when test="${m.senderId.accountid == m.boxChatId.user2.accountid}">
                                                            ${m.boxChatId.user1.userName}

                                                        </c:when>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                </c:otherwise>
                                            </c:choose>




                                        </h5>
                                        <p class="text-bold">  ${m.message} ${m.boxChatId.id} </p>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>




                    </div>
                    <!----------------End of the message --------------->




                    <!---------------- friend request  --------------->

                    <div class="friend-requests">
                        <h4>Requests</h4>



                        <div class="request">
                            <div class="info">

                                <div class="profile-photo">
                                    <img src="image/truongcun.png">
                                </div>

                                <div>
                                    <h5>Kibyhunter </h5>
                                    <p class="text-muted"> 999 mutual friend</p>
                                </div>

                            </div>


                            <div class="action">
                                <button class="btn btn-primary"> Accept </button>
                                <button class="btn btn-primary"> Decline </button>
                            </div>

                        </div>




                        <div class="request">
                            <div class="info">

                                <div class="profile-photo">
                                    <img src="image/akaza1.jpg">
                                </div>

                                <div>
                                    <h5>Kibyhunter</h5>
                                    <p class="text-muted"> 8 mutual friend</p>
                                </div>

                            </div>


                            <div class="action">
                                <button class="btn btn-primary"> Accept </button>
                                <button class="btn btn-primary"> Decline </button>
                            </div>

                        </div>




                    </div>




                    <!---------------- boxchat  --------------->
                    <div class="box_chat">




                        <div class="card">

                            <c:set var="printed" value="false" />
                            <c:set var="ac" value="${requestScope.account}"></c:set>

                            <c:if test="${sessionScope.boxChatId != null}">
                                <c:set var="boxChat" value="${sessionScope.boxChatId}" />

                            </c:if>



                            <div id="listMessage1_Ajax">
                                <c:forEach var="mes" items="${listMessage1}">
                                    <c:if  test="${mes.boxChatId.id ==boxChat && printed == false}">


                                        <c:set var="printed" value="true" />

                                        <div style="background: gray; position: relative;" class="card-header msg_head">
                                            <div style="display: flex;" class="d-flex bd-highlight">
                                                <div class="img_cont">



                                                    <c:if test="${mes.senderId.accountid != ac.accountid && mes.senderId.accountid == mes.boxChatId.user1.accountid}">
                                                        <img style="width: 37px;height: 37px ; border-radius: 50%;" id="action_menu_btn"

                                                             src="${mes.boxChatId.user1.avatar}" class="rounded-circle user_img">

                                                    </c:if>    


                                                    <c:if test="${mes.senderId.accountid != ac.accountid && mes.senderId.accountid == mes.boxChatId.user2.accountid}">
                                                        <img style="width: 37px;height: 37px ; border-radius: 50%;" id="action_menu_btn"

                                                             src="${mes.boxChatId.user2.avatar}" class="rounded-circle user_img">

                                                    </c:if>




                                                </div>
                                                <div style="display: flex;" class="user_info">
                                                    <span style="font-size: 15px;"> ${mes.boxChatId.user1.userName} </span>
                                                    <!-- <p>1767 Messages</p> -->


                                                </div>

                                            </div>
                                            <!-- <span ><i class="fas fa-ellipsis-v"></i></span> -->

                                            <div class="action_menu">
                                                <ul>
                                                    <li><i class="fas fa-user-circle"></i> View profile</li>
                                                    <li><i class="fas fa-users"></i> Add to close friends</li>
                                                    <li><i class="fas fa-plus"></i> Add to group</li>
                                                    <li><i class="fas fa-ban"></i> Block</li>
                                                </ul>
                                            </div>
                                            <img  class="mess_Of" style="width: 20px ;height: 20px;float: right;position: absolute;top :13px ;right: 5px;" src="image/exit.ico">

                                        </div>



                                        <div style="padding: 3px; height: 230px;overflow-y: scroll;background: lavender;" class="msg_card_body">


                                            <c:set var="customer" value="${cumtomer}"></c:set>

                                            <c:forEach var="m" items="${listMessage1}">
                                                <c:if  test="${m.boxChatId.id ==boxChat}">

                                                    <c:if  test="${m.senderId.accountid == customer}">
                                                        <div  style=" float: right;width: -webkit-fill-available;margin-left: 54%;" class="d-flex justify-content-end mb-4">
                                                            <div style="text-align: center" class="msg_cotainer_send">
                                                                <!--Ok, thank you have a good day-->

                                                                <c:if test="${m.image!=null}">
                                                                    <img style="max-height: 70px ;max-width: 70px" src="${m.image}">
                                                                </c:if>


                                                                <c:if test="${m.message!=null}">
                                                                    ${m.message}
                                                                </c:if>



                                                            <!--<span class="msg_time_send"> ${m.createAt}  </span>-->
                                                            </div>

                                                            <div style="text-align: center" class="img_cont_msg">

                                                                <c:if test="${m.senderId.accountid==ac.accountid}">
                                                                    <img src="${m.senderId.avatar}" class="rounded-circle user_img_msg">
                                                                </c:if>

                                                                <c:if test="${m.senderId.accountid != ac.accountid && m.senderId.accountid == m.boxChatId.user1.accountid}">
                                                                    <img src="${m.boxChatId.user1.avatar}" class="rounded-circle user_img_msg">
                                                                </c:if>    


                                                                <c:if test="${m.senderId.accountid != ac.accountid && m.senderId.accountid == m.boxChatId.user2.accountid}">
                                                                    <img src="${m.boxChatId.user2.avatar}" class="rounded-circle user_img_msg">
                                                                </c:if>

                                                            </div>
                                                        </div>
                                                    </c:if>



                                                    <c:if  test="${m.senderId.accountid != customer}">
                                                        <div style=" float: left;width: -webkit-fill-available" class="d-flex justify-content-end mb-4">
                                                            <div style="text-align: center" class="img_cont_msg">

                                                                <c:if test="${m.senderId.accountid==ac.accountid}">
                                                                    <img src="${m.senderId.avatar}" class="rounded-circle user_img_msg">
                                                                </c:if>

                                                                <c:if test="${m.senderId.accountid != ac.accountid && m.senderId.accountid == m.boxChatId.user1.accountid}">
                                                                    <img src="${m.boxChatId.user1.avatar}" class="rounded-circle user_img_msg">
                                                                </c:if>    


                                                                <c:if test="${m.senderId.accountid != ac.accountid && m.senderId.accountid == m.boxChatId.user2.accountid}">
                                                                    <img src="${m.boxChatId.user2.avatar}" class="rounded-circle user_img_msg">
                                                                </c:if>
                                                            </div>

                                                            <div style="text-align: center" class="msg_cotainer_send">
                                                                <!--Ok, thank you have a good day-->
                                                                <c:if test="${m.image!=null}">
                                                                    <img style="max-height: 70px ;max-width: 70px" src="${m.image}">
                                                                </c:if>


                                                                <c:if test="${m.message!=null}">
                                                                    ${m.message}
                                                                </c:if>





                                                            <!--<span class="msg_time_send"> ${m.createAt}  </span>-->
                                                            </div>

                                                        </div>
                                                    </c:if>



                                                </c:if>
                                            </c:forEach>

                                        </div>





                                        <div style="background-color: gray ;bottom: -40px;" class="card-footer">
                                            <form method="post" id="messageFormAjax" enctype="multipart/form-data" action="sentMessage?boxChat=${boxChat}">

                                                <div style="display: flex; ">
                                                    <span style="width: 34px" class="attach_btn2k3"> <i style="
                                                                                                        margin-top: 7px;
                                                                                                        padding: 5px;
                                                                                                        color: white;
                                                                                                        font-size: large;" class="fas fa-paperclip"></i></span>

                                                    <button class="remove-img-btn" style="display: none;">Remove Image</button>

                                                    <div style="position: relative;">

                                                        <div style="position: absolute;bottom:45px ;" class="upload-img-preview">

                                                        </div>
                                                        <input hidden type="file" name="messageImage" class="visually-hidden" id="upload-input1" >

                                                        <input type="text" name="messageText" class="type_msg" placeholder="Type your message...">
                                                    </div>

                                                    <span class=" send_btn_mes"><i style="
                                                                                   font-size: large;
                                                                                   color: white;
                                                                                   padding: 5px;
                                                                                   margin-left: 10px;
                                                                                   margin-top: 8px;" class="fas fa-location-arrow"></i></span>
                                                    <input id="sent_btn_mes_id" type="submit" hidden>


                                                </div>
                                            </form>

                                        </div>




                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>





                    </div>



                </div>

            </div>
        </main>

        <!-------- theme customzation ---------->
        <div class="customize-theme">
            <div class="card">

                <h2> Customize your view </h2>
                <p class="text-muted"> Manage your font size , color ,and background</p>



                <!--------font-size--------->
                <div class="font-size">
                    <h4>Font size</h4>
                    <div>
                        <h6 style="padding: 10px;">Aa</h6>
                        <div class="choose-size">

                            <span class="font-size-1"></span>
                            <span class="font-size-2 active"></span>
                            <span class="font-size-3"></span>
                            <span class="font-size-4"></span>
                            <span class="font-size-5"></span>
                        </div>

                        <h3 style="padding: 10px;"> Aa </h3>
                    </div>

                </div>

                <!--------Primary color-------->
                <div class="color">
                    <h4>Color</h4>
                    <div class="choose-color">
                        <span class="color-1 active"></span>
                        <span class="color-2"></span>
                        <span class="color-3"></span>
                        <span class="color-4"></span>
                        <span class="color-5"></span>


                    </div>
                </div>

                <!--------Background color-------->

                <div class="background">

                    <h4>Background</h4>
                    <div class="choose-bg">
                        <div class="bg-1 active">
                            <span></span>
                            <h5 for="bg-1">Light</h5>
                        </div>

                        <div class="bg-2">
                            <span></span>
                            <h5 for="bg-2">Dim</h5>
                        </div>

                        <div class="bg-3">
                            <span></span>
                            <h5 for="bg-3">Lights Out</h5>
                        </div>

                    </div>

                </div>
                <i id="dm" class="uil uil-signout"></i>

            </div>

        </div>




        <!--------post-------->


        <div style="left :29%" class="postBox">
            <div class="container123">
                <div class="wrapper123">
                    <section class="post">
                        <header class="btn btn-primary">Create Post <img class="post_of" style="width: 30px ;height: 30px;float: right;" src="image/exit.ico"></header>

                        <form id="myForm" enctype="multipart/form-data" action="createPost" method="post">
                            <div class="content">
                                <img style="border-radius: 50%;" src="image/truongcun.png" alt="logo">
                                <div class="details">
                                    <p>Vũ Trường</p>

                                    <div class="privacy"   style="background: gray">

                                        <i class="fas fa-globe"></i>

                                        <span id="publicPrivateSpan">Public</span>


                                        <input type="hidden" id="publicPrivateInput" name="publicPrivate" value="Public">

                                        <i class="fas fa-caret-down"></i>
                                    </div>

                                </div>
                            </div>



                            <div style="margin-top: 6px;"> <strong>Title</strong> </div>

                            <textarea name="name" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                      placeholder="Product name" spellcheck="false" required></textarea>


                            <div style="margin-bottom: 13px;"> <strong> Fee of tranction </strong> </div>

                            <div class="radio-group">
                                <label>
                                    <input type="radio"  name="fee" value="seller" >Seller
                                </label>
                                <label>
                                    <input type="radio" checked name="fee" value="both" > Both
                                </label>
                                <label>
                                    <input type="radio" name="fee" value="buyer" style="margin-bottom: 6px;"> Buyer

                                </label>
                                <div style="margin-bottom: 13px;">    <strong> Price </strong> </div>


                                <textarea id="numberInput_Price" name="price" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                          placeholder="Input your price ..." spellcheck="false" required></textarea>
                                <div id="errorMessage_Price" style="color: red; display: none;">Please must input number!</div>


                                <div style="margin-bottom: 6px;"> <strong> Contact </strong> </div>

                                <textarea name="info" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                          placeholder="Input your information" spellcheck="false" required></textarea>

                                <div style="margin-bottom: 6px;"> <strong> Discription </strong> </div>


                                <textarea  id="editor" name="describe" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                           placeholder="Input your information" spellcheck="false"></textarea>



                                <div style="margin-bottom: 6px;margin-top: 10px;"> <strong> Information private </strong> </div>

                                <textarea name="privateInfo" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                          placeholder="Private information" spellcheck="false" required></textarea>


                                <div style="margin-bottom: 6px;"> <strong> Image discription </strong> </div>




                                <div class='wrapper234'>
                                    <div class="upload">
                                        <div class="upload-wrapper">
                                            <div class="upload-img">
                                                <!-- image here -->
                                            </div>
                                            <div class="upload-info">
                                                <p>
                                                    <span class="upload-info-value">0</span> file(s) uploaded.
                                                </p>
                                            </div>
                                            <div class="upload-area">
                                                <div class="upload-area-img">
                                                    <img src="image/upload.png" alt="">
                                                </div>
                                                <p class="upload-area-text">Select images or <span>browse</span>.</p>
                                            </div>
                                            <input name="image" type="file" class="visually-hidden" id="upload-input" multiple>
                                        </div>
                                    </div>
                                </div>




                                <div class="theme-emoji">
                                    <!-- <img src="icons/theme.svg" alt="theme"> -->
                                    <img src="icons/smile.svg" alt="smile">
                                </div>
                                <div class="options">
                                    <p>Add to Your Post</p>
                                    <ul style="display: flex ; float: right" class="list">
                                        <li><img src="icons/gallery.svg" alt="gallery"></li>
                                        <li><img src="icons/tag.svg" alt="gallery"></li>
                                        <li><img src="icons/emoji.svg" alt="gallery"></li>
                                        <li><img src="icons/mic.svg" alt="gallery"></li>
                                        <li><img src="icons/more.svg" alt="gallery"></li>
                                    </ul>
                                </div>


                                <button type="submit" onclick="submitForm()  class ="btn btn-primary">Post</button>

                        </form>




                    </section>
                    <section class="audience">
                        <header>
                            <div class="arrow-back"><i class="fas fa-arrow-left"></i></div>
                            <p>Select Audience</p>
                        </header>
                        <div class="content">
                            <p>Who can see your post?</p>
                            <span>Your post will show up in News Feed, on your profile and in search results.</span>
                        </div>


                        <ul class="list1">
                            <li class="active">
                                <div class="column">
                                    <div style=" margin: inherit; margin-top: 14px;" class="icon"><i style="margin: inherit;
                                                                                                     margin-left: 14px;" class="fas fa-globe-asia"></i></div>
                                    <div class="details">
                                        <p>Public</p>
                                        <span>Anyone on or off Facebook</span>
                                    </div>
                                </div>
                                <div class="radio"></div>
                            </li>

                            <li>
                                <div class="column">
                                    <div class="icon"><i style="margin-top: 12px;
                                                         margin-left: 12px;"  class="fas fa-user-friends"></i></div>
                                    <div class="details">
                                        <p>Friends</p>
                                        <span>Your friends on Facebook</span>
                                    </div>
                                </div>
                                <div class="radio"></div>
                            </li>

                            <li>
                                <div class="column">
                                    <div class="icon"><i style="margin-top: 12px;
                                                         margin-left: 14px;"  class="fas fa-user"></i></div>
                                    <div class="details">
                                        <p>Specific</p>
                                        <span>Only show to some friends</span>
                                    </div>
                                </div>
                                <div class="radio"></div>
                            </li>

                            <li>
                                <div class="column">
                                    <div class="icon"><i style="margin-top: 13px;
                                                         margin-left: 14px;"  class="fas fa-lock"></i></div>
                                    <div class="details">
                                        <p>Only me</p>
                                        <span>Only you can see your post</span>
                                    </div>
                                </div>
                                <div class="radio"></div>
                            </li>

                            <li>
                                <div class="column">
                                    <div class="icon"><i style="margin-top: 13px;
                                                         margin-left: 14px;"  class="fas fa-cog"></i></div>
                                    <div class="details">
                                        <p>Custom</p>
                                        <span>Include and exclude friends</span>
                                    </div>
                                </div>
                                <div class="radio"></div>
                            </li>

                        </ul>

                    </section>
                </div>
            </div>


        </div>

        <button class="chatbot-toggler">
            <span class="material-symbols-rounded">ki</span>
            <span class="material-symbols-outlined">close</span>
        </button>
        <div class="chatbot_ai">
            <header>
                <h2>Chatbot</h2>
                <span class="close-btn material-symbols-outlined">close</span>
            </header>
            <ul class="chatbox_ai">
                <li class="chat incoming">
                    <img src="image/logoKiby.webp" style="width: 40px;
                         margin-top: 9px;
                         height: 40px;
                         margin-right: 8px;
                         border-radius: 50%;border-radius: 50% ;">
                    <!--<span >smart_toy</span>-->
                    <p>Hi there 👋<br>How can I help you today?</p>
                </li>
            </ul>
            <div class="chat-input">
                <textarea placeholder="Enter a message..." spellcheck="false" required></textarea>
                <span id="send-btn" class="material-symbols-rounded">send</span>
            </div>
        </div>










        <script src="https://cdn.ckeditor.com/ckeditor5/41.0.0/super-build/ckeditor.js"></script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.3.js"
        integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
        <script src="./javacript/index2.js"></script>
        <script src="./javacript/index1.js"></script>

        <script src="./javacript/index.js"></script>





        <script>
//function deleteProduct(event, productId) {
//    event.preventDefault(); // Ngăn chặn mọi hành động mặc định
//
//    // Tạo yêu cầu xóa sản phẩm
//  var url = 'deleteProduct?id=' + encodeURIComponent(productId);
//console.log(url);
//    fetch(url, {
//        method: 'get' // Hoặc 'POST', tùy thuộc vào API của bạn
//    })
//    .then(response => {
//        if (!response.ok) {
//            throw new Error('Network response was not ok');
//        }
//
//        const productElement = document.querySelector(`#productElementId_${productId}`);
//        if (productElement) {
//            productElement.style.display = 'none'; // Hoặc productElement.remove() để xóa hoàn toàn
//        }
//        // Hiển thị thông báo thành công
//        createToast_123('success', 'Bạn đã xóa thành công !');
//    })
//    .catch(error => {
//        console.error('Lỗi khi xóa:', error);
//        // Hiển thị thông báo lỗi
//        createToast_123('error', 'Lỗi khi xóa sản phẩm');
//    });
//}


                                    document.getElementById('viewCommentsBtn').addEventListener('click', function (event) {
                                        // Prevent the default form submission
                                        event.preventDefault();

                                        // Submit the form programmatically
                                        document.getElementById('postDetailForm').submit();

                                        // Optionally, you can close the current tab after opening the new one
                                        // window.close();
                                    });



                                    var page = 1;
                                    var loading = false;



                                    $(document).ready(function () {
                                        // Sử dụng sự kiện scroll để kiểm tra và tải thêm dữ liệu khi cần thiết
                                        $(window).scroll(function () {
                                            if (!loading && $(window).scrollTop() + $(window).height() >= $(document).height() - 8) {
                                                page++;
                                                loadMoreProducts(page);
                                            }
                                        });

                                        // Gắn sự kiện click cho tất cả các phần tử .feed hiện tại và tương lai
                                        // bằng cách sử dụng event delegation trên .feeds
                                        $('.feeds').on('click', '.feed', function () {
//                                            ============================













//                                          =====================================
                                        });
                                    });

                                    function loadMoreProducts(page) {
                                        loading = true;
                                        $.ajax({
                                            url: 'home?page=' + page,
                                            type: 'GET',
                                            success: function (response) {
                                                var newContent = $(response).find('.feeds').html();
                                                if (newContent.trim() !== "") {
                                                    $('.feeds').append(newContent);
                                                } else {
                                                    // Gỡ bỏ trình xử lý sự kiện scroll khi không còn dữ liệu mới
                                                    $(window).off('scroll'); // Gỡ bỏ sự kiện scroll
                                                }
                                                loading = false;
                                            },
                                            error: function (xhr, status, error) {
                                                console.error('Error:', error);
                                                loading = false;
                                            }
                                        });
                                    }







                                    document.querySelectorAll('.caption').forEach(caption => {
                                        const toggleBtn = caption.querySelector('.toggle-btn_caption');
                                        const captionPost = caption.querySelector('.caption_post');

                                        toggleBtn.addEventListener('click', function () {
                                            const isExpanded = captionPost.classList.contains('expanded');
                                            if (isExpanded) {
                                                captionPost.classList.remove('expanded');
                                                toggleBtn.textContent = 'Show More';
                                            } else {
                                                captionPost.classList.add('expanded');
                                                toggleBtn.textContent = 'Show Less';
                                            }
                                        });
                                    });



                                    document.addEventListener("DOMContentLoaded", function () {
                                        // Đăng ký sự kiện click cho các nút hiển thị modal
                                        document.querySelectorAll(".view_modal_shareP").forEach(function (viewBtn) {

                                            viewBtn.addEventListener('click', function () {
                                                const popup = viewBtn.closest('.popup123'); // Tìm popup gần nhất tương ứng với nút được click
                                                event.stopPropagation();
                                                popup.style.display = 'block';
                                            });
                                        });

                                        // Đăng ký sự kiện click cho các nút đóng modal
                                        document.querySelectorAll(".popup123 .close").forEach(function (closeBtn) {
                                            closeBtn.addEventListener('click', function () {
                                                const popup = closeBtn.closest('.popup123'); // Tìm popup gần nhất tương ứng với nút được click
                                                popup.style.display = 'none';

                                            });
                                        });

                                        // Đăng ký sự kiện click cho nút sao chép
                                        document.querySelectorAll(".popup123 .field button").forEach(function (copyBtn) {
                                            copyBtn.addEventListener('click', function () {
                                                const input = copyBtn.previousElementSibling; // Input ngay trước nút copy
                                                input.select();
                                                if (document.execCommand("copy")) {
                                                    const field = copyBtn.parentElement;
                                                    field.classList.add("active");
                                                    copyBtn.innerText = "Copied";
                                                    setTimeout(() => {
                                                        window.getSelection().removeAllRanges();
                                                        field.classList.remove("active");
                                                        copyBtn.innerText = "Copy";
                                                    }, 3000);
                                                }
                                            });
                                        });

                                        // Đăng ký sự kiện cho các nút chia sẻ trên mạng xã hội
                                        document.querySelectorAll('.shareBtn_product').forEach(function (button) {
                                            button.addEventListener('click', function (event) {
                                                shareOnSocial(event, button);
                                            });
                                        });

                                        function shareOnSocial(event, button) {
                                            const postUrl = button.getAttribute('data-url');
                                            const socialNetwork = button.getAttribute('data-social');
                                            let shareUrl;

                                            switch (socialNetwork) {
                                                case 'facebook':
                                                    shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(postUrl);
                                                    break;
                                                case 'twitter':
                                                    shareUrl = 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(postUrl);
                                                    break;
                                                case 'instagram':
                                                    alert('Instagram không hỗ trợ chia sẻ trực tiếp từ trang web. Vui lòng sử dụng ứng dụng di động.');
                                                    return;
                                                case 'line':
                                                    shareUrl = 'https://lineit.line.me/share/ui?url=' + encodeURIComponent(postUrl);
                                                    break;
                                                case 'telegram':
                                                    shareUrl = 'https://telegram.me/share/url?url=' + encodeURIComponent(postUrl);
                                                    break;
                                                default:
                                                    shareUrl = '';
                                            }

                                            if (shareUrl !== '') {
                                                window.open(shareUrl, socialNetwork + '-share-dialog', 'width=626,height=436');
                                            }
                                        }
                                    });






// var popup = this.closest('.feed').querySelector('.popup123');
//            popup.style.display = 'block';
//            popup.style.boxShadow = '0 0 1rem var(--color-primary) ';
//
//            // Đặt thời gian tự động tắt popup sau 4 giây
//            setTimeout(function () {
//                popup.style.boxShadow = 'none';
//            }, 4000); // 4000 milliseconds = 4 seconds
//



                                    $(document).ready(function () {
                                        $('#messageFormAjax').submit(function (event) {
                                            event.preventDefault(); // Ngăn chặn form gửi thông thường

                                            var formData = new FormData($(this)[0]); // Lấy dữ liệu từ form

                                            $.ajax({
                                                url: $(this).attr('action'),
                                                type: "post",
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                success: function (data) {
                                                    // Gửi thành công, tải lại listMessage2
                                                    loadListMessage2();
                                                    loadListMessage1();



                                                },
                                                error: function (xhr) {
                                                    // Xử lý lỗi tại đây
                                                    console.error("Error: ", xhr);
                                                }
                                            });
                                        });

                                        function loadListMessage2() {
                                            $.ajax({
                                                url: "/swp391/demo", // Thay thế url_to_load_listMessage2 bằng URL thích hợp
                                                type: "get",
                                                success: function (data) {
                                                    // Tạo một jQuery object từ dữ liệu mới
                                                    var $newListMessage2 = $(data);
                                                    // Thay đổi chỉ nội dung của #listMessage2Container bằng dữ liệu mới
                                                    $('#listMessage2Container').html($newListMessage2.find('#listMessage2Container').html());
                                                },
                                                error: function (xhr) {
                                                    // Xử lý lỗi tại đây
                                                    console.error("Error: ", xhr);
                                                }
                                            });
                                        }

                                        function loadListMessage1() {
                                            $.ajax({
                                                url: "/swp391/home", // Thay thế url_to_load_listMessage2 bằng URL thích hợp
                                                type: "get",
                                                success: function (data) {
                                                    // Tạo một jQuery object từ dữ liệu mới
                                                    var $newListMessage1 = $(data);
                                                    // Thay đổi chỉ nội dung của #listMessage2Container bằng dữ liệu mới
                                                    $('#listMessage1_Ajax').html($newListMessage1.find('#listMessage1_Ajax').html());
                                                },
                                                error: function (xhr) {
                                                    // Xử lý lỗi tại đây
                                                    console.error("Error: ", xhr);
                                                }
                                            });
                                        }


                                    });











                                    document.querySelectorAll('.price-input').forEach(function (element) {
                                        element.addEventListener('input', function (e) {
                                            var input = e.target.value;
                                            var numericInput = input.replace(/[^\d]/g, '');



                                            var formattedInput = numericInput
                                                    .split('')
                                                    .reverse()
                                                    .join('')
                                                    .replace(/\d{3}(?=\d)/g, '$&,')
                                                    .split('')
                                                    .reverse()
                                                    .join('');

                                            e.target.value = formattedInput;
                                        });
                                    });



                                    document.getElementById('numberInput_Price').addEventListener('input', function (e) {
                                        var input = e.target.value;
                                        // Loại bỏ tất cả ký tự không phải số và dấu phẩy
                                        var numericInput = input.replace(/[^\d]/g, '');

                                        // Kiểm tra nếu có ký tự không phải số đã bị loại bỏ
                                        if (numericInput === '' && input !== '' && input.replace(/,/g, '') !== '') {
                                            document.getElementById('errorMessage_Price').style.display = 'block';
                                        } else {
                                            document.getElementById('errorMessage_Price').style.display = 'none';
                                        }

                                        // Định dạng lại số với dấu phẩy sau mỗi 3 chữ số từ phải sang trái
                                        var formattedInput = numericInput
                                                .split('') // Chuyển đổi chuỗi thành mảng các ký tự
                                                .reverse() // Đảo ngược mảng để dễ dàng thêm dấu phẩy
                                                .join('') // Chuyển đổi mảng ngược lại thành chuỗi
                                                .replace(/\d{3}(?=\d)/g, '$&,') // Thêm dấu phẩy sau mỗi 3 số
                                                .split('') // Chuyển đổi chuỗi thành mảng lại lần nữa
                                                .reverse() // Đảo ngược mảng để trở lại trạng thái ban đầu
                                                .join(''); // Chuyển đổi mảng thành chuỗi

                                        e.target.value = formattedInput;
                                    });










                                    function sendMessageId(boxChatId) {
                                        $.ajax({
                                            url: "/swp391/home1",
                                            type: "post",
                                            data: {
                                                boxChat: boxChatId
                                            },
                                            success: function (data) {
                                                // Xử lý dữ liệu nhận được từ server tại đây
                                                console.log(data);
                                            },
                                            error: function (xhr) {
                                                // Xử lý lỗi tại đây
                                                console.error("Error: ", xhr);
                                            }
                                        });
                                    }






                                    const notifications_ok = document.querySelector(".notifications_ok");

                                    const toastDetails = {
                                        timer: 3000,
                                        success: {
                                            icon: 'fa-circle-check',
                                            text: 'Success: ' // Cập nhật thành công
                                        },
                                        error: {
                                            icon: 'fa-circle-xmark',
                                            text: 'Error: ' // Cập nhật có lỗi
                                        },
                                        warning: {
                                            icon: 'fa-triangle-exclamation',
                                            text: 'Warning: ' // Cập nhật cảnh báo
                                        },
                                        info: {
                                            icon: 'fa-circle-info',
                                            text: 'Info: ' // Cập nhật thông tin
                                        }
                                    };

                                    const removeToast = (toast) => {
                                        toast.classList.add("hide");
                                        if (toast.timeoutId)
                                            clearTimeout(toast.timeoutId);
                                        setTimeout(() => toast.remove(), 500);
                                    };

// Cập nhật hàm createToast để nhận thêm một tham số message
                                    const createToast_123 = (id, message) => {
                                        const {icon, text} = toastDetails[id];
                                        const toast = document.createElement("li");
                                        console.log(icon);
                                        console.log(message);
                                        console.log(text);


                                        toast.className = `toast ` + id;
                                        toast.innerHTML = '<div style="z-index:99999" class="column_toast">' +
                                                '<i class="fa-solid ' + icon + '"></i>' +
                                                '<span style="color: black">' + text + message + '</span>' + // Thêm message vào đây
                                                '</div>' +
                                                '<i class="fa-solid fa-xmark" onclick="removeToast(this.parentElement)"></i>';

                                        notifications_ok.appendChild(toast);
                                        toast.timeoutId = setTimeout(() => removeToast(toast), toastDetails.timer);
                                    };










                                    const chatbotToggler = document.querySelector(".chatbot-toggler");
                                    const closeBtn_huhu = document.querySelector(".close-btn");
                                    const chatbox_huhu = document.querySelector(".chatbox_ai");
                                    const chatInput = document.querySelector(".chat-input textarea");
                                    const sendChatBtn = document.querySelector(".chat-input span");

                                    let userMessage = null; // Variable to store user's message
                                    const API_KEY = "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"; // Paste your API key here
                                    const inputInitHeight = chatInput.scrollHeight;

                                    const createChatLi = (message, className) => {
                                        // Create a chat <li> element with passed message and className
                                        const chatLi = document.createElement("li");
                                        chatLi.classList.add("chat", className);
                                        let chatContent = className === "outgoing" ? `<p></p>` : ` <img src="image/logoKiby.webp" style="width: 40px;
                         margin-top: 9px;
                         height: 40px;
                         margin-right: 8px;
                         border-radius: 50%;border-radius: 50% ;"><p></p>`;
                                        chatLi.innerHTML = chatContent;
                                        chatLi.querySelector("p").textContent = message;
                                        return chatLi; // return chat <li> element
                                    };

                                    const generateResponse = (chatElement) => {
                                        const API_URL = "https://api.openai.com/v1/chat/completions";
                                        const messageElement = chatElement.querySelector("p");

                                        // Define the properties and message for the API request
                                        const requestOptions = {
                                            method: "POST",
                                            headers: {
                                                "Content-Type": "application/json",
                                                "Authorization": `Bearer ${API_KEY}`
                                            },
                                            body: JSON.stringify({
                                                model: "gpt-3.5-turbo",
                                                messages: [{role: "user", content: userMessage}],
                                            })
                                        };

                                        // Send POST request to API, get response and set the reponse as paragraph text
                                        fetch(API_URL, requestOptions).then(res => res.json()).then(data => {
                                            messageElement.textContent = data.choices[0].message.content.trim();
                                        }).catch(() => {
                                            messageElement.classList.add("error");
                                            messageElement.textContent = "Oops! Something went wrong. Please try again.";
                                        }).finally(() => chatbox_huhu.scrollTo(0, chatbox_huhu.scrollHeight));
                                    };

                                    const handleChat = () => {
                                        userMessage = chatInput.value.trim(); // Get user entered message and remove extra whitespace
                                        if (!userMessage)
                                            return;

                                        // Clear the input textarea and set its height to default
                                        chatInput.value = "";
                                        chatInput.style.height = `${inputInitHeight}px`;

                                        // Append the user's message to the chatbox
                                        chatbox_huhu.appendChild(createChatLi(userMessage, "outgoing"));
                                        chatbox_huhu.scrollTo(0, chatbox_huhu.scrollHeight);

                                        setTimeout(() => {
                                            // Display "Thinking..." message while waiting for the response
                                            const incomingChatLi = createChatLi("Thinking...", "incoming");
                                            chatbox_huhu.appendChild(incomingChatLi);
                                            chatbox_huhu.scrollTo(0, chatbox_huhu.scrollHeight);
                                            generateResponse(incomingChatLi);
                                        }, 600);
                                    };

                                    chatInput.addEventListener("input", () => {
                                        // Adjust the height of the input textarea based on its content
                                        chatInput.style.height = `${inputInitHeight}px`;
                                        chatInput.style.height = `${chatInput.scrollHeight}px`;
                                    });

                                    chatInput.addEventListener("keydown", (e) => {
                                        // If Enter key is pressed without Shift key and the window 
                                        // width is greater than 800px, handle the chat
                                        if (e.key === "Enter" && !e.shiftKey && window.innerWidth > 800) {
                                            e.preventDefault();
                                            handleChat();
                                        }
                                    });

                                    sendChatBtn.addEventListener("click", handleChat);
                                    closeBtn_huhu.addEventListener("click", () => document.body.classList.remove("show-chatbot"));
                                    chatbotToggler.addEventListener("click", () => document.body.classList.toggle("show-chatbot"));




                                    const editButtons = document.querySelectorAll('.editPost');

// Lặp qua từng nút Edit và thêm sự kiện click
                                    editButtons.forEach(function (btn) {
                                        btn.addEventListener('click', function () {
                                            // Tìm modal-overlay gần nhất từ nút Edit được click
                                            const modal = btn.closest('.feed').querySelector('.modal-overlay');

                                            // Hiển thị modal-overlay
                                            modal.classList.add('active');
                                            modal.style.display = 'block';

                                            // Tìm nút đóng trong modal hiện tại và thêm sự kiện click để đóng modal
                                            const close_Buttons = modal.querySelector('.close-btn');
                                            close_Buttons.addEventListener('click', function () {
                                                // Đóng modal bằng cách loại bỏ class 'active' và ẩn nó
                                                modal.classList.remove('active');
                                                modal.style.display = 'none';
                                            });
                                        });
                                    });









                                    // Function to update privacy indicator dynamically
                                    function updatePrivacyIndicator(listItem) {
                                        const feedContainer = listItem.closest('.feed');
                                        const iconElement = feedContainer.querySelector('.post_123 .privacy_123 i');
                                        const privacyText = feedContainer.querySelector('.post_123 .privacy_123 span');
                                        const activeItemIndex = Array.from(listItem.parentNode.children).indexOf(listItem);

                                        // Logic to update icon and text based on active item
                                        // This assumes the order of items reflects the desired privacy settings
                                        const privacySettings = [
                                            {icon: 'fa-globe', text: 'Public'},
                                            {icon: 'fa-user-friends', text: 'Friends'},
                                            {icon: 'fa-user', text: 'Specific'},
                                            {icon: 'fa-lock', text: 'Private'},
                                            {icon: 'fa-cog', text: 'Custom'}
                                        ];

                                        const setting = privacySettings[activeItemIndex];
                                        if (setting) {
                                            iconElement.className = 'fas ' + setting.icon; // Reset and apply new classes
                                            privacyText.innerText = setting.text;


                                            document.querySelectorAll('.helpme').forEach(function (item) {
                                                // Tìm phần tử span chứa text "Public" hoặc "Private"
                                                var spanText = item.querySelector('.public_Private_Span').innerText;

                                                // Gán giá trị của span vào thuộc tính value của input có tên "uPublicPrivate"
                                                item.querySelector('input[name="uPublicPrivate"]').value = spanText;
                                            });
                                        }
                                    }







        </script>
    </body>
</html>
