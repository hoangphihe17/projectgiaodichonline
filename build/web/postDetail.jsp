
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Detail</title>

        <link rel="stylesheet" href="css/postDetail.css" />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon-32x32.png" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <style>

            .slider_post,
            .post-container {
                animation: shadow-glow 8s linear infinite;
            }

            .post-content p {
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-line-clamp: 3;
                /* Số dòng hiển thị */
                -webkit-box-orient: vertical;
            }

            .post-content p.show-all {
                -webkit-line-clamp: unset;
                /* Hiển thị toàn bộ nội dung */
            }

            .hidden {
                display: none;

            }



            ::selection{
                color: #fff;
                background: #7d2ae8;
            }
            .view-modal, .popup123{
                border: 2px dashed lightseagreen;
                display: none;
                position: absolute;
                left: 50%;
            }
            .copy{
                outline: none;
                cursor: pointer;
                font-weight: 500;
                border-radius: 4px;
                border: 2px solid transparent;
                transition: background 0.1s linear, border-color 0.1s linear, color 0.1s linear;
            }
            .view-modal{
                top: 50%;
                color: #7d2ae8;
                font-size: 18px;
                padding: 10px 25px;
                background: #fff;
                transform: translate(-50%, -50%);
            }
            .popup123{
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
                background: #fff;
                padding: 13px;
                border-radius: 15px;
                top: -150%;
                max-width: 310px;
                width: 100%;
                opacity: 0;
                pointer-events: none;
                box-shadow: 0px 10px 15px rgba(0,0,0,0.1);
                transform: translate(-50%, -50%) scale(1.2);
                transition: top 0s 0.2s ease-in-out,
                    opacity 0.2s 0s ease-in-out,
                    transform 0.2s 0s ease-in-out;
            }
            .popup123.show{
                top: 50%;
                opacity: 1;
                pointer-events: auto;
                transform:translate(-50%, -50%) scale(1);
                transition: top 0s 0s ease-in-out,
                    opacity 0.2s 0s ease-in-out,
                    transform 0.2s 0s ease-in-out;

            }
            .popup123 :is(header, .icons, .field){
                display: flex;
                align-items: center;
                justify-content: space-between;
            }
            .popup123 header{
                padding-bottom: 5px;
                border-bottom: 1px solid #ebedf9;
            }
            header span{
                font-size: 19px;
                font-weight: 600;
            }
            header .close, .icons a{
                display: flex;
                align-items: center;
                border-radius: 50%;
                justify-content: center;
                transition: all 0.3s ease-in-out;
            }
            header .close{
                color: #878787;
                font-size: 17px;
                background: #f2f3fb;
                height: 33px;
                width: 33px;
                cursor: pointer;
            }
            header .close:hover{
                background: #ebedf9;
            }
            .popup123 .content_123
            {
                margin: 20px 0;
            }
            .popup123 .icons{
                padding: unset;
                margin: 1px;
            }
            .content_123 p{
                font-size: 13px;
            }
            .content_123 .icons a{
                height: 50px;
                width: 50px;
                font-size: 16px;
                text-decoration: none;
                border: 1px solid transparent;
            }
            .icons a i{
                transition: transform 0.3s ease-in-out;
            }
            .icons a:nth-child(1){
                color: #1877F2;
                border-color: #b7d4fb;
            }
            .icons a:nth-child(1):hover{
                background: #1877F2;
            }
            .icons a:nth-child(2){
                color: #46C1F6;
                border-color: #b6e7fc;
            }
            .icons a:nth-child(2):hover{
                background: #46C1F6;
            }
            .icons a:nth-child(3){
                color: #e1306c;
                border-color: #f5bccf;
            }
            .icons a:nth-child(3):hover{
                background: #e1306c;
            }
            .icons a:nth-child(4){
                color: #25D366;
                border-color: #bef4d2;
            }
            .icons a:nth-child(4):hover{
                background: #25D366;
            }
            .icons a:nth-child(5){
                color: #0088cc;
                border-color: #b3e6ff;
            }
            .icons a:nth-child(5):hover{
                background: #0088cc;
            }
            .icons a:hover{
                color: #fff;
                border-color: transparent;
            }
            .icons a:hover i{
                transform: scale(1.2);
            }
            .content_123 .field{
                margin: 12px 0 -5px 0;
                height: 45px;
                border-radius: 4px;
                padding: 2px 5px;
                border: 1px solid #e1e1e1;
            }
            .field.active{
                border-color: #7d2ae8;
            }
            .field i{
                width: 50px;
                font-size: 18px;
                text-align: center;
            }
            .field.active i{
                color: #7d2ae8;
            }
            .field input{
                width: 100%;
                height: 100%;
                border: none;
                outline: none;
                font-size: 15px;
            }
            .field .copy{
                color: #fff;
                padding: 5px 18px;
                background: #7d2ae8;
            }
            .field .copy:hover{
                background: #8d39fa;
            }




            .container_update {
                max-width: 600px;
                margin: 50px auto;
                background-color: #fff;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 2px 5px rgba(0,0,0,0.1);

                position: absolute;
                top: 247px;
                width: 498px;
                left: 673px;
                height: 220px;
                padding-top: 4px;
            }
            .nameUpdate {
                text-align: center;

            }
            .commentUpdate {
                font-weight: bold;
                display: block;
                margin-bottom: 5px;
            }
            .commentUpdate2 {
                width: 100%;
                padding: 10px;

                border: 1px solid #ccc;
                border-radius: 5px;
                box-sizing: border-box;
                resize: vertical;
                height: 130px;
            }
            .submitInput {
                background-color: peru;
                padding: 7px 4px;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                font-size: 16px;
                display: block;
                width: 60px;
                margin-top: 7px;
                gap: 10px;
                font-weight: bold;
                margin: 5px;
                color: white;
            }

            .submitInput:hover {
                background-color: #45a049;
            }

        </style>

    </head>



    <body>



        <div class="container_12345">
            <input type="checkbox" id="check" class="input-checkbox">
            <div class="background"></div>
            <div class="alert_box">
                <div class="icon">
                    <i style="    position: absolute;
                       top: 32px;
                       right: 178px;" class="fas fa-exclamation-triangle"></i>
                </div>
                <header style="font-weight: 700;
                        color: black;">Confirm</header>
                <p class="content_confirm">Are you sure want to permanently delete this Photo?</p>
                <div class="btns" style="font-size: medium;">
                    <label for="check" onclick="createToast('success', 'Bạn đã xác nhận mua hàng.')">Okey</label>
                    <label for="check">Cancel</label>
                </div>
            </div>
        </div>



        <div class="body-wrapper"></div>

        <!-- Overlay image Modal -->


        <!-- Header -->
        <header class="head">
            <div class="head-lft">
                <button class="head-lft__btn">
                    <div class="icon-menu"></div>
                    <img src="icons/more.svg" alt="menu image" class="head-lft__btn-img" />
                </button>
                <img src="image/logoKiby.webp" style="width: 35px;height: 35px; border-radius: 50%" alt="logo" class="head-logo" />
                <ul class="head-nav">
                    <li class="head-nav__item">Home</li>
                    <li class="head-nav__item">Bookmark</li>
                    <li class="head-nav__item">Notification</li>
                    <li class="head-nav__item">Analysis</li>
                    <li class="head-nav__item">Setting</li>

                </ul>
            </div>

            <div class="head-rgt">
                <!--                <button class="head-rgt__btn">
                                    <img src="images/icon-cart.svg" alt="cart image" class="head-cart__btn-img" />
                                </button>-->

                <img src="image/truongcun.png" alt="person image" class="head-rgt__img" />
            </div>



        </header>



        <!-- Main item container -->
        <main class="item">






            <div style="display: flex; margin: 10px;gap:5px ; padding-left: 60px ; padding-right: 60px;max-height: 595px; position: relative;"
                 class="item_1">



                <div class="popup123" style="top:13%;left: 46%; opacity: 1;z-index: 9999;">

                    <header>
                        <span>Share Product</span>
                        <div class="close"><i class="uil uil-times"></i></div>
                    </header>
                    <div class="content_123">
                        <p>Share this link via</p>
                        <ul class="icons share_product">
                            <a class="shareBtn_product" data-url="https://www.facebook.com/kibyhunters" data-social="facebook"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="twitter"><i
                                    class="fab fa-twitter"></i></a>
                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="instagram"><i
                                    class="fab fa-instagram"></i></a>
                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="line"><i
                                    class="fab fa-whatsapp"></i></a>
                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="telegram"><i
                                    class="fab fa-telegram-plane"></i></a>
                        </ul>
                        <p>Or copy link</p>
                        <div class="field">
                            <i class="url-icon uil uil-link"></i>
                            <input type="text" readonly value="http://localhost:9999/swp391/postDetail?id=${p.id}">
                            <button class="copy">Copy</button>
                        </div>
                    </div>
                </div>







                <div style="border: 2px dashed green;" class="slider_post">
                    <div style="margin: auto" class="slides_post">
                        <c:if test="${listImage!=null}">
                            <c:forEach var="img" items="${listImage}">
                                <img src="${img.path}" class="slide_post">

                            </c:forEach>
                        </c:if>
                        <!--                        <img src="image/khang.png" class="slide_post">
                                                <img src="image/truongcun.png" class="slide_post">
                                                <img src="image/feed4.jpg" class="slide_post">
                                                <img src="image/chay4.jpg" class="slide_post">-->

                    </div>
                    <div class="slider-controls-post">
                        <button id="prev_post"> <img style="border-radius: 50%; width: 40px; height: 40px;" src="image/left.jpg">
                        </button>
                        <button id="next_post"> <img style="border-radius: 50%; width: 40px; height: 40px;" src="image/right.jpg">
                        </button>
                    </div>
                    <div class="counter_post" id="counter_post">1/3</div>
                </div>




                <c:set var="f" value="${p}"></c:set>

                    <div style="border: 2px dashed green;min-width: 570px;" class="post-container">
                        <div class="post-header">
                            <img src="${f.accountId.avatar}" alt="Avatar" class="post-avatar">
                        <div class="post-info">
                            <h3 style="color: wheat;">${f.accountId.userName}</h3>
                            <p style="font-size: 9px;color:wheat">${f.createAt}</p>
                            <input type="hidden" value="${f.createAt}">
                        </div>
                    </div>
                    <div style="font-size: small;color: wheat;" class="post-content" >
                        <p class="post_caption" >  
                        <div>Title : ${f.productName}</div>                        
                        <div>Describe :${f.describe}</div>
                        <div>Information : ${f.information}</div>
                        <div>Price : ${f.price}</div>

                        </p>

                        <ul class="notifications_ok"></ul>

                        <!-- <div class="buttons_ok">
                            <button class="btn_noti" id="success" onclick="createToast('success', 'Xin chào')">Success</button>
                            <button class="btn_noti" id="error" onclick="createToast('error', 'Có lỗi')">Error</button>
                            <button class="btn_noti" id="warning" onclick="createToast('warning', 'Cảnh báo')">Warning</button>
                            <button class="btn_noti" id="info" onclick="createToast('info', 'Thông tin')">Info</button>
                          </div> -->


                        <div class="buttons">
                            <button style="padding: 4px; border-radius: 14px;background-color: antiquewhite;border: none;"
                                    onclick="toggleContent()">Show more</button>

                        </div>
                    </div>

                    <div class="post-footer">
                        <div
                            style="padding-bottom: 5px;width: -webkit-fill-available;display: flex;font-size: initial;justify-content: space-between; border-bottom: 1px solid white;"
                            class="action-buttons">
                            <div class="interaction-buttons">

                                <span id="heartIcon">
                                    <i class="uil uil-heart"></i>
                                </span>

                                <span>
                                    <i class="uil uil-comment-dots"></i>
                                </span>

                                <span style="position: relative;" class="share_media_copy">
                                    <i class="uil uil-share-alt"></i>






                                </span>

                                <label for="check">
                                    <span onclick="changeContent('Are you sure to buy this product?')">
                                        <i class="fas fa-shopping-cart cart-icon"></i>
                                    </span>
                                </label>

                                <span>
                                    <i class="fas fa-chart-bar analysis-icon"></i>

                                </span>
                            </div>

<!--                            <div id="bookmarkIcon" style="float: right;">
                                <span> <i class="uil uil-bookmark"></i></span>
                            </div>-->

                            <c:set var="bookmarkDisplayed" value="false" />
                            <c:forEach var="p" items="${favoriteProducts}">
                                <c:if test="${f.id == p.id}">
                                    <c:set var="bookmarkDisplayed" value="true" />

                                </c:if>
                                
                            </c:forEach>

                            <c:if test="${bookmarkDisplayed==true}">

                                <div id="bookmarkIcon" style="float: right;">
                                    <span class="bookmark" data-product-id="${f.id}"><i class="fa fa-solid fa-bookmark"></i></span>

                                </div>



                            </c:if>

                            <c:if test="${bookmarkDisplayed==false}">
                                <div id="bookmarkIcon" style="float: right;">

                                    <span class="bookmark" data-product-id="${f.id}"><i class="uil uil-bookmark"></i></span>
                                </div>
                            </c:if>

                        </div>










                        <form method="get" action="crudComment" style="display: flex;margin-top: 6px;justify-content: space-between;">
                            <img style="width: 33px ;height: 33px;" src="image/truongcun.png" alt="person image"
                                 class="head-rgt__img" />
                            <input style="border-radius: 15px;background-color: antiquewhite;width:84%;"
                                   placeholder="What do you think?" type="text" name="comment">
                            <input hidden
                                   placeholder="What do you think?" type="text" name="productid" value="${p.id}">

                            <button style="border-radius: 15px; padding: 4px;color: aqua;">Submit</button>
                        </form>




                        <!--overflow-y: scroll;-->

                        <div class="comment_detail_post" id="comments" style="
                             max-height: 250px;
                             margin-top: 4px;">

                            <select style="padding: 3px;border-radius: 9px;background-color: darkseagreen;" id="comment-options">
                                <option value="2" id="show-more_2">Show more 2 comments</option>
                                <option value="4" id="show-more_4">Show more 4 comments</option>
                                <option value="all" id="show-all">Show all comments</option>
                            </select>



                            <div class="wrapper">

                                <ul class="list" depth="0" style="--depth: 0;">
                                    <li  style="--nested: true;">
                                        <c:forEach var="c" items="${comment}">

                                            <div class="comment">
                                                <div class="user">
                                                    <img style="width: 25px; height: 25px; border-radius: 50%;" src="${c.accountid.avatar}">
                                                </div>
                                                <div class="">
                                                    <!-- body -->
                                                    <div class="comment__body">
                                                        <p><strong>${c.accountid.userName}</strong></p>
                                                        <p>${c.content}.</p>
                                                    </div>
                                                    <!-- actions -->
                                                    <div class="comment__actions">
                                                        <a href="#">Like</a>
                                                        <a href="#">Reply  </a>

                                                        <c:if test="${sessionScope.account.accountid == c.accountid.accountid && sessionScope.account!=null}">
                                                            <a href="#" class="update-btn" data-comment-id="${c.id}">Update</a>
                                                            <a href="crudComment?deleteComment=${c.id}&productid=${p.id}">Delete</a>
                                                        </c:if>


                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display: none; z-index: 11111" class="container_update" data-comment-id="${c.id}">

                                                <h2 class="nameUpdate">Update Comment</h2>
                                                <form action="crudComment" method="get">
                                                    <label class="commentUpdate" for="comment">Comment:</label>
                                                    <textarea  class="commentUpdate2" id="comment" name="commentUpdate" placeholder="Input your comment">${c.content}</textarea>
                                                    <input hidden name="id" value="${c.id}">
                                                    <input hidden name="productid" value="${p.id}">

                                                    <div style="display: flex">
                                                        <input class="submitInput" type="submit" value="Save">
                                                        <div style="text-align: center" class="submitInput">  Cancel </div>
                                                    </div>
                                                </form>
                                            </div>


                                        </c:forEach>






                                        </div>


                                        </div>


                                        </div>



                                        </div>






                                        </div>


                                        </main>

                                        <!-- Attribution -->
                                        <div style="text-align: center;" style="margin-bottom: 12px;" class="attribution">
                                            Copyright by
                                            <a href="#" target="_blank">Kibyhunter social </a>
                                        </div>
                                        <script defer src="javacript/postDetail.js"></script>
                                        <script>

                                        document.addEventListener('DOMContentLoaded', function () {
                                            // Lấy thời gian hiện tại
                                            const now = new Date();

                                            // Lấy phần tử chứa thời gian đăng bài
                                            const timePostInput = document.querySelector('.post-info input[type="hidden"]');
                                            const timePostElement = document.querySelector('.post-info p');

                                            // Hàm xử lý thời gian đăng bài
                                            const updateTimeOfPost = () => {
                                                const postedDate = new Date(timePostInput.value); // Giả sử giá trị là thời gian hợp lệ
                                                const timeDifference = Math.abs(now - postedDate); // Khoảng thời gian tính bằng milliseconds
                                                const timeInSeconds = timeDifference / 1000; // Chuyển đổi sang giây

                                                if (timeInSeconds < 60) {
                                                    timePostElement.innerHTML = Math.floor(timeInSeconds) + " seconds ago";
                                                } else if (timeInSeconds < 3600) {
                                                    const minutes = Math.floor(timeInSeconds / 60);
                                                    timePostElement.innerHTML = minutes + " minutes ago";
                                                } else if (timeInSeconds < 86400) {
                                                    const hours = Math.floor(timeInSeconds / 3600);
                                                    timePostElement.innerHTML = hours + " hours ago";
                                                } else {
                                                    const days = Math.floor(timeInSeconds / 86400);
                                                    timePostElement.innerHTML = days + " days ago";
                                                }
                                            };

                                            // Gọi hàm để cập nhật thời gian
                                            updateTimeOfPost();
                                        });






                                        const comment_detail_post = document.querySelectorAll(".comment_detail_post .wrapper .list .comment");
                                        const list = document.querySelector('.comment_detail_post'); // Declare this outside so it's accessible in both blocks

                                        if (list) { // Ensure the element exists
                                            if (comment_detail_post.length > 5) {
                                                list.style.overflowY = 'scroll';
                                            } else {
                                                list.style.overflowY = 'auto'; // 'auto' will only show the scrollbar when the content overflows
                                            }
                                        }




                                        document.addEventListener('DOMContentLoaded', function () {
                                            // Bắt sự kiện click cho mỗi nút "Update"
                                            document.querySelectorAll('.update-btn').forEach(function (button) {
                                                button.addEventListener('click', function (event) {
                                                    event.preventDefault();
                                                    const commentId = this.getAttribute('data-comment-id');
                                                    const containerToUpdate = document.querySelector(`.container_update[data-comment-id="` + commentId + `"]`);
                                                    if (containerToUpdate) {
                                                        containerToUpdate.style.display = 'block';
                                                    }
                                                });
                                            });

                                            // Bắt sự kiện click cho "Cancel"
                                            document.addEventListener('click', function (event) {
                                                if (event.target.matches('.submitInput') && event.target.textContent.includes('Cancel')) {
                                                    const containerToUpdate = event.target.closest('.container_update');
                                                    if (containerToUpdate) {
                                                        containerToUpdate.style.display = 'none';
                                                    }
                                                }
                                            });
                                        });


                                        </script>

                                        </body>
                                        </html>
