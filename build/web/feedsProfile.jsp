
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>feed</title>
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
              integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <link rel="stylesheet" href="./css/home1.css">

        <link rel="stylesheet" href="./css/styleHome.css">

        <style>


            :root {
                --dark: #34495E;
                --light: #ffffff;
                --success: #0ABF30;
                --error: #E24D4C;
                --warning: #E9BD0C;
                --info: #3498DB;
            }

            .notifications_ok {
                position: fixed;
                top: 70px;
                right: 20px;
            }
            .notifications_ok .toast {
                display: flex;
                align-items: center;
            }

            .notifications_ok .column_toast {
                display: flex;
                align-items: center;
            }
            .notifications_ok .toast {
                width: 400px;
                position: relative;
                overflow: hidden;
                list-style: none;
                border-radius: 4px;
                padding: 16px 17px;
                margin-bottom: 10px;
                background: var(--light);
                justify-content: space-between;
                animation: show_toast 0.3s ease forwards;
            }

            @keyframes show_toast {
                0% {
                    transform: translateX(100%);
                }
                40% {
                    transform: translateX(-5%);
                }
                80% {
                    transform: translateX(0%);
                }
                100% {
                    transform: translateX(-10px);
                }
            }
            .notifications_ok .toast.hide {
                animation: hide_toast 0.3s ease forwards;
            }
            @keyframes hide_toast {
                0% {
                    transform: translateX(-10px);
                }
                40% {
                    transform: translateX(0%);
                }
                80% {
                    transform: translateX(-5%);
                }
                100% {
                    transform: translateX(calc(100% + 20px));
                }
            }
            .toast::before {
                position: absolute;
                content: "";
                height: 3px;
                width: 100%;
                bottom: 0px;
                left: 0px;
                animation: progress 3s linear forwards;
            }

            @keyframes progress {
                100% {
                    width: 0%;
                }
            }
            .toast.success::before, .btn_noti#success {
                background: var(--success);
            }
            .toast.error::before, .btn_noti#error {
                background: var(--error);
            }
            .toast.warning::before, .btn_noti#warning {
                background: var(--warning);
            }
            .toast.info::before, .btn_noti#info {
                background: var(--info);
            }
            .toast .column_toast i {
                font-size: 1.75rem;
            }
            .toast.success .column_toast i {
                color: var(--success);
            }
            .toast.error .column_toast i {
                color: var(--error);
            }
            .toast.warning .column_toast i {
                color: var(--warning);
            }
            .toast.info .column_toast i {
                color: var(--info);
            }
            .toast .column_toast span {
                font-size: 1.07rem;
                margin-left: 12px;
            }
            .toast i:last-child {
                color: #aeb0d7;
                cursor: pointer;
            }
            .toast i:last-child:hover {
                color: var(--dark);
            }
            .buttons .btn_noti {
                border: none;
                outline: none;
                cursor: pointer;
                margin: 0 5px;
                color: var(--light);
                font-size: 1.2rem;
                padding: 10px 20px;
                border-radius: 4px;
            }

            @media screen and (max-width: 530px) {
                .notifications_ok {
                    width: 95%;
                }
                .notifications_ok .toast {
                    width: 100%;
                    font-size: 1rem;
                    margin-left: 20px;
                }
                .buttons .btn_noti {
                    margin: 0 1px;
                    font-size: 1.1rem;
                    padding: 8px 15px;
                }
            }





            .alert_box_123confirm,
            .show_button_123confirm{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50% , -50%);
            }
            .show_button_123confirm{
                height: 55px;
                padding: 0 30px;
                font-size: 20px;
                font-weight: 400;
                cursor: pointer;
                outline: none;
                border: none;
                color: #fff;
                line-height: 55px;
                background: #2980b9;
                border-radius: 5px;
                transition: all 0.3s ease;
            }
            .show_button_123confirm:hover{
                background: #2573a7;
            }
            .background_123confirm{
                position: absolute;
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
                background: rgba(0, 0, 0, 0.5);
                opacity: 0;
                pointer-events: none;
                transition: all 0.3s ease;
            }
            .alert_box_123confirm{
                border : 2px dashed green;
                padding: 19px;
                display: flex;
                background: rgb(255, 255, 255);
                flex-direction: column;
                align-items: center;
                text-align: center;
                max-width: 390px;
                width: 100%;
                border-radius: 5px;
                z-index: 5;
                opacity: 0;
                pointer-events: none;
                transform: translate(-50%, -50%) scale(0.97);
                transition: all 0.3s ease 0s;
            }
            .input-checkbox_123confirm:checked ~ .alert_box_123confirm{
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50% , -50%) scale(1);
            }
            .input-checkbox_123confirm:checked ~ .background_123confirm{
                opacity: 1;
                pointer-events: auto;
            }


            .input-checkbox_123confirm1:checked ~ .alert_box_123confirm{
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50% , -50%) scale(1);
            }
            .input-checkbox_123confirm1:checked ~ .background_123confirm{
                opacity: 1;
                pointer-events: auto;
            }


            .input-checkbox_123confirm:checked ~ .alert_box_123confirm {
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50%, -50%) scale(1);
            }

            .input-checkbox_123confirm:checked ~ .background_123confirm {
                opacity: 1;
                pointer-events: auto;
            }


            .input-checkbox_123confirm1:checked ~ .alert_box_123confirm {
                opacity: 1;
                pointer-events: auto;
                transform: translate(-50%, -50%) scale(1);
            }

            .input-checkbox_123confirm1:checked ~ .background_123confirm {
                opacity: 1;
                pointer-events: auto;
            }




            .input-checkbox_123confirm{
                display: none;
            }
            .input-checkbox_123confirm1{
                display: none;
            }
            .alert_box_123confirm .icon_123confirm{
                height: 60px;
                width: 60px;
                color: #f23b26;
                border: 3px solid #f23b26;
                border-radius: 50%;
                line-height: 97px;
                font-size: 30px;
            }
            .alert_box_123confirm header{
                font-size: 22px;
                font-weight: 600;

            }
            .alert_box_123confirm p{
                font-size: 15px;
            }
            .alert_box_123confirm .btns_123confirm{
                margin-top: 10px;
            }
            .btns_123confirm label{
                width: 111px;
                display: inline-flex;
                height: 45px;

                padding: 0 30px;
                /* font-size: 20px; */
                font-weight: 400;
                cursor: pointer;
                line-height: 49px;
                outline: none;
                margin: 0 10px;
                border: none;
                color: #fff;


                border-radius : 30px;
                transition: all 0.3s ease;
            }
            .btns_123confirm label:first-child{
                background: #2980b9;
            }
            .btns_123confirm label:first-child:hover{
                background: #2573a7;
            }
            .btns_123confirm label:last-child{
                background: #f23b26;
            }
            .btns_123confirm label:last-child:hover{
                background: #d9210d;
            }







            .page-inner.no-page-title {
                padding-top: 30px;
            }

            .page-inner {
                position: relative;
                min-height: calc(100% - 56px);
                padding: 20px 30px 40px 30px;
                background: #f3f4f7;
            }

            .card.card-white {
                background-color: #fff;
                border: 1px solid transparent;
                border-radius: 4px;
                box-shadow: 0 0.05rem 0.01rem rgba(75, 75, 90, 0.075);
                padding: 25px;
            }

            .grid-margin {
                margin-bottom: 2rem;
            }

            .profile-timeline ul li .timeline-item-header {
                width: 100%;
                overflow: hidden;
            }

            .profile-timeline ul li .timeline-item-header img {
                width: 40px;
                height: 40px;
                float: left;
                margin-right: 10px;
                border-radius: 50%;
            }

            .profile-timeline ul li .timeline-item-header p {
                margin: 0;
                color: #000;
                font-weight: 500;
            }

            .profile-timeline ul li .timeline-item-header p span {
                margin: 0;
                color: #8e8e8e;
                font-weight: normal;
            }

            .profile-timeline ul li .timeline-item-header small {
                margin: 0;
                color: #8e8e8e;
            }

            .profile-timeline ul li .timeline-item-post {
                padding: 20px 0 0 0;
                position: relative;
            }

            .profile-timeline ul li .timeline-item-post>img {
                width: 100%;
            }

            .timeline-options {
                overflow: hidden;
                margin-top: 20px;
                margin-bottom: 20px;
                border-bottom: 1px solid #f1f1f1;
                padding: 10px 0 10px 0;
            }

            .timeline-options a {
                display: block;
                margin-right: 20px;
                float: left;
                color: #2b2b2b;
                text-decoration: none;
            }

            .timeline-options a i {
                margin-right: 3px;
            }

            .timeline-options a:hover {
                color: #5369f8;
            }

            .timeline-comment {
                overflow: hidden;
                margin-bottom: 10px;
                width: 100%;
                border-bottom: 1px solid #f1f1f1;
                padding-bottom: 5px;
            }

            .timeline-comment .timeline-comment-header {
                overflow: hidden;
            }

            .timeline-comment .timeline-comment-header img {
                width: 30px;
                border-radius: 50%;
                float: left;
                margin-right: 10px;
            }

            .timeline-comment .timeline-comment-header p {
                color: #000;
                float: left;
                margin: 0;
                font-weight: 500;
            }

            .timeline-comment .timeline-comment-header small {
                font-weight: normal;
                color: #8e8e8e;
            }

            .timeline-comment p.timeline-comment-text {
                display: block;
                color: #2b2b2b;
                font-size: 14px;
                padding-left: 40px;
            }

            .post-options {
                overflow: hidden;
                margin-top: 15px;
                margin-left: 15px;
            }

            .post-options a {
                display: block;
                margin-top: 5px;
                margin-right: 20px;
                float: left;
                color: #2b2b2b;
                text-decoration: none;
                font-size: 16px !important;
            }

            .post-options a:hover {
                color: #5369f8;
            }

            .online {
                position: absolute;
                top: 2px;
                right: 2px;
                display: block;
                width: 9px;
                height: 9px;
                border-radius: 50%;
                background: #ccc;
            }

            .online.on {
                background: #2ec5d3;
            }

            .online.off {
                background: #ec5e69;
            }

            #cd-timeline::before {
                border: 0;
                background: #f1f1f1;
            }

            .cd-timeline-content p,
            .cd-timeline-content .cd-read-more,
            .cd-timeline-content .cd-date {
                font-size: 14px;
            }

            .cd-timeline-img.cd-success {
                background: #2ec5d3;
            }

            .cd-timeline-img.cd-danger {
                background: #ec5e69;
            }

            .cd-timeline-img.cd-info {
                background: #5893df;
            }

            .cd-timeline-img.cd-warning {
                background: #f1c205;
            }

            .cd-timeline-img.cd-primary {
                background: #9f7ce1;
            }

            .page-inner.full-page {
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
            }

            .user-profile-card {
                text-align: center;
            }

            .user-profile-image {
                width: 100px;
                height: 100px;
                margin-bottom: 10px;
            }

            .team .team-member {
                display: block;
                overflow: hidden;
                margin-bottom: 10px;
                float: left;
                position: relative;
            }

            .team .team-member .online {
                top: 5px;
                right: 5px;
            }

            .team .team-member img {
                width: 40px;
                float: left;
                border-radius: 50%;
                margin: 0 5px 0 5px;
            }

            .label.label-success {
                background: #43d39e;
            }

            .label {
                font-weight: 400;
                padding: 4px 8px;
                font-size: 11px;
                display: inline-block;
                line-height: 1;
                color: #fff;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: 0.25em;
            }




            .select-menu_pro{
                width: 200px;
                padding-right: 29px;
/*                margin: 140px auto;*/
            }
            .select-menu_pro .select-btn_pro{
                display: flex;
                height: 55px;
                background: #fff;
                padding: 20px;
                font-size: 18px;
                font-weight: 400;
                border-radius: 8px;
                align-items: center;
                cursor: pointer;
                justify-content: space-between;
                box-shadow: 0 0 5px rgba(0,0,0,0.1);
            }
            .select-btn_pro i{
                font-size: 25px;
                transition: 0.3s;
            }
            .select-menu_pro.active .select-btn_pro i{
                transform: rotate(-180deg);
            }
            .select-menu_pro .options_pro{
                position: relative;
                padding: 20px;
                margin-top: 10px;
                border-radius: 8px;
                background: #fff;
                box-shadow: 0 0 3px rgba(0,0,0,0.1);
                display: none;
            }
            .select-menu_pro.active .options_pro{
                display: block;
            }
            .options_pro .option_pro{
                display: flex;
                height: 55px;
                cursor: pointer;
                padding: 0 16px;
                border-radius: 8px;
                align-items: center;
                background: #fff;
            }
            .options_pro .option_pro:hover{
                background: #F2F2F2;
            }
            .option_pro i{
                font-size: 25px;
                margin-right: 12px;
            }
            .option_pro .option-text{
                font-size: 18px;
                color: #333;
            }


        </style>
    </head>
    <body>

        <ul style="z-index: 999" class="notifications_ok"></ul>

        <!--<div class="container" style="display: flex ; justify-content: center ; gap: 25px">-->


        <div  class="middle">
            <form class="create-post">
                <div class="profile-photo">
                    <img src="${listFeed.get(0).accountId.avatar}">
                </div>
                <input id="create-post" type="text" placeholder="What's on your mind ?">
                <input type="submit" value="Post" class="btn btn-primary">
            </form>


            <div  class="feeds">


                <c:forEach  items="${listFeed}" var="f" >

                    <div   class="feed"   id="feed-${f.id}">


                        <div class="head">
                            <div class="user">
                                <div class="profile-photo">
                                    <img src="${f.accountId.avatar}">


                                </div>

                                <div class="ingo">
                                    <h3>${f.accountId.userName}
                                        <c:if test="${f.publicPrivate=='Public'}">
                                            <p class="fas fa-globe-asia"></p>
                                        </c:if>
                                        <c:if test="${f.publicPrivate!='Public'}">
                                            <p class="fas fa-lock">  </p>
                                        </c:if>
                                    </h3>


                                    <small>Vietnam ,</small>
                                    <small class="dumaaa">${f.createAt}</small>

                                    <input type="hidden" value="${f.createAt}">

                                </div>

                            </div>
                            <span style="position: relative"  id="editdm" class="edit">
                                <i class="uil uil-ellipsis-h"></i>



                                <div style="z-index: 1;" class="wrapper_111">
                                    <div class="content_111">
                                        <ul class="menu">
                                            <li class="item">
                                                <i class="uil uil-eye"></i>
                                                <span>Preview</span>
                                            </li>
                                            <li class="item share">
                                                <div>
                                                    <i class="uil uil-share"></i>
                                                    <span>Share</span>
                                                </div>
                                                <i class="uil uil-angle-right"></i>
                                                <ul class="share-menu">
                                                    <li class="item">
                                                        <i class="uil uil-twitter-alt"></i>
                                                        <span>Twitter</span>
                                                    </li>
                                                    <li class="item">
                                                        <i class="uil uil-instagram"></i>
                                                        <span>Instagram</span>
                                                    </li>
                                                    <li class="item">
                                                        <i class="uil uil-dribbble"></i>
                                                        <span>Dribble</span>
                                                    </li>
                                                    <li class="item">
                                                        <i class="uil uil-telegram-alt"></i>
                                                        <span>Telegram</span>
                                                    </li>
                                                </ul>
                                            </li>
                                            <input hidden name="copyProduct" value="http://localhost:9999/swp391/postDetail?id=${f.id}">
                                            <li onclick="createToast_123('success', 'Sao chép link thành công!.')" class="item copyProduct">
                                                <i class="uil uil-link-alt"></i>
                                                <span>Get Link</span>
                                            </li>
                                            <li  class="item editPost">
                                                <i class="uil uil-edit"></i>
                                                <span>Edit</span>
                                            </li>


                                            <label for="check1_${f.id}">
                                                <li class="item">
                                                    <i class="uil uil-trash-alt"></i>
                                                    <span>Delete</span>
                                                </li>

                                            </label>


                                        </ul>
                                        <div class="setting">
                                            <li class="item">
                                                <i class="uil uil-setting"></i>
                                                <span>Settings</span>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </span>


                        </div>



                        <div class="slider_post">
                            <div class="slides_post">


                                <c:forEach var="i" items="${ListImage}"  varStatus="loop">
                                    <c:if test="${i.productId.id==f.id }">

                                        <c:if test="${i.path !=null}">

                                            <img style="max-height: 600px" src="${i.path}" class="slide_post">
                                            <c:set var="imageCount" value="${loop.index + 1}" />
                                        </c:if>


                                    </c:if>
                                </c:forEach>  

                            </div>
                            <c:if test="${imageCount >1}">
                                <div class="slider-controls-post">
                                    <button id="prev_post"> <img style="border-radius: 50%; width: 40px; height: 40px;" src="image/left.jpg"> </button>  
                                    <button id="next_post"> <img style="border-radius: 50%; width: 40px; height: 40px;" src="image/right.jpg"> </button>   
                                </div>

                            </c:if>

                            <div class="counter_post" id="counter_post">1/1</div> 
                        </div>



                        <div class="action-buttons">
                            <div style="position: relative" class="interaction-buttons">

                                <span  class="heartIcon" >
                                    <i class="uil uil-heart"></i>
                                </span>

                                <span>
                                    <i class="uil uil-comment-dots"></i>
                                </span>

                                <span class="share_media view_modal_shareP">
                                    <i class="uil uil-share-alt"></i>
                                </span>

                                <label for="check_${f.id}">
                                    <span >
                                        <i class="fas fa-shopping-cart cart-icon"></i>
                                    </span>

                                </label>

                                <span>
                                    <i style="font-weight: 100" class="fas fa-chart-bar analysis-icon"></i>

                                </span>

                                <!--                                        <div class="popup123">
                                                                            <header>
                                                                                <span>Share Product</span>
                                                                                <div class="close"><i class="uil uil-times"></i></div>
                                                                            </header>
                                                                            <div class="content_123">
                                                                                <p>Share this link via</p>
                                                                                <ul class="icons">
                                                                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                                                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                                                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                                                                    <a href="#"><i class="fab fa-whatsapp"></i></a>
                                                                                    <a href="#"><i class="fab fa-telegram-plane"></i></a>
                                                                                </ul>
                                                                                <p>Or copy link</p>
                                                                                <div class="field">
                                                                                    <i class="url-icon uil uil-link"></i>
                                                                                    <input type="text" readonly value="example.com/share-link">
                                                                                    <button>Copy</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->

                                <div style="z-index: 999999999" class="popup123">
                                    <header>
                                        <span>Share Modal</span>
                                        <div class="close"><i class="uil uil-times"></i></div>
                                    </header>
                                    <div class="content_123">
                                        <p>Share this link via</p>
                                        <ul class="icons share_product">
                                            <a class="shareBtn_product" data-url="https://www.facebook.com/kibyhunters" data-social="facebook"><i
                                                    class="fab fa-facebook-f"></i></a>
                                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="twitter"><i
                                                    class="fab fa-twitter"></i></a>
                                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="instagram"><i
                                                    class="fab fa-instagram"></i></a>
                                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="line"><i
                                                    class="fab fa-whatsapp"></i></a>
                                            <a class="shareBtn_product" data-url="https://example.com/article" data-social="telegram"><i
                                                    class="fab fa-telegram-plane"></i></a>
                                        </ul>
                                        <p>Or copy link</p>
                                        <div class="field">
                                            <i class="url-icon uil uil-link"></i>
                                            <input type="text" readonly value="huhu">
                                            <button>Copy</button>
                                        </div>
                                    </div>
                                </div>





                            </div>

                            <c:set var="bookmarkDisplayed" value="false" />

                            <c:forEach var="p" items="${favoriteProducts}">
                                <c:if test="${f.id ==p.id}">
                                    <c:set var="bookmarkDisplayed" value="true" />

                                </c:if>
                            </c:forEach>

                            <c:if test="${bookmarkDisplayed==true}">
                                <div>
                                    <span class="bookmark" data-product-id="${f.id}"><i class="fa fa-solid fa-bookmark"></i></span>
                                </div>
                            </c:if>

                            <c:if test="${bookmarkDisplayed==false}">
                                <div>
                                    <span class="bookmark" data-product-id="${f.id}"><i class="uil uil-bookmark"></i></span>
                                </div>
                            </c:if>




                        </div>


                        <div class="liked-by">
                            <span> <img src="image/phuc.png"></span>
                            <span> <img src="image/khang.png"></span>
                            <span> <img src="image/hai.jpg"></span>

                            <p> Like by <b>Nguyen Phuc</b> and <b> 999 others</b></p>
                        </div>

                        <div class="caption">
                            <div class="caption_post">

                                <p>${f.productName}</p>
                                <p>${f.describe}</p>
                                <p>${f.information}</p>
                                <p>${f.price}</p>

                            </div>
                            <button style="padding: 2px; border-radius: 9px;" class="toggle-btn_caption">Show More</button>
                        </div>


                        <div class="text-muted">

                            <form id="postDetailForm" action="postDetail" method="post" target="_blank">
                                <input type="hidden" name="id" value="${f.id}">
                                <button type="submit" id="viewCommentsBtn" style="font-size: 12px;
                                        color: var(--color-dark);
                                        padding: 3px;
                                        width: fit-content;
                                        height: fit-content;
                                        margin-top: 6px;
                                        color: black; cursor: pointer">
                                    View all the comments
                                </button>
                            </form>

<!--<a  style="text-decoration: none; color: var(--color-dark); " href="postDetail?id=${f.id}" target="_blank">View all the comments </a>-->




                        </div>






                        <div id="modal-overlay-${f.id}" style="left: 29% ; top: 13% ;overflow: none" class="modal-overlay">

                            <div class="postBox_123 modal-content">
                                <button style="z-index: 888" class="close-btn"> <img style="width: 25px" src="image/exit.ico"> </button>

                                <div class="container_123">
                                    <div class="wrapper_123">
                                        <section class="post_123">
                                            <header style="width: inherit;" class="btn btn-primary">Edit Post</header>
                                            <form action="updatePost?id=${f.id}" enctype="multipart/form-data" method="post" style="    overflow-y: scroll;

                                                  height: 550px;
                                                  margin-right: 0px;" >

                                                <div class="content_123 helpme">
                                                    <img style="border-radius: 50%" src="${f.accountId.avatar}" alt="logo">
                                                    <div class="details_123 public_123">
                                                        <p> ${f.accountId.userName} </p>

                                                        <div class="privacy_123">

                                                            <i class="fas fa-globe"></i>



                                                            <span class="public_Private_Span">${f.publicPrivate}</span>


                                                            <input type="hidden" class="public_Private_Input" name="uPublicPrivate" value="Public">
                                                            <i class="fas fa-caret-down"></i>

                                                        </div>

                                                    </div>
                                                </div>


                                                <div style="margin-top: 6px;"> <strong>Title</strong> </div>

                                                <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                                          placeholder="Post and become a real seller" name="uTitle" spellcheck="false" required>${f.productName}</textarea>


                                                <div style="margin-bottom: 13px;"> <strong> Fee of tranction </strong> </div>

                                                <div class="radio-group_123">
                                                    <label>
                                                        <input type="radio" ${f.fee=='seller' ? 'checked' : ''} name="uFee" value="seller">Seller
                                                    </label>
                                                    <label>
                                                        <input type="radio" ${f.fee=='both' ? 'checked' : ''} name="uFee" value="both"> Both
                                                    </label>
                                                    <label>
                                                        <input type="radio" ${f.fee=='buyer' ? 'checked' : ''} name="uFee" value="buyer"> Buyer
                                                    </label>
                                                </div>



                                                <div style="margin-bottom: 6px;"> <strong> Price </strong> </div>

                                                <textarea class="price-input" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                                          name="uPrice" placeholder="Input your price ..." spellcheck="false" required>
                                                    ${f.price}

                                                </textarea>


                                                <div style="margin-bottom: 6px;"> <strong> Contact </strong> </div>

                                                <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                                          name="uContact" placeholder="Post and become a real seller" spellcheck="false" required>${f.information}</textarea>

                                                <div style="margin-bottom: 6px;"> <strong> Discription </strong> </div>


                                                <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                                          name="uDiscription"    placeholder="Post and become a real seller" spellcheck="false" required>${f.describe}</textarea>


                                                <div style="margin-bottom: 6px;margin-top: 10px;"> <strong> Information private </strong> </div>

                                                <textarea style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                                          name="uPrivateInfo"   placeholder="Post and become a real seller" spellcheck="false" required>${f.privateInfo}</textarea>


                                                <div style="margin-bottom: 6px;"> <strong> Image discription </strong> </div>



                                                <div class='wrapper1234 upload-container'>
                                                    <div class="upload_123">
                                                        <div class="upload-wrapper_123">

                                                            <div class="upload-img_123">
                                                                <!-- image here -->
                                                                <c:forEach var="i" items="${ListImage}">
                                                                    <c:if test="${i.productId.id==f.id }">

                                                                        <c:if test="${i.path !=null}">

                                                                            <div class="uploaded-img">
                                                                                <img src="${i.path}">
                                                                                <input type="hidden" name="uFileImage_normal" value="${i.path}">

                                                                                <button style="color:yellow" type="button" class="remove-btn">
                                                                                    <i class="fas fa-times"></i>
                                                                                </button>
                                                                            </div>

                                                                        </c:if>

                                                                    </c:if>
                                                                </c:forEach>  
                                                            </div>

                                                            <div class="upload-info_123">
                                                                <p>
                                                                    <span class="upload-info-value_123">0</span> file(s) uploaded.
                                                                </p>
                                                            </div>
                                                            <div class="upload-area_123"  >
                                                                <div class="upload-area-img_123">
                                                                    <img style="max-width: 80px;" src="image/upload.png" alt="">
                                                                </div>
                                                                <p class="upload-area-text_123">Select images or <span>browse</span>.</p>
                                                            </div>
                                                            <input type="file" name="uFileImage" class="visually-hidden upload-input_123" multiple>
                                                        </div>
                                                    </div>
                                                </div>








                                                <div class="theme-emoji">
                                                    <!-- <img src="icons/theme.svg" alt="theme"> -->
                                                    <img src="icons/smile.svg" alt="smile">
                                                </div>
                                                <div class="options">
                                                    <p>Add to Your Post</p>
                                                    <ul class="list">
                                                        <li><img src="icons/gallery.svg" alt="gallery"></li>
                                                        <li><img src="icons/tag.svg" alt="gallery"></li>
                                                        <li><img src="icons/emoji.svg" alt="gallery"></li>
                                                        <li><img src="icons/mic.svg" alt="gallery"></li>
                                                        <li><img src="icons/more.svg" alt="gallery"></li>
                                                    </ul>
                                                </div>
                                                <button>Post</button>
                                            </form>
                                        </section>
                                        <section class="audience_123">
                                            <header>
                                                <div class="arrow-back_123"><i class="fas fa-arrow-left"></i></div>
                                                <p>Select Audience</p>
                                            </header>
                                            <div class="content_123">
                                                <p>Who can see your post?</p>
                                                <span>Your post will show up in News Feed, on your profile and in search results.</span>
                                            </div>


                                            <ul class="list_123">
                                                <li class="active">
                                                    <div class="column_123">
                                                        <div class="icon"><i style="margin-left: 14px;
                                                                             margin-top: 13px;" class="fas fa-globe-asia"></i></div>
                                                        <div class="details_123">
                                                            <p>Public</p>
                                                            <span>Anyone on or off Facebook</span>
                                                        </div>
                                                    </div>
                                                    <div class="radio_123"></div>
                                                </li>

                                                <li>
                                                    <div class="column_123">
                                                        <div class="icon"><i style="margin-left: 14px;
                                                                             margin-top: 13px;" class="fas fa-user-friends"></i></div>
                                                        <div class="details_123">
                                                            <p>Friends</p>
                                                            <span>Your friends on Facebook</span>
                                                        </div>
                                                    </div>
                                                    <div class="radio_123"></div>
                                                </li>

                                                <li>
                                                    <div class="column_123">
                                                        <div class="icon"><i style="margin-left: 14px;
                                                                             margin-top: 13px;" class="fas fa-user"></i></div>
                                                        <div class="details_123">
                                                            <p>Specific</p>
                                                            <span>Only show to some friends</span>
                                                        </div>
                                                    </div>
                                                    <div class="radio_123"></div>
                                                </li>

                                                <li>
                                                    <div class="column_123">
                                                        <div class="icon"><i style="margin-left: 14px;
                                                                             margin-top: 13px;" class="fas fa-lock"></i></div>
                                                        <div class="details_123">
                                                            <p>Only me</p>
                                                            <span>Only you can see your post</span>
                                                        </div>
                                                    </div>
                                                    <div class="radio_123"></div>
                                                </li>

                                                <li>
                                                    <div class="column_123">
                                                        <div class="icon"><i style="margin-left: 14px;
                                                                             margin-top: 13px;" class="fas fa-cog"></i></div>
                                                        <div class="details_123">
                                                            <p>Custom</p>
                                                            <span>Include and exclude friends</span>
                                                        </div>
                                                    </div>
                                                    <div class="radio_123"></div>
                                                </li>

                                            </ul>

                                        </section>
                                    </div>
                                </div>


                            </div>
                        </div>




                        <div class="container_12345_123confirm">
                            <input type="checkbox"  id="check_${f.id}" class="input-checkbox_123confirm">
                            <div class="background_123confirm"></div>
                            <div  class="alert_box_123confirm">
                                <div class="icon_123confirm">
                                    <i style="position: absolute; top: 32px; right: 176px;" class="fas fa-exclamation-triangle"></i>
                                </div>
                                <header style="font-weight: 700; color: black;">Confirm</header>
                                <p style=" color: black;" class="content_confirm_123confirm">Are you sure want to buy this product?</p>
                                <div class="btns_123confirm" style="font-size: medium;">

                                    <label for="check_${f.id}" onclick="createToast_123('success', 'Bạn đã xác nhận mua đơn hàng mã ${f.id}.')"> Okey </label>
                                    <label for="check_${f.id}">Cancel</label>
                                </div>
                            </div>
                        </div>


                        <div class="container_12345_123confirm">
                            <input type="checkbox"  id="check1_${f.id}" class="input-checkbox_123confirm1">
                            <div class="background_123confirm"></div>
                            <div  class="alert_box_123confirm">
                                <div class="icon_123confirm">
                                    <i style="position: absolute; top: 32px; right: 176px;" class="fas fa-exclamation-triangle"></i>
                                </div>
                                <header style="font-weight: 700; color: black;">Confirm</header>
                                <p style=" color: black;" class="content_confirm_123confirm">Are you sure want to <span style="font-weight: bolder">delete</span> this product?</p>
                                <div class="btns_123confirm" style="font-size: medium;">
                                    <label for="check1_${f.id}" onclick="createToast_123('success', 'Bạn đã xóa thành công !')"> <a style="color: white" href="deleteProduct?id=${f.id}">Okey</a>  </label>
    <!--                                            <label for="check1_${f.id}" onclick="deleteProduct(event,`${f.id}`)" class="delete-product-btn" data-product-id="${f.id}">
    <span style="color: white; cursor: pointer;">Okey</span>
    </label>-->


                                    <label for="check1_${f.id}">Cancel</label>
                                </div>
                            </div>
                        </div>






                    </div>
                </c:forEach>









            </div>


        </div>



    </div>
    <div style="left :29% ;z-index: 9999" class="postBox">
        <div style="height:  650px" class="container123">
            <div class="wrapper123">
                <section class="post">
                    <header class="btn btn-primary">Create Post <img class="post_of" style="width: 30px ;height: 30px;float: right;" src="image/exit.ico"></header>

                    <form style="height: 575px; overflow-y: scroll;" id="myForm" enctype="multipart/form-data" action="createPost" method="post">
                        <div class="content">
                            <img style="border-radius: 50%;" src="${sessionScope.accountid.avatar}" alt="logo">
                            <div class="details">
                                <p>Vũ Trường</p>

                                <div class="privacy"   style="background: gray">

                                    <i class="fas fa-globe"></i>

                                    <span id="publicPrivateSpan">Public</span>


                                    <input type="hidden" id="publicPrivateInput" name="publicPrivate" value="Public">

                                    <i class="fas fa-caret-down"></i>
                                </div>

                            </div>
                        </div>



                        <div style="margin-top: 6px;"> <strong>Title</strong> </div>

                        <textarea name="name" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                  placeholder="Product name" spellcheck="false" required></textarea>


                        <div style="margin-bottom: 13px;"> <strong> Fee of tranction </strong> </div>

                        <div class="radio-group">
                            <label>
                                <input type="radio"  name="fee" value="seller" >Seller
                            </label>
                            <label>
                                <input type="radio" checked name="fee" value="both" > Both
                            </label>
                            <label>
                                <input type="radio" name="fee" value="buyer" style="margin-bottom: 6px;"> Buyer

                            </label>
                            <div style="margin-bottom: 13px;">    <strong> Price </strong> </div>


                            <textarea id="numberInput_Price" name="price" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;"
                                      placeholder="Input your price ..." spellcheck="false" required></textarea>
                            <div id="errorMessage_Price" style="color: red; display: none;">Please must input number!</div>


                            <div style="margin-bottom: 6px;"> <strong> Contact </strong> </div>

                            <textarea name="info" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                      placeholder="Input your information" spellcheck="false" required></textarea>

                            <div style="margin-bottom: 6px;"> <strong> Discription </strong> </div>


                            <textarea  id="editor" name="describe" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                       placeholder="Input your information" spellcheck="false"></textarea>



                            <div style="margin-bottom: 6px;margin-top: 10px;"> <strong> Information private </strong> </div>

                            <textarea name="privateInfo" style="border: 2px dashed green; padding: 6px ;border-radius: 5px;margin-bottom: 10px;"
                                      placeholder="Private information" spellcheck="false" required></textarea>


                            <div style="margin-bottom: 6px;"> <strong> Image discription </strong> </div>




                            <div class='wrapper234'>
                                <div class="upload">
                                    <div class="upload-wrapper">
                                        <div class="upload-img">
                                            <!-- image here -->
                                        </div>
                                        <div class="upload-info">
                                            <p>
                                                <span class="upload-info-value">0</span> file(s) uploaded.
                                            </p>
                                        </div>
                                        <div class="upload-area">
                                            <div class="upload-area-img">
                                                <img src="image/upload.png" alt="">
                                            </div>
                                            <p class="upload-area-text">Select images or <span>browse</span>.</p>
                                        </div>
                                        <input name="image" type="file" class="visually-hidden" id="upload-input" multiple>
                                    </div>
                                </div>
                            </div>




                            <div class="theme-emoji">
                                <!-- <img src="icons/theme.svg" alt="theme"> -->
                                <img src="icons/smile.svg" alt="smile">
                            </div>
                            <div class="options">
                                <p>Add to Your Post</p>
                                <ul style="display: flex ; float: right" class="list">
                                    <li><img src="icons/gallery.svg" alt="gallery"></li>
                                    <li><img src="icons/tag.svg" alt="gallery"></li>
                                    <li><img src="icons/emoji.svg" alt="gallery"></li>
                                    <li><img src="icons/mic.svg" alt="gallery"></li>
                                    <li><img src="icons/more.svg" alt="gallery"></li>
                                </ul>
                            </div>


                            <button type="submit" onclick="submitForm()  class ="btn btn-primary">Post</button>

                    </form>




                </section>
                <section class="audience">
                    <header>
                        <div class="arrow-back"><i class="fas fa-arrow-left"></i></div>
                        <p>Select Audience</p>
                    </header>
                    <div class="content">
                        <p>Who can see your post?</p>
                        <span>Your post will show up in News Feed, on your profile and in search results.</span>
                    </div>


                    <ul class="list1">
                        <li class="active">
                            <div class="column">
                                <div style=" margin: inherit; margin-top: 14px;" class="icon"><i style="margin: inherit;
                                                                                                 margin-left: 14px;" class="fas fa-globe-asia"></i></div>
                                <div class="details">
                                    <p>Public</p>
                                    <span>Anyone on or off Facebook</span>
                                </div>
                            </div>
                            <div class="radio"></div>
                        </li>

                        <li>
                            <div class="column">
                                <div class="icon"><i style="margin-top: 12px;
                                                     margin-left: 12px;"  class="fas fa-user-friends"></i></div>
                                <div class="details">
                                    <p>Friends</p>
                                    <span>Your friends on Facebook</span>
                                </div>
                            </div>
                            <div class="radio"></div>
                        </li>

                        <li>
                            <div class="column">
                                <div class="icon"><i style="margin-top: 12px;
                                                     margin-left: 14px;"  class="fas fa-user"></i></div>
                                <div class="details">
                                    <p>Specific</p>
                                    <span>Only show to some friends</span>
                                </div>
                            </div>
                            <div class="radio"></div>
                        </li>

                        <li>
                            <div class="column">
                                <div class="icon"><i style="margin-top: 13px;
                                                     margin-left: 14px;"  class="fas fa-lock" ></i></div>
                                <div class="details">
                                    <p>Only me</p>
                                    <span>Only you can see your post</span>
                                </div>
                            </div>
                            <div class="radio"></div>
                        </li>

                        <li>
                            <div class="column">
                                <div class="icon"><i style="margin-top: 13px;
                                                     margin-left: 14px;"  class="fas fa-cog"></i></div>
                                <div class="details">
                                    <p>Custom</p>
                                    <span>Include and exclude friends</span>
                                </div>
                            </div>
                            <div class="radio"></div>
                        </li>

                    </ul>

                </section>
            </div>
        </div>


    </div>
    <div style="z-index: 0" class="col-lg-12 col-xl-3">
        <div class="card card-white grid-margin">
            <div class="card-heading clearfix">
                <h4 class="card-title">Suggestions</h4>
            </div>
            <div class="card-body">
                <div class="team">
                    <div class="select-menu_pro">
                        <div class="select-btn_pro">
                            <span class="sBtn-text">Menu option</span>
                            <i class="bx bx-chevron-down"></i>
                        </div>

                        <ul class="options_pro">
                            <li class="option_pro">
                                <i class="bx bxl-github" style="color: #171515;"></i>
                                <span class="option-text">Register OTP</span>
                            </li>
                            <li class="option_pro">
                                <i class="bx bxl-instagram-alt" style="color: #E1306C;"></i>
                                <span class="option-text">Change OTP</span>
                            </li>
                            <li class="option_pro">
                                <i class="bx bxl-linkedin-square" style="color: #0E76A8;"></i>
                                <span class="option-text">Forgot OTP</span>
                            </li>
                            <li class="option_pro">
                                <i class="bx bxl-facebook-circle" style="color: #4267B2;"></i>
                                <span class="option-text">Change Password</span>
                            </li>
                            <li class="option_pro">
                                <i class="bx bxl-twitter" style="color: #1DA1F2;"></i>
                                <span class="option-text">Twitter</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-white grid-margin">
            <div class="card-heading clearfix">
                <h4 class="card-title">Some Info</h4>
            </div>
            <div class="card-body">
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                    architecto.</p>
            </div>
        </div>
    </div>

    <!--</div>-->





    <script>
//function deleteProduct(event, productId) {
//    event.preventDefault(); // Ngăn chặn mọi hành động mặc định
//
//    // Tạo yêu cầu xóa sản phẩm
//  var url = 'deleteProduct?id=' + encodeURIComponent(productId);
//console.log(url);
//    fetch(url, {
//        method: 'get' // Hoặc 'POST', tùy thuộc vào API của bạn
//    })
//    .then(response => {
//        if (!response.ok) {
//            throw new Error('Network response was not ok');
//        }
//
//        const productElement = document.querySelector(`#productElementId_${productId}`);
//        if (productElement) {
//            productElement.style.display = 'none'; // Hoặc productElement.remove() để xóa hoàn toàn
//        }
//        // Hiển thị thông báo thành công
//        createToast_123('success', 'Bạn đã xóa thành công !');
//    })
//    .catch(error => {
//        console.error('Lỗi khi xóa:', error);
//        // Hiển thị thông báo lỗi
//        createToast_123('error', 'Lỗi khi xóa sản phẩm');
//    });
//}







        document.querySelectorAll('.bookmark').forEach(function (bookmarkIcon) {
            bookmarkIcon.addEventListener('click', function (event) {
                event.preventDefault(); // Ngăn chặn hành vi mặc định của nút

                const bookmarkIconInner = bookmarkIcon.querySelector('i');
                const productId = bookmarkIcon.getAttribute('data-product-id');

                // Kiểm tra và chuyển đổi class
                if (bookmarkIconInner.classList.contains('uil-bookmark')) {
                    bookmarkIconInner.classList.remove('uil-bookmark');
                    bookmarkIconInner.classList.add('fa', 'fa-solid', 'fa-bookmark');
                    createToast_123('success', 'Thêm vào yêu thích thành công !');
                    // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
                    updateBookmarkStatus(productId, 'add');
                } else {
                    bookmarkIconInner.classList.remove('fa', 'fa-solid', 'fa-bookmark');
                    bookmarkIconInner.classList.add('uil-bookmark');
                    createToast_123('success', 'Đã xóa khỏi yêu thích !');
                    // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
                    updateBookmarkStatus(productId, 'remove');
                }
            });
        });



        const optionMenu = document.querySelector(".select-menu_pro"),
                selectBtn = optionMenu.querySelector(".select-btn_pro"),
                options = optionMenu.querySelectorAll(".option_pro"),
                sBtn_text = optionMenu.querySelector(".sBtn-text");

        selectBtn.addEventListener("click", () => optionMenu.classList.toggle("active"));

        options.forEach(option => {
            option.addEventListener("click", () => {
                let selectedOption = option.querySelector(".option-text").innerText;
                sBtn_text.innerText = selectedOption;

                optionMenu.classList.remove("active");
            })
        })

        function updateBookmarkStatus(productId, action) {
            // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
            // Ví dụ:
            fetch('bookmark?action=' + action + '&productId=' + productId)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        return response.json();
                    })
                    .then(data => {
                        // Xử lý kết quả trả về từ máy chủ (nếu cần)
                    })
                    .catch(error => {
                        console.error('There was a problem with the fetch operation:', error);
                    });
        }


    </script>
</body>
</html>
