
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Singup</title>

        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">

        <link rel="stylesheet" href="./css/login.css">

        <style>
            .generatePass{
                /*display: none;*/
                position: absolute;
                left: 63%
            }
        </style>

    </head>

    <body >

        <section>

            <div class="generatePass" >
                <jsp:include page="generatePass.jsp"></jsp:include>

                </div>

                <div class="form-box">
                    <div style="padding: 30px ;text-align: -webkit-center;" class="form-value">
                        <form  action="signup" method="post"  >
                            <h2>Sign Up</h2>

                            <div class="inputbox"> <ion-icon name="mail-outline"></ion-icon> 
                                <input type="text" name="username" value="${param.username}" required>
                            <label>UserName</label>
                        </div>

                        <div class="inputbox"> <ion-icon class="icon_gene" name="lock-closed-outline">  <i class="fas fa-key"></i> </ion-icon> 
                            <input type="password" name="password" value="${param.password}" required>
                            <label>Password</label>

                        </div>


                        <div class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <input type="password" name="cfpassword" value="${param.cfpassword}" required>
                            <label>Confirm Password</label>
                        </div>    

                        <div class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <input type="email" name="email" value="${param.email}" required>
                            <label>Email</label>
                        </div>  

                        <div style="border-bottom:none;display: flex" class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <img id="captcha" style="width: 256px" class="captcha" alt="captcha" src="/swp391/captcha">
                            <button id="reloadCaptcha" class="reload-btn" style="height: 44px; width: 44px; margin-top: 8px ;margin-left: 8px"><i class="fas fa-redo-alt"></i></button>
                        </div>


                        <div class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <input  type="text" name="captcha" > 
                            <label>Input captcha code</label>
                        </div>
                        <h3 id="error" style="color : red ; margin-bottom: 10px"> ${error} </h3>

                        <button id="dmm" type="submit">Sign Up</button>

                        <!--                        <div class="register">
                                                    <p>Don't have an account? <a href="#">Sign Up</a></p>
                                                </div>-->
                    </form>
                </div>
            </div>








        </section>








        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.3.js"
        integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>


        <script>

            $(document).ready(function () {
                // Gắn sự kiện click cho nút reload captcha
                $('#reloadCaptcha').click(function (e) {
                    e.preventDefault(); // Ngăn chặn hành động mặc định của nút
                    reloadCaptcha();
                });

                // Hàm để tải lại captcha bằng AJAX
                function reloadCaptcha() {
                    // Gửi yêu cầu AJAX để tải lại captcha
                    $.ajax({
                        type: 'GET',
                        url: '/swp391/captcha', // Đường dẫn tới endpoint để lấy captcha mới
                        success: function (data) {
                            // Cập nhật hình ảnh captcha với dữ liệu mới

                            var newImg = new Image();
                            // Đặt src của hình ảnh mới bằng đường dẫn tới endpoint để lấy captcha mới
                            newImg.src = '/swp391/captcha?' + new Date().getTime(); // Thêm timestamp để tránh cache
                            // Gán hình ảnh mới cho src của hình ảnh hiện tại
                            $('.captcha').attr('src', newImg.src);



                        },
                        error: function (xhr, status, error) {
                            // Xử lý lỗi nếu có
                            console.error('Lỗi tải lại captcha:', error);
                        }
                    });
                }
            });


            const lengthSlider = document.querySelector(".pg_pass-length input"),
                    options = document.querySelectorAll(".pg_option input"),
                    copyIcon = document.querySelector(".pg_input-box span"),
                    passwordInput = document.querySelector(".pg_input-box input"),
                    passIndicator = document.querySelector(".pg_pass-indicator"),
                    generateBtn = document.querySelector(".pg_generate-btn");

            const characters = {// object of letters, numbers & symbols
                lowercase: "abcdefghijklmnopqrstuvwxyz",
                uppercase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                numbers: "0123456789",
                symbols: "^!$%&|[](){}:;.,*+-#@<>~"
            };

            const generatePassword = () => {
                let staticPassword = "",
                        randomPassword = "",
                        excludeDuplicate = false,
                        passLength = lengthSlider.value;

                options.forEach(option => { // looping through each option's checkbox
                    if (option.checked) { // if checkbox is checked
                        // Correcting the IDs to include the 'pg_' prefix
                        if (option.id !== "pg_exc-duplicate" && option.id !== "pg_spaces") {
                            // adding particular key value from character object to staticPassword
                            staticPassword += characters[option.id.replace("pg_", "")];
                        } else if (option.id === "pg_spaces") { // if checkbox id is spaces
                            staticPassword += `  ${staticPassword}  `; // adding space at the beginning & end of staticPassword
                        } else { // else pass true value to excludeDuplicate
                            excludeDuplicate = true;
                        }
                    }
                });

                for (let i = 0; i < passLength; i++) {
                    // getting random character from the static password
                    let randomChar = staticPassword[Math.floor(Math.random() * staticPassword.length)];
                    if (excludeDuplicate) { // if excludeDuplicate is true
                        // if randomPassword doesn't contains the current random character or randomChar is equal 
                        // to space " " then add random character to randomPassword else decrement i by -1
                        !randomPassword.includes(randomChar) || randomChar == " " ? randomPassword += randomChar : i--;
                    } else { // else add random character to randomPassword
                        randomPassword += randomChar;
                    }
                }
                passwordInput.value = randomPassword; // passing randomPassword to passwordInput value
            };

            const upadatePassIndicator = () => {
                // Updating the ID assignment to include 'pg_' prefix for consistency
                passIndicator.id = lengthSlider.value <= 8 ? "weak" : lengthSlider.value <= 16 ? "medium" : "strong";
            };

            const updateSlider = () => {
                // passing slider value as counter text
                document.querySelector(".pg_pass-length span").innerText = lengthSlider.value;
                generatePassword();
                upadatePassIndicator();
            };
            updateSlider();

            const copyPassword = () => {
                navigator.clipboard.writeText(passwordInput.value); // copying random password
                copyIcon.innerText = "check"; // changing copy icon to tick
                copyIcon.style.color = "#4285F4";
                setTimeout(() => { // after 1500 ms, changing tick icon back to copy
                    copyIcon.innerText = "copy_all";
                    copyIcon.style.color = "#707070";
                }, 1500);
            };

            copyIcon.addEventListener("click", copyPassword);
            lengthSlider.addEventListener("input", updateSlider);
            generateBtn.addEventListener("click", generatePassword);





            document.addEventListener('DOMContentLoaded', function () {
                // Truy cập đến phần tử đầu tiên có class 'icon_gene'
                var iconGene = document.querySelector('.icon_gene');

                // Kiểm tra xem phần tử có tồn tại không
                if (iconGene) {
                    // Thêm sự kiện 'click' cho phần tử
                    iconGene.addEventListener('click', function () {
                        // Truy cập đến phần tử đầu tiên có class 'generatePass'
                        var generatePassElement = document.querySelector('.generatePass');

                        // Kiểm tra xem phần tử có tồn tại không
                        if (generatePassElement) {
                            // Kiểm tra trạng thái hiển thị hiện tại và chuyển đổi nó
                            if (generatePassElement.style.display === 'none' || !generatePassElement.style.display) {
                                generatePassElement.style.display = 'block'; // Hiển thị phần tử
                            } else {
                                generatePassElement.style.display = 'none'; // Ẩn phần tử
                            }
                        }
                    });
                }
            });

        </script>

    </body>

</html>
