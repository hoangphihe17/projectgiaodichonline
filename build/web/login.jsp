

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="./css/login.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">

        <style>
            /* Import Google font - Poppins */
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap');
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
            }
            :root {
                --dark: #34495E;
                --light: #ffffff;
                --success: #0ABF30;
                --error: #E24D4C;
                --warning: #E9BD0C;
                --info: #3498DB;
            }
            body {
                display: flex;
                align-items: center;
                justify-content: center;
                min-height: 100vh;
                background: var(--dark);
            }
            .notifications {
                position: fixed;
                top: 30px;
                right: 20px;
            }
            .notifications .toast {
                display: flex;
                align-items: center;
            }
            .notifications .column {
                display: flex;
                align-items: center;
            }
            .notifications .toast {
                width: 400px;
                position: relative;
                overflow: hidden;
                list-style: none;
                border-radius: 4px;
                padding: 16px 17px;
                margin-bottom: 10px;
                background: var(--light);
                justify-content: space-between;
                animation: show_toast 0.3s ease forwards;
            }
            @keyframes show_toast {
                0% {
                    transform: translateX(100%);
                }
                40% {
                    transform: translateX(-5%);
                }
                80% {
                    transform: translateX(0%);
                }
                100% {
                    transform: translateX(-10px);
                }
            }
            .notifications .toast.hide {
                animation: hide_toast 0.3s ease forwards;
            }
            @keyframes hide_toast {
                0% {
                    transform: translateX(-10px);
                }
                40% {
                    transform: translateX(0%);
                }
                80% {
                    transform: translateX(-5%);
                }
                100% {
                    transform: translateX(calc(100% + 20px));
                }
            }
            .toast::before {
                position: absolute;
                content: "";
                height: 3px;
                width: 100%;
                bottom: 0px;
                left: 0px;
                animation: progress 3s linear forwards;
            }
            @keyframes progress {
                100% {
                    width: 0%;
                }
            }
            .toast.success::before, .btn#success {
                background: var(--success);
            }
            .toast.error::before, .btn#error {
                background: var(--error);
            }
            .toast.warning::before, .btn#warning {
                background: var(--warning);
            }
            .toast.info::before, .btn#info {
                background: var(--info);
            }
            .toast .column i {
                font-size: 1.75rem;
            }
            .toast.success .column i {
                color: var(--success);
            }
            .toast.error .column i {
                color: var(--error);

            }
            .toast.warning .column i {
                color: var(--warning);
            }
            .toast.info .column i {
                color: var(--info);
            }
            .toast .column span {
                font-size: 1.07rem;
                margin-left: 12px;
            }
            .toast i:last-child {
                color: #aeb0d7;
                cursor: pointer;
            }
            .toast i:last-child:hover {
                color: var(--dark);
            }
            .buttons .btn {
                border: none;
                outline: none;
                cursor: pointer;
                margin: 0 5px;
                color: var(--light);
                font-size: 1.2rem;
                padding: 10px 20px;
                border-radius: 4px;
            }

            @media screen and (max-width: 530px) {
                .notifications {
                    width: 95%;
                }
                .notifications .toast {
                    width: 100%;
                    font-size: 1rem;
                    margin-left: 20px;
                }
                .buttons .btn {
                    margin: 0 1px;
                    font-size: 1.1rem;
                    padding: 8px 15px;
                }
            }
        </style>

    </head>

    <body >




        <section>
            <ul class="notifications"></ul>

            <div class="form-box">
                <div class="form-value">

                    <form id="loginForm" action="login" method="post">

                        <h2>Login</h2>
                        <c:set var="cookie" value="${pageContext.request.cookies}"> </c:set>

                            <div class="inputbox"> <ion-icon name="mail-outline"></ion-icon> 
                                <input name="username" value="${cookie.user.value}"  type="text" required>
                            <label>UserName</label>
                        </div>

                        <div class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <input type="password" name="password" value="${cookie.pass.value}"  required> 
                            <label>Password</label>
                        </div>

                        <!--                        <div>
                        
                                                    <div style="width: 100%" class="col-md-6">
                                                        <img style="width: 200px" class="captcha" alt="captcha" src="/swp391/captcha">
                                                        <button class="reload-btn" style="height: 44px; width: 44px;"><i class="fas fa-redo-alt"></i></button>
                                                    </div>
                                                </div>   -->

                        <div style="border-bottom:none;display: flex" class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <img id="captcha" style="width: 256px" class="captcha" alt="captcha" src="/swp391/captcha">
                            <button id="reloadCaptcha" class="reload-btn" style="height: 44px; width: 44px; margin-top: 8px ;margin-left: 8px"><i class="fas fa-redo-alt"></i></button>
                        </div>


                        <div class="inputbox"> <ion-icon name="lock-closed-outline"></ion-icon> 
                            <input  type="text" name="captcha" > 
                            <label>Input captcha code</label>
                        </div>



                        <h3 style="color : red ;margin-bottom: 10px"> ${error} </h3>



                        <div style="display: flex" class="forget"  > <label style="display: flex"> <input style="display: block" ${(cookie.remember!=null?'checked':'')} type="checkbox" name="remember" >Remember Me</label>
                            <a href="forgotPass">ForgotPassword</a> 
                        </div> 




                        <button class="btn"  onclick="createToast('warning', '${error}')" >Login</button>

                        <div class="register">
                            <p>Don't have an account? <a href="signup">Sign Up</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </section>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.3.js"
        integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script src="./javacript/demo.js"></script>


        <script>

//                            document.addEventListener("DOMContentLoaded", function () {
//                                const loginForm = document.getElementById("loginForm");
//
//                                loginForm.addEventListener("submit", function (event) {
//                                    // Ngăn chặn sự kiện mặc định của form
//                                    event.preventDefault();
//
//                                    // Lấy dữ liệu form
//                                    const formData = new FormData(loginForm);
//
//                                    // Gửi dữ liệu form bằng AJAX
//                                    const xhr = new XMLHttpRequest();
//                                    xhr.open("POST", loginForm.getAttribute("action"));
//                                    xhr.onreadystatechange = function () {
//                                        if (xhr.readyState === XMLHttpRequest.DONE) {
//                                            if (xhr.status === 200) {
//                                                // Xử lý phản hồi từ máy chủ (nếu cần)
//                                                // Ví dụ: hiển thị thông báo thành công
//                                                const response = xhr.responseText;
//                                                console.log(response); // Để xem phản hồi từ máy chủ
//                                                createToast('success', loginForm); // Hiển thị thông báo
//                                            } else {
//                                                // Xử lý lỗi từ máy chủ (nếu cần)
//                                                // Ví dụ: hiển thị thông báo lỗi
//                                                createToast('error', 'An error occurred'); // Hiển thị thông báo
//                                            }
//                                        }
//                                    };
//                                    xhr.send(formData);
//                                });
//                            });
//


  
            $(document).ready(function () {
                // Gắn sự kiện click cho nút reload captcha
                $('#reloadCaptcha').click(function (e) {
                    e.preventDefault(); // Ngăn chặn hành động mặc định của nút
                    reloadCaptcha();
                });

                // Hàm để tải lại captcha bằng AJAX
                function reloadCaptcha() {
                    // Gửi yêu cầu AJAX để tải lại captcha
                    $.ajax({
                        type: 'GET',
                        url: '/swp391/captcha', // Đường dẫn tới endpoint để lấy captcha mới
                        success: function (data) {
                            // Cập nhật hình ảnh captcha với dữ liệu mới

                            var newImg = new Image();
                            // Đặt src của hình ảnh mới bằng đường dẫn tới endpoint để lấy captcha mới
                            newImg.src = '/swp391/captcha?' + new Date().getTime(); // Thêm timestamp để tránh cache
                            // Gán hình ảnh mới cho src của hình ảnh hiện tại
                            $('.captcha').attr('src', newImg.src);


                            
                        },
                        error: function (xhr, status, error) {
                            // Xử lý lỗi nếu có
                            console.error('Lỗi tải lại captcha:', error);
                        }
                    });
                }
            });


        </script>


    </body>

</html>
