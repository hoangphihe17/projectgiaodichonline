
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">

        <title>Generate Passoword</title>
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap');
            *{
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
            }
            body{
                display: flex;
                align-items: center;
                justify-content: center;
                min-height: 100vh;
                /*background: #4285F4;*/
            }
            .pg_container{
                width: 340px;
                height: 400px;
                background: #fff;
                border-radius: 8px;
                box-shadow: 0 10px 20px rgba(0,0,0,0.05);
            }
            .pg_container h2{
                font-weight: 600;
                font-size: 1.31rem;
                padding: 1rem 1.75rem;
                border-bottom: 1px solid #d4dbe5;
            }
            .pg_wrapper{
                margin: 1.25rem 1.75rem;
            }
            .pg_wrapper .pg_input-box{
                position: relative;
            }
            .pg_input-box input{
                width: 100%;
                height: 53px;
                color: #000;
                background: none;
                font-size: 17px;
                font-weight: 500;
                border-radius: 4px;
                letter-spacing: 1.4px;
                border: 1px solid #aaa;
                padding: 0 2.85rem 0 1rem;
            }
            .pg_input-box span{
                position: absolute;
                right: 13px;
                cursor: pointer;
                line-height: 53px;
                color: #707070;
            }
            .pg_input-box span:hover{
                color: #4285F4!important;
            }
            .pg_wrapper .pg_pass-indicator{
                width: 100%;
                height: 4px;
                position: relative;
                background: #dfdfdf;
                margin-top: 0.75rem;
                border-radius: 25px;
            }
            .pg_pass-indicator::before{
                position: absolute;
                content: "";
                height: 100%;
                width: 50%;
                border-radius: inherit;
                transition: width 0.3s ease;
            }
            .pg_pass-indicator#weak::before{
                width: 20%;
                background: #E64A4A;
            }
            .pg_pass-indicator#medium::before{
                width: 50%;
                background: #f1c80b;
            }
            .pg_pass-indicator#strong::before{
                width: 100%;
                background: #4285F4;
            }
            .pg_wrapper .pg_pass-length{
                margin: 1.56rem 0 1.25rem;
            }
            .pg_pass-length .pg_details{
                display: flex;
                justify-content: space-between;
            }
            .pg_pass-length input{
                width: 100%;
                height: 5px;
            }
            .pg_pass-settings .pg_options{
                display: flex;
                list-style: none;
                flex-wrap: wrap;
                margin-top: 1rem;
            }
            .pg_pass-settings .pg_options .pg_option{
                display: flex;
                align-items: center;
                margin-bottom: 1rem;
                width: calc(100% / 2);
            }
            .pg_options .pg_option:first-child{
                pointer-events: none;
            }
            .pg_options .pg_option:first-child input{
                opacity: 0.7;
            }
            .pg_options .pg_option input{
                height: 16px;
                width: 16px;
                cursor: pointer;
            }
            .pg_options .pg_option label{
                cursor: pointer;
                color: #4f4f4f;
                padding-left: 0.63rem;
            }
            .pg_wrapper .pg_generate-btn{
                width: 100%;
                color: #fff;
                border: none;
                outline: none;
                cursor: pointer;
                background: #4285F4;
                font-size: 1.06rem;
                padding: 3px;
                border-radius: 5px;
                text-transform: uppercase;
            }

        </style>
    </head>

    <body>

        <div style="border-radius: 15px;" class="pg_container">
            <h2 style="font-size: 17px;padding: 10px;border-radius: 13px;background: #4285F4;">Password Generator</h2>
            <div class="pg_wrapper">
                <div class="pg_input-box">
                    <input type="text" disabled>
                    <span class="material-symbols-rounded">copy_all</span>
                </div>
                <div class="pg_pass-indicator"></div>
                <div style="margin: 13px;" class="pg_pass-length">
                    <div class="pg_details">
                        <label style="font-size: 13px;" class="pg_title">Password Length</label>
                        <span></span>
                    </div>
                    <input style="font-size: 13px;" type="range" min="6" max="30" value="15" step="1">
                </div>
                <div class="pg_pass-settings">
                    <label style="font-size: 13px;" class="pg_title">Password Settings</label>
                    <ul class="pg_options">
                        <li class="pg_option">
                            <input type="checkbox" id="lowercase" checked>
                            <label style="font-size: 13px;" for="lowercase">Lowercase (a-z)</label>
                        </li>
                        <li class="pg_option">
                            <input type="checkbox" id="uppercase">
                            <label style="font-size: 13px;" for="uppercase">Uppercase (A-Z)</label>
                        </li>
                        <li class="pg_option">
                            <input type="checkbox" id="numbers">
                            <label style="font-size: 13px;" for="numbers">Numbers (0-9)</label>
                        </li>
                        <li class="pg_option">
                            <input type="checkbox" id="symbols">
                            <label style="font-size: 13px;" for="symbols">Symbols (!-$^+)</label>
                        </li>
                        <li class="pg_option">
                            <input type="checkbox" id="exc-duplicate">
                            <label style="font-size: 13px;" for="exc-duplicate">Exclude Duplicate</label>
                        </li>
                        <li class="pg_option">
                            <input type="checkbox" id="spaces">
                            <label style="font-size: 13px;" for="spaces">Include Spaces</label>
                        </li>
                    </ul>
                </div>
                <button class="pg_generate-btn">Generate Password</button>
            </div>
        </div>

        <script>
            const lengthSlider = document.querySelector(".pg_pass-length input"),
                    options = document.querySelectorAll(".pg_option input"),
                    copyIcon = document.querySelector(".pg_input-box span"),
                    passwordInput = document.querySelector(".pg_input-box input"),
                    passIndicator = document.querySelector(".pg_pass-indicator"),
                    generateBtn = document.querySelector(".pg_generate-btn");

            const characters = {// object of letters, numbers & symbols
                lowercase: "abcdefghijklmnopqrstuvwxyz",
                uppercase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                numbers: "0123456789",
                symbols: "^!$%&|[](){}:;.,*+-#@<>~"
            }

            const generatePassword = () => {
                let staticPassword = "",
                        randomPassword = "",
                        excludeDuplicate = false,
                        passLength = lengthSlider.value;

                options.forEach(option => { // looping through each option's checkbox
                    if (option.checked) { // if checkbox is checked
                        // Correcting the IDs to include the 'pg_' prefix
                        if (option.id !== "pg_exc-duplicate" && option.id !== "pg_spaces") {
                            // adding particular key value from character object to staticPassword
                            staticPassword += characters[option.id.replace("pg_", "")];
                        } else if (option.id === "pg_spaces") { // if checkbox id is spaces
                            staticPassword += `  ${staticPassword}  `; // adding space at the beginning & end of staticPassword
                        } else { // else pass true value to excludeDuplicate
                            excludeDuplicate = true;
                        }
                    }
                });

                for (let i = 0; i < passLength; i++) {
                    // getting random character from the static password
                    let randomChar = staticPassword[Math.floor(Math.random() * staticPassword.length)];
                    if (excludeDuplicate) { // if excludeDuplicate is true
                        // if randomPassword doesn't contains the current random character or randomChar is equal 
                        // to space " " then add random character to randomPassword else decrement i by -1
                        !randomPassword.includes(randomChar) || randomChar == " " ? randomPassword += randomChar : i--;
                    } else { // else add random character to randomPassword
                        randomPassword += randomChar;
                    }
                }
                passwordInput.value = randomPassword; // passing randomPassword to passwordInput value
            };

            const upadatePassIndicator = () => {
                // Updating the ID assignment to include 'pg_' prefix for consistency
                passIndicator.id = lengthSlider.value <= 8 ? "weak" : lengthSlider.value <= 16 ? "medium" : "strong";
            };

            const updateSlider = () => {
                // passing slider value as counter text
                document.querySelector(".pg_pass-length span").innerText = lengthSlider.value;
                generatePassword();
                upadatePassIndicator();
            };
            updateSlider();

            const copyPassword = () => {
                navigator.clipboard.writeText(passwordInput.value); // copying random password
                copyIcon.innerText = "check"; // changing copy icon to tick
                copyIcon.style.color = "#4285F4";
                setTimeout(() => { // after 1500 ms, changing tick icon back to copy
                    copyIcon.innerText = "copy_all";
                    copyIcon.style.color = "#707070";
                }, 1500);
            };

            copyIcon.addEventListener("click", copyPassword);
            lengthSlider.addEventListener("input", updateSlider);
            generateBtn.addEventListener("click", generatePassword);

        </script>
    </body>
</html>
