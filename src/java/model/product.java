/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class product {

    String id;
    String productName, fee, describe, information;
    int like, view, quantity;
    String price, privateInfo, publicPrivate, status, sortDelete, createAt, updateAt;
    int category;
    account accountId;

    public product() {
    }

    public product(String id, String productName, String fee, String describe, String information, int like, int view, int quantity, String price, String privateInfo, String publicPrivate, String status, String sortDelete, String createAt, String updateAt, int category, account accountId) {
        this.id = id;
        this.productName = productName;
        this.fee = fee;
        this.describe = describe;
        this.information = information;
        this.like = like;
        this.view = view;
        this.quantity = quantity;
        this.price = price;
        this.privateInfo = privateInfo;
        this.publicPrivate = publicPrivate;
        this.status = status;
        this.sortDelete = sortDelete;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.category = category;
        this.accountId = accountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrivateInfo() {
        return privateInfo;
    }

    public void setPrivateInfo(String privateInfo) {
        this.privateInfo = privateInfo;
    }

    public String getPublicPrivate() {
        return publicPrivate;
    }

    public void setPublicPrivate(String publicPrivate) {
        this.publicPrivate = publicPrivate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSortDelete() {
        return sortDelete;
    }

    public void setSortDelete(String sortDelete) {
        this.sortDelete = sortDelete;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public account getAccountId() {
        return accountId;
    }

    public void setAccountId(account accountId) {
        this.accountId = accountId;
    }

    
    
    
    
}
