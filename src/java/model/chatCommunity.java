/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class chatCommunity {
    int id ; 
    String message , createAt , updateAt ;
    account accountid ;

    public chatCommunity() {
    }

    public chatCommunity(int id, String message, String createAt, String updateAt, account accountid) {
        this.id = id;
        this.message = message;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.accountid = accountid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public account getAccountid() {
        return accountid;
    }

    public void setAccountid(account accountid) {
        this.accountid = accountid;
    }
    
}
