/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class rating {
    int id , starNo;
    String content, createAt , updateAt , productid ;
    account accountid;
    String softDelete , deleteBy;

    public rating() {
    }

    public rating(int id, int starNo, String content, String createAt, String updateAt, String productid, account accountid, String softDelete, String deleteBy) {
        this.id = id;
        this.starNo = starNo;
        this.content = content;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.productid = productid;
        this.accountid = accountid;
        this.softDelete = softDelete;
        this.deleteBy = deleteBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStarNo() {
        return starNo;
    }

    public void setStarNo(int starNo) {
        this.starNo = starNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public account getAccountid() {
        return accountid;
    }

    public void setAccountid(account accountid) {
        this.accountid = accountid;
    }

    public String getSoftDelete() {
        return softDelete;
    }

    public void setSoftDelete(String softDelete) {
        this.softDelete = softDelete;
    }

    public String getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(String deleteBy) {
        this.deleteBy = deleteBy;
    }
    
    
}
