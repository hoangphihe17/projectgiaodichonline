
package model;

/**
 *
 * @author Truong cun
 */
public class verify {
    int id ;
    String bank , numberBank , otp;
    int accountid;

    public verify() {
    }

    public verify(int id, String bank, String numberBank, String otp, int accountid) {
        this.id = id;
        this.bank = bank;
        this.numberBank = numberBank;
        this.otp = otp;
        this.accountid = accountid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }
    
}
