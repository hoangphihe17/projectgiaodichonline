/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class analyze {
    String orderSuccess , revenue , numberOfCustomer , revenueLastMonth;

    public analyze() {
    }

    public analyze(String orderSuccess, String revenue, String numberOfCustomer, String revenueLastMonth) {
        this.orderSuccess = orderSuccess;
        this.revenue = revenue;
        this.numberOfCustomer = numberOfCustomer;
        this.revenueLastMonth = revenueLastMonth;
    }
    
    
      public analyze(String orderSuccess, String revenue) {
        this.orderSuccess = orderSuccess;
        this.revenue = revenue;
       
    }

    public String getOrderSuccess() {
        return orderSuccess;
    }

    public void setOrderSuccess(String orderSuccess) {
        this.orderSuccess = orderSuccess;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getNumberOfCustomer() {
        return numberOfCustomer;
    }

    public void setNumberOfCustomer(String numberOfCustomer) {
        this.numberOfCustomer = numberOfCustomer;
    }

    public String getRevenueLastMonth() {
        return revenueLastMonth;
    }

    public void setRevenueLastMonth(String revenueLastMonth) {
        this.revenueLastMonth = revenueLastMonth;
    }
    
    
    
}
