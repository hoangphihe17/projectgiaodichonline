/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.analyze;
import model.order;
import model.product;

/**
 *
 * @author Truong cun
 */
public class productDAO extends DBContext {

    public List<product> getAllList() {
        List<product> list = new ArrayList<>();

        String sql = "select a.id as aid , userName , password, email, address, phone, avatar , role, band, code,a.status as astatus ,a.sortDelete as aSort ,\n"
                + "a.createAt as acreate ,a.updateAt as aupdate , p.* from account a join product p on a.id = p.accountId ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("aid"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("astatus"), rs.getString("aSort"), rs.getString("acreate"), rs.getString("aupdate"));

                product p = new product(rs.getString("id"), rs.getString("productName"), rs.getString("fee"), rs.getString("describe"),
                        rs.getString("infomation"), rs.getInt("like"), rs.getInt("view"), rs.getInt("quantity"),
                        rs.getString("price"), rs.getString("privateInfo"), rs.getString("publicPrivate"), rs.getString("status"),
                        rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"), rs.getInt("categoryId"), a);
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public List<product> getAllListByID(int id) {
        List<product> list = new ArrayList<>();

        String sql = "select a.id as aid , userName , password, email, address, phone, avatar , role, band, code,a.status as astatus ,a.sortDelete as aSort ,\n"
                + "a.createAt as acreate ,a.updateAt as aupdate , p.* from account a join product p on a.id = p.accountId where a.id =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("aid"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("astatus"), rs.getString("aSort"), rs.getString("acreate"), rs.getString("aupdate"));

                product p = new product(rs.getString("id"), rs.getString("productName"), rs.getString("fee"), rs.getString("describe"),
                        rs.getString("infomation"), rs.getInt("like"), rs.getInt("view"), rs.getInt("quantity"),
                        rs.getString("price"), rs.getString("privateInfo"), rs.getString("publicPrivate"), rs.getString("status"),
                        rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"), rs.getInt("categoryId"), a);
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<product> getListByPage(List<product> list, int s, int e) {
        ArrayList<product> a = new ArrayList<>();
        for (int i = s; i < e; i++) {
            a.add(list.get(i));
        }
        return a;
    }

    public List<product> checkID(String id) {
        List<product> list = new ArrayList<>();

        String sql = "select * from product where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                account a = new account();
                product p = new product(rs.getString("id"), rs.getString("productName"), rs.getString("fee"), rs.getString("describe"),
                        rs.getString("infomation"), rs.getInt("like"), rs.getInt("view"), rs.getInt("quantity"),
                        rs.getString("price"), rs.getString("privateInfo"), rs.getString("publicPrivate"), rs.getString("status"),
                        rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"), rs.getInt("categoryId"), a);
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void createPost(String tradeID, String name, String fee, String describe, String info, String price,
            String privateInfo, String publicprivate, int userID) {
        String sql = "INSERT INTO [dbo].[product]\n"
                + "           ([productName]\n"
                + "           ,[fee]\n"
                + "           ,[describe]\n"
                + "           ,[infomation]\n"
                + "           ,[like]\n"
                + "           ,[view]\n"
                + "           ,[quantity]\n"
                + "           ,[price]\n"
                + "           ,[privateInfo]\n"
                + "           ,[publicPrivate]\n"
                + "           ,[status]\n"
                + "           ,[sortDelete]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt]\n"
                + "           ,[categoryId]\n"
                + "           ,[accountId]\n"
                + "            ,[id])"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setString(2, fee);
            st.setString(3, describe);
            st.setString(4, info);
            st.setInt(5, 0);
            st.setInt(6, 0);
            st.setInt(7, 1);
            st.setString(8, price);
            st.setString(9, privateInfo);
            st.setString(10, publicprivate);
            st.setString(11, "wait");
            st.setString(12, "no");
            st.setString(13, formattedDateTime);
            st.setString(14, formattedDateTime);
            st.setInt(15, 1);
            st.setInt(16, userID);
            st.setString(17, tradeID);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updatePost(String tradeID, String name, String fee, String describe, String info, String price,
            String privateInfo, String publicprivate) {
        String sql = "UPDATE [dbo].[product]\n"
                + "   SET \n"
                + "      [productName] = ?\n"
                + "      ,[fee] = ?\n"
                + "      ,[describe] = ?\n"
                + "      ,[infomation] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[privateInfo] = ?\n"
                + "      ,[publicPrivate] = ?\n"
                + "     \n"
                + "      ,[updateAt] = ?\n"
                + "    \n"
                + " WHERE id = ?";
        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setString(2, fee);
            st.setString(3, describe);
            st.setString(4, info);

            st.setString(5, price);
            st.setString(6, privateInfo);
            st.setString(7, publicprivate);
            st.setString(8, formattedDateTime);
            st.setString(9, tradeID);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteProduct(String id) {
        String sql = "delete from product where id =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println();
        }
    }

    public product getProductbyID(String id) {
        String sql = "select a.id as aid , userName , password, email, address, phone, avatar , role, band, code,a.status as astatus ,a.sortDelete as aSort ,\n"
                + "a.createAt as acreate ,a.updateAt as aupdate , p.* from account a join product p on a.id = p.accountId where p.id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                account a = new account(rs.getInt("aid"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("astatus"), rs.getString("aSort"), rs.getString("acreate"), rs.getString("aupdate"));

                product d = new product(rs.getString("id"), rs.getString("productName"), rs.getString("fee"), rs.getString("describe"),
                        rs.getString("infomation"), rs.getInt("like"), rs.getInt("view"), rs.getInt("quantity"),
                        rs.getString("price"), rs.getString("privateInfo"), rs.getString("publicPrivate"), rs.getString("status"),
                        rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"), rs.getInt("categoryId"), a);
                return d;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    public String time(String time) {
        String sql = "SELECT DATEDIFF(minute, ?, GETDATE()) AS MinuteDiff;";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, time);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int minuteDiff = rs.getInt("MinuteDiff");
                return Integer.toString(minuteDiff); // Trả về sự khác biệt thời gian dưới dạng chuỗi
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ""; // Trả về chuỗi trống nếu có lỗi
    }

    public List<product> searchProduct(String key, String min, String max) {
        List<product> list = new ArrayList<>();
        String sql = "SELECT\n"
                + "  product.id AS product_id,\n"
                + "  productName,\n"
                + "  fee,\n"
                + "  describe,\n"
                + "  infomation,\n"
                + "  [like],\n"
                + "  [view],\n"
                + "  quantity,\n"
                + "  price,\n"
                + "  privateInfo,\n"
                + "  publicPrivate,\n"
                + "  product.status AS product_status,\n"
                + "  product.sortDelete AS product_sortDelete,\n"
                + "  product.createAt AS product_createAt,\n"
                + "  product.updateAt AS product_updateAt,\n"
                + "  categoryId,\n"
                + "  account.id AS account_id,\n"
                + "  userName,\n"
                + "  password,\n"
                + "  email,\n"
                + "  address,\n"
                + "  phone,\n"
                + "  avatar,\n"
                + "  role,\n"
                + "  band,\n"
                + "  code,\n"
                + "  account.status AS account_status,\n"
                + "  account.sortDelete AS account_sortDelete,\n"
                + "  account.createAt AS account_createAt,\n"
                + "  account.updateAt AS account_updateAt\n"
                + "FROM\n"
                + "  product\n"
                + "JOIN account ON product.accountId = account.id \n"
                + "where 1 =1 ";
        if (key != null && !(key.equals(""))) {
            sql += " and productName  like '%" + key + "%' or describe like '%" + key + "%'\n"
                    + "or infomation  like '%" + key + "%' or userName like '%" + key + "%' ";
        }
        if (min != null && !(min.equals(""))) {
            sql += " and price > " + min;
        }
        if (max != null && !(max.equals(""))) {
            sql += " and price < " + max;

        }
        sql += " and publicPrivate like 'Public%'";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(
                        rs.getInt("account_id"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getString("address"),
                        rs.getString("phone"),
                        rs.getString("avatar"),
                        rs.getString("role"),
                        rs.getString("band"),
                        rs.getString("code"),
                        rs.getString("account_status"),
                        rs.getString("account_sortDelete"),
                        rs.getString("account_createAt"),
                        rs.getString("account_updateAt")
                );

                product d = new product(
                        rs.getString("product_id"),
                        rs.getString("productName"),
                        rs.getString("fee"),
                        rs.getString("describe"),
                        rs.getString("infomation"),
                        rs.getInt("like"),
                        rs.getInt("view"),
                        rs.getInt("quantity"),
                        rs.getString("price"),
                        rs.getString("privateInfo"),
                        rs.getString("publicPrivate"),
                        rs.getString("product_status"),
                        rs.getString("product_sortDelete"),
                        rs.getString("product_createAt"),
                        rs.getString("product_updateAt"),
                        rs.getInt("categoryId"),
                        a
                );
                list.add(d);
            }
        } catch (SQLException e) {
            System.out.println();

        }

        return list;
    }

    public static void main(String[] args) {
        productDAO d = new productDAO();
        productImageDAO d1= new productImageDAO();
//        d1.deleteImage("9Q1dkMoj51DHOoI");
//        d.deleteProduct("9Q1dkMoj51DHOoI");
        System.out.println(d.getAllListByID(1).size());
//System.out.println(d.getAllListOrder("1").get(0).getAccountid().getAvatar());
    }
}
