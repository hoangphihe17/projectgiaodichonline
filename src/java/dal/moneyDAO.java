/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.deposit;
import model.transfer;
import model.withdraw;

/**
 *
 * @author Truong cun
 */
public class moneyDAO extends DBContext {

    public List<deposit> getALlListDeposit(String id) {
        List<deposit> list = new ArrayList<>();
        String sql = "select * from deposit where accountId =? order by id desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                deposit a = new deposit(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<withdraw> getALlListWithDraw(String id) {
        List<withdraw> list = new ArrayList<>();
        String sql = "select * from [Withdraw] where accountId =?  order by id desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                withdraw a = new withdraw(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public void deposit(double money, String date, int accountid) {
        String sql = "INSERT INTO [dbo].[deposit]\n"
                + "           ([money]\n"
                + "          \n"
                + "           ,[content]\n"
                + "           ,[createAt]\n"
                + "           ,[accountId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "          \n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, money);
            st.setString(2, "image/ncbBank.jpg");
            st.setString(3, date);
            st.setInt(4, accountid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<String> totalDeposit(int accountid) {
        List<String> list = new ArrayList<>();
        String sql = "select count(d.id) as number , SUM(d.money) as deposit  from deposit d  join account a on "
                + "d.accountId = a.id where a.id =? ";
        String a = "";
        String b = "";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = rs.getString("deposit");
                b = rs.getString("number");
                list.add(b);
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<String> totalWithdraw(int accountid) {
        List<String> list = new ArrayList<>();

        String sql = "select count(w.id) as number , SUM(w.money)"
                + " as withdraw from withdraw w join account a on a.id =w.accountId where a.id =? ";
        String a = "";
        String b = "";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = rs.getString("withdraw");
                b = rs.getString("number");
                list.add(b);
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<String> totalTrans(int accountid) {
        List<String> list = new ArrayList<>();
        String sql = "select count(t.id) as number , SUM(t.money) as transer"
                + "  from transfer t join account a on t.senderId = a.id where a.id =?";
        String a = "";
        String b = "";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = rs.getString("transer");
                b = rs.getString("number");
                list.add(b);
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<transfer> getAllTransfer(int accountid) {
        String sql = "select t.id as tid , t.money , t.content , t.createAt as tCreate, t.status as tStatus , t.senderId , t.receiveId,b.* from transfer \n"
                + "t join account a on t.senderId = a.id join account b on b.id = t.receiveId  where t.receiveId =?  or t.senderId =? order by t.id desc ";
//        String a = "";
        List<transfer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountid);
            st.setInt(2, accountid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
                transfer t = new transfer(rs.getInt("tid"), rs.getString("money"), rs.getString("content"),
                        rs.getString("tCreate"), rs.getString("tStatus"), rs.getString("senderId"), a);
                list.add(t);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }
 LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public void insertTransfer(double money, int sender, int receive) {
        String sql = "INSERT INTO [dbo].[transfer]\n"
                + "           ([money]\n"
                + "           ,[content]\n"
                + "           ,[createAt]\n"
                + "           ,[status]\n"
                + "           ,[senderId]\n"
                + "           ,[receiveId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
          String formattedDateTime = currentDateTime.format(formatter);

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, money);
            st.setString(2, "transfer");
            st.setString(3, formattedDateTime);
            st.setString(4, "Success");
            
            st.setInt(5, sender);
            st.setInt(6, receive);
            
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        moneyDAO m = new moneyDAO();
        System.out.println(m.totalDeposit(2).get(1));
        m.insertTransfer(111111, 3, 4);
//m.deposit("10000", "2024", "1");
//        System.out.println(m.getAllTransfer(1).get(0).getReceiveId().getAddress());
    }

}
