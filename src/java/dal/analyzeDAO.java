/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.analyze;
import model.order;
import model.product;
import model.weekDay;

/**
 *
 * @author Truong cun
 */
public class analyzeDAO extends DBContext {

    public analyze getAnalyzeOK(String id) {
        String sql = "SELECT \n"
                + "    COUNT(o.id) AS orderSuccess,\n"
                + "    SUM(o.price * o.quantity) AS revenue,\n"
                + "    COUNT(DISTINCT o.accountId) AS numberOfCustomer,\n"
                + "    (\n"
                + "        SELECT \n"
                + "            SUM(o.price * o.quantity) \n"
                + "        FROM \n"
                + "             [order] o  join product p on o.productId =p.id\n"
                + "        WHERE \n"
                + "            success = 'yes' \n"
                + "            AND MONTH(orderdate) = MONTH(GETDATE()) - 1\n"
                + "    ) AS revenueLastMonth\n"
                + "\n"
                + "FROM \n"
                + "    [order] o  join product p on o.productId =p.id\n"
                + "WHERE \n"
                + "     \n"
                + "    success = 'yes' \n"
                + "    AND MONTH(orderdate) = MONTH(GETDATE())\n"
                + "	and p.accountId=?;\n"
                + "\n"
                + "";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new analyze(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public analyze getAnalyzeNO(String id) {
        String sql = "SELECT \n"
                + "    COUNT(o.id) AS orderFail,\n"
                + "    SUM(o.price * o.quantity) AS revenueFail,\n"
                + "    COUNT(DISTINCT o.accountId) AS numberOfCustomerFail,\n"
                + "    (\n"
                + "        SELECT \n"
                + "            SUM(o.price * o.quantity) \n"
                + "        FROM \n"
                + "             [order] o  join product p on o.productId =p.id\n"
                + "        WHERE \n"
                + "            success = 'no' \n"
                + "            AND MONTH(orderdate) = MONTH(GETDATE()) - 1\n"
                + "    ) AS revenueFailLastMonth\n"
                + "\n"
                + "FROM \n"
                + "    [order] o  join product p on o.productId =p.id\n"
                + "WHERE \n"
                + "     \n"
                + "    success = 'no' \n"
                + "    AND MONTH(orderdate) = MONTH(GETDATE())\n"
                + "	and p.accountId=?;";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new analyze(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public analyze top1Customer(String id) {
        String sql = "	SELECT top 1\n"
                + "    o.accountId,\n"
                + "    SUM(o.price * o.quantity) AS total\n"
                + "FROM \n"
                + "    [order] o\n"
                + "JOIN \n"
                + "    product p ON o.productId = p.id\n"
                + "WHERE \n"
                + "    o.success = 'yes'\n"
                + "    AND MONTH(o.orderdate) = MONTH(GETDATE())\n"
                + "	and p.accountId=?\n"
                + "\n"
                + "GROUP BY \n"
                + "    o.accountId\n"
                + "\n"
                + "order by SUM(o.price * o.quantity) desc;\n"
                + "";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                if (rs.getString(2) == null) {
                    return new analyze(rs.getString(1), "0");

                } else {
                    return new analyze(rs.getString(1), rs.getString(2));

                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<analyze> top3Customer(String id) {
        List<analyze> list = new ArrayList<>();
        String sql = "SELECT top 3 \n"
                + "    o.accountId,\n"
                + "    SUM(o.price * o.quantity) AS total\n"
                + "FROM \n"
                + "    [order] o\n"
                + "JOIN \n"
                + "    product p ON o.productId = p.id\n"
                + "WHERE \n"
                + "    o.success = 'yes'\n"
                + "    AND year(o.orderdate) = year(GETDATE())\n"
                + "	and p.accountId=?\n"
                + "\n"
                + "GROUP BY \n"
                + "    o.accountId\n"
                + "\n"
                + "order by SUM(o.price * o.quantity) desc;";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString(2) == null) {
                    analyze ana = new analyze(rs.getString(1), "0");
                    list.add(ana);

                } else {
                    analyze ana = new analyze(rs.getString(1), rs.getString(2));
                    list.add(ana);

                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public analyze lastTop1Customer(String id) {
        String sql = "	SELECT top 1\n"
                + "    o.accountId,\n"
                + "    SUM(o.price * o.quantity) AS total\n"
                + "FROM \n"
                + "    [order] o\n"
                + "JOIN \n"
                + "    product p ON o.productId = p.id\n"
                + "WHERE \n"
                + "    o.success = 'yes'\n"
                + "    AND MONTH(o.orderdate) = (MONTH(GETDATE())-1)\n"
                + "	and p.accountId=?\n"
                + "\n"
                + "GROUP BY \n"
                + "    o.accountId\n"
                + "\n"
                + "order by SUM(o.price * o.quantity) desc;\n"
                + "";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                if (rs.getString(2) == null) {
                    return new analyze(rs.getString(1), "0");

                } else {
                    return new analyze(rs.getString(1), rs.getString(2));

                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<weekDay> weekly(String id) {
        List<weekDay> list = new ArrayList<>();
        String sql = "WITH DaysOfWeek AS (\n"
                + "    SELECT 'Monday' AS DayName, 1 AS SortOrder UNION ALL\n"
                + "    SELECT 'Tuesday', 2 UNION ALL\n"
                + "    SELECT 'Wednesday', 3 UNION ALL\n"
                + "    SELECT 'Thursday', 4 UNION ALL\n"
                + "    SELECT 'Friday', 5 UNION ALL\n"
                + "    SELECT 'Saturday', 6 UNION ALL\n"
                + "    SELECT 'Sunday', 7\n"
                + "),\n"
                + "OrdersPerDay AS (\n"
                + "    SELECT \n"
                + "        d.DayName,\n"
                + "        ISNULL(SUM(o.price * o.quantity), 0) AS Revenue\n"
                + "    FROM \n"
                + "        DaysOfWeek d\n"
                + "    LEFT JOIN [order] o ON DATENAME(weekday, o.orderdate) = d.DayName\n"
                + "        AND MONTH(o.orderdate) = MONTH(GETDATE())\n"
                + "        AND YEAR(o.orderdate) = YEAR(GETDATE())\n"
                + "        AND o.success = 'yes'\n"
                + "     JOIN product p ON p.id = o.productId \n"
                + "    JOIN account a ON p.accountId = a.id AND a.id = ?\n"
                + "    GROUP BY \n"
                + "        d.DayName, d.SortOrder\n"
                + ")\n"
                + "SELECT \n"
                + "    d.DayName,\n"
                + "    od.Revenue\n"
                + "FROM \n"
                + "    DaysOfWeek d\n"
                + "LEFT JOIN OrdersPerDay od ON d.DayName = od.DayName\n"
                + "ORDER BY \n"
                + "    d.SortOrder;";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                if (rs.getString(2) == null) {
                    weekDay d = new weekDay(rs.getString(1), "0");
                    list.add(d);
                } else {
                    weekDay d = new weekDay(rs.getString(1), rs.getString(2));
                    list.add(d);
                }

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<weekDay> lastWeek(String id) {
        List<weekDay> list = new ArrayList<>();
        String sql = "WITH DaysOfWeek AS (\n"
                + "    SELECT 'Monday' AS DayName, 1 AS SortOrder UNION ALL\n"
                + "    SELECT 'Tuesday', 2 UNION ALL\n"
                + "    SELECT 'Wednesday', 3 UNION ALL\n"
                + "    SELECT 'Thursday', 4 UNION ALL\n"
                + "    SELECT 'Friday', 5 UNION ALL\n"
                + "    SELECT 'Saturday', 6 UNION ALL\n"
                + "    SELECT 'Sunday', 7\n"
                + "),\n"
                + "OrdersPerDay AS (\n"
                + "    SELECT \n"
                + "        d.DayName,\n"
                + "        ISNULL(SUM(o.price * o.quantity), 0) AS Revenue\n"
                + "    FROM \n"
                + "        DaysOfWeek d\n"
                + "    LEFT JOIN [order] o ON DATENAME(weekday, o.orderdate) = d.DayName\n"
                + "        AND MONTH(o.orderdate) = (MONTH(GETDATE())-1)\n"
                + "        AND YEAR(o.orderdate) = YEAR(GETDATE())\n"
                + "        AND o.success = 'yes'\n"
                + "     JOIN product p ON p.id = o.productId \n"
                + "    JOIN account a ON p.accountId = a.id AND a.id = ?\n"
                + "    GROUP BY \n"
                + "        d.DayName, d.SortOrder\n"
                + ")\n"
                + "SELECT \n"
                + "    d.DayName,\n"
                + "    od.Revenue\n"
                + "FROM \n"
                + "    DaysOfWeek d\n"
                + "LEFT JOIN OrdersPerDay od ON d.DayName = od.DayName\n"
                + "ORDER BY \n"
                + "    d.SortOrder;";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                if (rs.getString(2) == null) {
                    weekDay d = new weekDay(rs.getString(1), "0");
                    list.add(d);
                } else {
                    weekDay d = new weekDay(rs.getString(1), rs.getString(2));
                    list.add(d);
                }

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public weekDay yearAndLastYear(String id) {

        String sql = "SELECT \n"
                + "    SUM(CASE WHEN YEAR(o.orderdate) = YEAR(GETDATE()) THEN o.price * o.quantity ELSE 0 END) AS totalNow,\n"
                + "    SUM(CASE WHEN YEAR(o.orderdate) = YEAR(GETDATE()) - 1 THEN o.price * o.quantity ELSE 0 END) AS totalLastYear\n"
                + "FROM \n"
                + "    [order] o \n"
                + "JOIN \n"
                + "    product p ON o.productId = p.id\n"
                + "WHERE \n"
                + "    o.success = 'yes'\n"
                + "    AND p.accountId = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                return new weekDay(rs.getString(1), rs.getString(2));

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<order> getAllListOrder(String id) {
        List<order> list = new ArrayList<>();

        String sql = "	SELECT\n"
                + "  p.id AS product_id,\n"
                + "  p.productName,\n"
                + "  p.fee,\n"
                + "  p.describe,\n"
                + "  p.infomation,\n"
                + "  p.[like],\n"
                + "  p.[view],\n"
                + "  p.quantity AS product_quantity,\n"
                + "  p.price AS product_price,\n"
                + "  p.privateInfo,\n"
                + "  p.publicPrivate,\n"
                + "  p.status AS product_status,\n"
                + "  p.sortDelete AS product_sortDelete,\n"
                + "  p.createAt AS product_createAt,\n"
                + "  p.updateAt AS product_updateAt,\n"
                + "  p.categoryId,\n"
                + "  a.id AS account_id,\n"
                + "  a.userName,\n"
                + "  a.password,\n"
                + "  a.email,\n"
                + "  a.address,\n"
                + "  a.phone,\n"
                + "  a.avatar,\n"
                + "  a.role,\n"
                + "  a.band,\n"
                + "  a.code,\n"
                + "  a.status AS account_status,\n"
                + "  a.sortDelete AS account_sortDelete,\n"
                + "  a.createAt AS account_createAt,\n"
                + "  a.updateAt AS account_updateAt,\n"
                + "  o.id AS order_id,\n"
                + "  o.price AS order_price,\n"
                + "  o.quantity AS order_quantity,\n"
                + "  o.note,\n"
                + "  o.orderdate,\n"
                + "  o.success,\n"
                + "  o.sortDelete AS order_sortDelete,\n"
                + "  o.productId AS order_productId,\n"
                + "  o.accountId AS order_accountId\n"
                + "FROM\n"
                + "  product p\n"
                + "JOIN\n"
                + "  [order] o ON p.id = o.productId\n"
                + "JOIN\n"
                + "  account a ON o.accountId = a.id\n"
                + "WHERE\n"
                + "  p.accountId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("account_id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("account_status"), rs.getString("account_sortDelete"), rs.getString("account_createAt"), rs.getString("account_updateAt"));

                product p = new product(rs.getString("product_id"), rs.getString("productName"), rs.getString("fee"), rs.getString("describe"),
                        rs.getString("infomation"), rs.getInt("like"), rs.getInt("view"), rs.getInt("product_quantity"),
                        rs.getString("product_price"), rs.getString("privateInfo"), rs.getString("publicPrivate"), rs.getString("product_status"),
                        rs.getString("product_sortDelete"), rs.getString("product_createAt"), rs.getString("product_updateAt"), rs.getInt("categoryId"), a);

                order o = new order(rs.getInt("order_id"), rs.getString("order_price"), rs.getInt("order_quantity"),
                        rs.getString("note"), rs.getString("orderdate"), rs.getString("success"),
                        rs.getString("order_sortDelete"), p, a);
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<order> Order(List<order> list, int s, int e) {
        ArrayList<order> a = new ArrayList<>();
        for (int i = s; i < e; i++) {
            a.add(list.get(i));
        }
        return a;
    }

    public static void main(String[] args) {

        analyzeDAO d = new analyzeDAO();
        System.out.println(d.top1Customer("1").getRevenue());

    }
}
