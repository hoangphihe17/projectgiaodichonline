/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.boxChat;
import model.message;

/**
 *
 * @author Truong cun
 */
public class boxChatDAO extends DBContext {

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public boxChat checkBoxChat(int user1, int user2) {
        String sql = "select * from boxChat where user1 = ? and user2 =?";
        try {
            
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user1);
            st.setInt(2, user2);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                account b = new account();
                boxChat a = new boxChat(rs.getInt("id"), b, b, "", "");
                return a;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
    
    public boxChat getBoxChatById(int id){
        String sql = "select * from boxChat where id =? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
                        ResultSet rs = st.executeQuery();

            if(rs.next()){
                account b = new account();
                boxChat a = new boxChat(rs.getInt("id"), b, b, "", "");
                return a;
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public void createBoxChat(int user1, int user2) {
        String sql = "INSERT INTO [dbo].[boxChat]\n"
                + "           ([user1]\n"
                + "           ,[user2]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";
        try {

            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user1);
            st.setInt(2, user2);
            st.setString(3, formattedDateTime);
            st.setString(4, formattedDateTime);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    
    
    
    
  

    public static void main(String[] args) {
        boxChatDAO b = new boxChatDAO();
//        System.out.println(b.getBoxChatById(1).getUser1());
    }
}
