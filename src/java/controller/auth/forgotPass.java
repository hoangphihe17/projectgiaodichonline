/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "forgotPass", urlPatterns = {"/forgotPass"})
public class forgotPass extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet forgotPass</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet forgotPass at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("forgotPass.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String captcha = request.getParameter("captcha");

        accountDAO d = new accountDAO();
        account a = d.acc(username);
        String captcha_txt = request.getSession().getAttribute("captcha").toString();

        HttpSession s = request.getSession();

        if (s.getAttribute("account") != null) {

            if (a == null) {
                request.setAttribute("error", "Account not exits");
                request.getRequestDispatcher("forgotPass.jsp").forward(request, response);

            } else if (!captcha_txt.equals(captcha)) {
                request.setAttribute("error", "Captcha must not null and match");

                request.getRequestDispatcher("forgotPass.jsp").forward(request, response);

            } else {
                request.setAttribute("error", "Check your email");

                request.getRequestDispatcher("login.jsp").forward(request, response);

                String accountMail = "truongvxhe176609@fpt.edu.vn";
                String codeMail = "ppgc nrmq ynyk mhwu";
                Properties props = new Properties();
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                Session session1 = Session.getInstance(props, new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(accountMail, codeMail);
                    }
                });
                try {
                    MimeMessage message = new MimeMessage(session1);
                    message.setFrom(new InternetAddress(accountMail));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(a.getEmail()));
                    message.setSubject("Your new password");
                    StringBuilder sb = new StringBuilder();

                    message.setContent("<h3 style=\"font-family: cursive; color: green; display :inline\" > New Password </h3> : <br>"
                            + "<h2> <a href=\"http://localhost:9999/swp391/changePass?user=" + username + "\">Click to change password</a> </h2>", "text/html");
                    Transport.send(message);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }

            }

        } else {

            s.setAttribute("account", a);

            if (a == null) {
                request.setAttribute("error", "Account not exits");
                request.getRequestDispatcher("forgotPass.jsp").forward(request, response);

            }else if (!captcha_txt.equals(captcha)) {
                request.setAttribute("error", "Captcha must not null and match");

                request.getRequestDispatcher("forgotPass.jsp").forward(request, response);

            } 
            
            else {
                request.setAttribute("error", "Check your email");

                request.getRequestDispatcher("login.jsp").forward(request, response);

                String accountMail = "truongvxhe176609@fpt.edu.vn";
                String codeMail = "ppgc nrmq ynyk mhwu";
                Properties props = new Properties();
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                Session session1 = Session.getInstance(props, new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(accountMail, codeMail);
                    }
                });
                try {
                    MimeMessage message = new MimeMessage(session1);
                    message.setFrom(new InternetAddress(accountMail));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(a.getEmail()));
                    message.setSubject("Your new password");
                    StringBuilder sb = new StringBuilder();

                    message.setContent("<h3 style=\"font-family: cursive; color: green; display :inline\" > New Password </h3> : <br>"
                            + "<h2> <a href=\"http://localhost:9999/swp391/changePass?user=" + username + "\">Click to change password</a> </h2>", "text/html");
                    Transport.send(message);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
