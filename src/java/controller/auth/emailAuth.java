/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "emailAuth", urlPatterns = {"/emailAuth"})
public class emailAuth extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet emailAuth</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet emailAuth at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     request.getRequestDispatcher("emailAuth.jsp").forward(request, response);
    }

//    public static boolean isValidEmail(String email) {
//        // Biểu thức chính quy để kiểm tra định dạng email
//        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
//
//        // Tạo đối tượng Pattern
//        Pattern pattern = Pattern.compile(emailRegex);
//
//        // Tạo đối tượng Matcher
//        Matcher matcher = pattern.matcher(email);
//
//        // Kiểm tra định dạng email
//        return matcher.matches();
//    }
    public String randomPass() {

        int maximum = 122;
        int minimum = 1;

        Random rn = new Random();
        String pass = "";
        int range = maximum - minimum;

        while (true) {

            int randomNum = rn.nextInt(range) + minimum;
            if (randomNum >= 65 && randomNum < 90 || randomNum >= 97 && randomNum <= 122) {
                char a = (char) randomNum;
                pass += a;
            } else if (randomNum < 10) {
                pass += randomNum;
            }

            if (pass.length() == 10) {
                break;
            }
        }

        return pass;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String email = request.getParameter("email");
        String code = request.getParameter("code");
     
        String user = request.getParameter("user");

     

        try {

            accountDAO a = new accountDAO();
            
            account ok = a.acc(user);
            

            if (code.equalsIgnoreCase(ok.getCode())) {
                account b = new account(ok.getUserName(), "0", "pass","");
                a.updatePro5(b);
                request.getRequestDispatcher("home.jsp").forward(request, response);

            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
