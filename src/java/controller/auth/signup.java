/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "signup", urlPatterns = {"/signup"})
public class signup extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet signup</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet signup at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    public String randomPass() {

        int maximum = 122;
        int minimum = 1;

        Random rn = new Random();
        String pass = "";
        int range = maximum - minimum;

        while (true) {

            int randomNum = rn.nextInt(range) + minimum;
            if (randomNum >= 65 && randomNum < 90 || randomNum >= 97 && randomNum <= 122) {
                char a = (char) randomNum;
                pass += a;
            } else if (randomNum < 10) {
                pass += randomNum;
            }

            if (pass.length() == 10) {
                break;
            }
        }

        return pass;
    }

    public static boolean isValidEmail(String email) {
        // Biểu thức chính quy để kiểm tra định dạng email
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

        // Tạo đối tượng Pattern
        Pattern pattern = Pattern.compile(emailRegex);

        // Tạo đối tượng Matcher
        Matcher matcher = pattern.matcher(email);

        // Kiểm tra định dạng email
        return matcher.matches();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("username");
        String pass = request.getParameter("password");
        String cfpass = request.getParameter("cfpassword");
        String email = request.getParameter("email");
        String captcha = request.getParameter("captcha");

        String captcha_txt = request.getSession().getAttribute("captcha").toString();

        if (pass.equalsIgnoreCase(cfpass)) {

            if (isValidPassword(pass)) {

                accountDAO d = new accountDAO();
                accountDAO ac = new accountDAO();

                account a = ac.check(user, pass);
                if (a != null || d.checkexits(user) || d.checkexitsEmail(email)) {
                    request.setAttribute("error", "UserName or Email exits please input other");
                    request.getRequestDispatcher("signup.jsp").forward(request, response);

                } else if (isValidUserName(user) == false) {
                    request.setAttribute("error", "UserName must >2 character and <15");
                    request.getRequestDispatcher("signup.jsp").forward(request, response);

                } else if (isValidEmail(email) == false) {
                    request.setAttribute("error", "Email invalid");
                    request.getRequestDispatcher("signup.jsp").forward(request, response);
                } else if (!captcha_txt.equals(captcha)) {

                    request.setAttribute("error", "Captcha code must not null and match");
                    request.getRequestDispatcher("signup.jsp").forward(request, response);

                } else {
                    String code = randomPass();
                    account b = new account(user, email, pass, "user", code, "not");
                    ac.register(b);
                    request.setAttribute("user", user);
                    
                    
                    
                    request.getRequestDispatcher("emailAuth.jsp").forward(request, response);

                    String accountMail = "truongvxhe176609@fpt.edu.vn";
                    String codeMail = "ppgc nrmq ynyk mhwu";
                    Properties props = new Properties();
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.port", "587");
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.starttls.enable", "true");
                    Session session1 = Session.getInstance(props, new javax.mail.Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(accountMail, codeMail);
                        }
                    });
                    try {
                        MimeMessage message = new MimeMessage(session1);
                        message.setFrom(new InternetAddress(accountMail));
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
                        message.setSubject("Your new password");
                        StringBuilder sb = new StringBuilder();

                        message.setContent("<html>\n"
                                + "<head>\n"
                                + "\n"
                                + "  <meta charset=\"utf-8\">\n"
                                + "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n"
                                + "  <title>Email Confirmation</title>\n"
                                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                                + "  <style type=\"text/css\">\n"
                                + "  /**\n"
                                + "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n"
                                + "   */\n"
                                + "  @media screen {\n"
                                + "    @font-face {\n"
                                + "      font-family: 'Source Sans Pro';\n"
                                + "      font-style: normal;\n"
                                + "      font-weight: 400;\n"
                                + "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n"
                                + "    }\n"
                                + "    @font-face {\n"
                                + "      font-family: 'Source Sans Pro';\n"
                                + "      font-style: normal;\n"
                                + "      font-weight: 700;\n"
                                + "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n"
                                + "    }\n"
                                + "  }\n"
                                + "  /**\n"
                                + "   * Avoid browser level font resizing.\n"
                                + "   * 1. Windows Mobile\n"
                                + "   * 2. iOS / OSX\n"
                                + "   */\n"
                                + "  body,\n"
                                + "  table,\n"
                                + "  td,\n"
                                + "  a {\n"
                                + "    -ms-text-size-adjust: 100%; /* 1 */\n"
                                + "    -webkit-text-size-adjust: 100%; /* 2 */\n"
                                + "  }\n"
                                + "  /**\n"
                                + "   * Remove extra space added to tables and cells in Outlook.\n"
                                + "   */\n"
                                + "  table,\n"
                                + "  td {\n"
                                + "    mso-table-rspace: 0pt;\n"
                                + "    mso-table-lspace: 0pt;\n"
                                + "  }\n"
                                + "  /**\n"
                                + "   * Better fluid images in Internet Explorer.\n"
                                + "   */\n"
                                + "  img {\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "  }\n"
                                + "  /**\n"
                                + "   * Remove blue links for iOS devices.\n"
                                + "   */\n"
                                + "  a[x-apple-data-detectors] {\n"
                                + "    font-family: inherit !important;\n"
                                + "    font-size: inherit !important;\n"
                                + "    font-weight: inherit !important;\n"
                                + "    line-height: inherit !important;\n"
                                + "    color: inherit !important;\n"
                                + "    text-decoration: none !important;\n"
                                + "  }\n"
                                + "  /**\n"
                                + "   * Fix centering issues in Android 4.4.\n"
                                + "   */\n"
                                + "  div[style*=\"margin: 16px 0;\"] {\n"
                                + "    margin: 0 !important;\n"
                                + "  }\n"
                                + "  body {\n"
                                + "    width: 100% !important;\n"
                                + "    height: 100% !important;\n"
                                + "    padding: 0 !important;\n"
                                + "    margin: 0 !important;\n"
                                + "  }\n"
                                + "  /**\n"
                                + "   * Collapse table borders to avoid space between cells.\n"
                                + "   */\n"
                                + "  table {\n"
                                + "    border-collapse: collapse !important;\n"
                                + "  }\n"
                                + "  a {\n"
                                + "    color: #1a82e2;\n"
                                + "  }\n"
                                + "  img {\n"
                                + "    height: auto;\n"
                                + "    line-height: 100%;\n"
                                + "    text-decoration: none;\n"
                                + "    border: 0;\n"
                                + "    outline: none;\n"
                                + "  }\n"
                                + "  </style>\n"
                                + "\n"
                                + "</head>\n"
                                + "<body style=\"background-color: #e9ecef;\">\n"
                                + "\n"
                                + "  <!-- start preheader -->\n"
                                + "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n"
                                + "    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n"
                                + "  </div>\n"
                                + "  <!-- end preheader -->\n"
                                + "\n"
                                + "  <!-- start body -->\n"
                                + "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                                + "\n"
                                + "    <!-- start logo -->\n"
                                + "    <tr>\n"
                                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n"
                                + "        <tr>\n"
                                + "        <td align=\"center\" valign=\"top\" width=\"600\">\n"
                                + "        <![endif]-->\n"
                                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                                + "          <tr>\n"
                                + "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n"
                                + "              <a href=\"https://www.blogdesire.com\" target=\"_blank\" style=\"display: inline-block;\">\n"
                                + "                <img src=\"https://www.blogdesire.com/wp-content/uploads/2019/07/blogdesire-1.png\" alt=\"Logo\" border=\"0\" width=\"48\" style=\"display: block; width: 48px; max-width: 48px; min-width: 48px;\">\n"
                                + "              </a>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "        </table>\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        </td>\n"
                                + "        </tr>\n"
                                + "        </table>\n"
                                + "        <![endif]-->\n"
                                + "      </td>\n"
                                + "    </tr>\n"
                                + "    <!-- end logo -->\n"
                                + "\n"
                                + "    <!-- start hero -->\n"
                                + "    <tr>\n"
                                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n"
                                + "        <tr>\n"
                                + "        <td align=\"center\" valign=\"top\" width=\"600\">\n"
                                + "        <![endif]-->\n"
                                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                                + "          <tr>\n"
                                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n"
                                + "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Confirm Your Email Address</h1>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "        </table>\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        </td>\n"
                                + "        </tr>\n"
                                + "        </table>\n"
                                + "        <![endif]-->\n"
                                + "      </td>\n"
                                + "    </tr>\n"
                                + "    <!-- end hero -->\n"
                                + "\n"
                                + "    <!-- start copy block -->\n"
                                + "    <tr>\n"
                                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n"
                                + "        <tr>\n"
                                + "        <td align=\"center\" valign=\"top\" width=\"600\">\n"
                                + "        <![endif]-->\n"
                                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                                + "\n"
                                + "          <!-- start copy -->\n"
                                + "          <tr>\n"
                                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                                + "              <p style=\"margin: 0;\">Tap the button below to confirm your email address. If you didn't create an account with <a href=\"https://blogdesire.com\">Paste</a>, you can safely delete this email.</p>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "          <!-- end copy -->\n"
                                + "\n"
                                + "          <!-- start button -->\n"
                                + "          <tr>\n"
                                + "            <td align=\"left\" bgcolor=\"#ffffff\">\n"
                                + "              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                                + "                <tr>\n"
                                + "                  <td align=\"center\" bgcolor=\"#ffffff\" style=\"padding: 12px;\">\n"
                                + "                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                                + "                      <tr>\n"
                                + "                        <td align=\"center\" bgcolor=\"#1a82e2\" style=\"border-radius: 6px;\">\n"
                                + "                          <a style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;\">" + code + "</a>\n"
                                + "                        </td>\n"
                                + "                      </tr>\n"
                                + "                    </table>\n"
                                + "                  </td>\n"
                                + "                </tr>\n"
                                + "              </table>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "          <!-- end button -->\n"
                                + "\n"
                                + "          <!-- start copy -->\n"
                                + "          <tr>\n"
                                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                                + "              <p style=\"margin: 0;\">If that doesn't work, copy and paste the following link in your browser:</p>\n"
                                + "              <p style=\"margin: 0;\"><a href=\"https://blogdesire.com\" target=\"_blank\">https://blogdesire.com/xxx-xxx-xxxx</a></p>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "          <!-- end copy -->\n"
                                + "\n"
                                + "          <!-- start copy -->\n"
                                + "          <tr>\n"
                                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n"
                                + "              <p style=\"margin: 0;\">Cheers,<br> Paste</p>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "          <!-- end copy -->\n"
                                + "\n"
                                + "        </table>\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        </td>\n"
                                + "        </tr>\n"
                                + "        </table>\n"
                                + "        <![endif]-->\n"
                                + "      </td>\n"
                                + "    </tr>\n"
                                + "    <!-- end copy block -->\n"
                                + "\n"
                                + "    <!-- start footer -->\n"
                                + "    <tr>\n"
                                + "      <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 24px;\">\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n"
                                + "        <tr>\n"
                                + "        <td align=\"center\" valign=\"top\" width=\"600\">\n"
                                + "        <![endif]-->\n"
                                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                                + "\n"
                                + "          <!-- start permission -->\n"
                                + "          <tr>\n"
                                + "            <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;\">\n"
                                + "              <p style=\"margin: 0;\">You received this email because we received a request for [type_of_action] for your account. If you didn't request [type_of_action] you can safely delete this email.</p>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "          <!-- end permission -->\n"
                                + "\n"
                                + "          <!-- start unsubscribe -->\n"
                                + "          <tr>\n"
                                + "            <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;\">\n"
                                + "              <p style=\"margin: 0;\">To stop receiving these emails, you can <a href=\"https://www.blogdesire.com\" target=\"_blank\">unsubscribe</a> at any time.</p>\n"
                                + "              <p style=\"margin: 0;\">Paste 1234 S. Broadway St. City, State 12345</p>\n"
                                + "            </td>\n"
                                + "          </tr>\n"
                                + "          <!-- end unsubscribe -->\n"
                                + "\n"
                                + "        </table>\n"
                                + "        <!--[if (gte mso 9)|(IE)]>\n"
                                + "        </td>\n"
                                + "        </tr>\n"
                                + "        </table>\n"
                                + "        <![endif]-->\n"
                                + "      </td>\n"
                                + "    </tr>\n"
                                + "    <!-- end footer -->\n"
                                + "\n"
                                + "  </table>\n"
                                + "  <!-- end body -->\n"
                                + "\n"
                                + "</body>\n"
                                + "</html>", "text/html");
                        Transport.send(message);
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }

                }

            } else {
                request.setAttribute("error", "Pass must contains [A-Za-z0-9] and special character and not null");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }

        } else {
            request.setAttribute("error", "Password and confirm password do not match");
            request.getRequestDispatcher("signup.jsp").forward(request, response);

        }
    }

    public static boolean isValidPassword(String password) {
        // Độ dài tối thiểu là 8 ký tự
        if (password.length() > 3 && password.length() < 8) {
            return false;
        }

        // Ít nhất một chữ cái viết hoa
        if (!Pattern.compile("[A-Z]").matcher(password).find()) {
            return false;
        }

        // Ít nhất một chữ cái thường
        if (!Pattern.compile("[a-z]").matcher(password).find()) {
            return false;
        }

        // Ít nhất một số
        if (!Pattern.compile("[0-9]").matcher(password).find()) {
            return false;
        }

        // Ít nhất một ký tự đặc biệt (có thể thay đổi dấu \\W để bao gồm các ký tự đặc biệt khác)
        if (!Pattern.compile("\\W").matcher(password).find()) {
            return false;
        }

        // Nếu mọi tiêu chí đều đáp ứng, trả về true
        return true;
    }

    public static boolean isValidUserName(String user) {
        // Độ dài tối thiểu là 8 ký tự
        if (user.length() > 3 && user.length() < 15) {
            return true;
        }

        // Nếu mọi tiêu chí đều đáp ứng, trả về true
        return false;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
