/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.vnPay;

import dal.accountDAO;
import dal.moneyDAO;
import dal.verifyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "transfer", urlPatterns = {"/transfer"})
public class transfer extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet transfer</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet transfer at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String otp1 = request.getParameter("otp1");
        String otp2 = request.getParameter("otp2");
        String otp3 = request.getParameter("otp3");
        String otp4 = request.getParameter("otp4");
        String otp5 = request.getParameter("otp5");
        String otp6 = request.getParameter("otp6");
        String otp = otp1 + otp2 + otp3 + otp4 + otp5 + otp6;
        HttpSession session = request.getSession();
        account c = (account) session.getAttribute("account");
        long balance = Integer.parseInt(c.getAddress());
        moneyDAO m = new moneyDAO();

        accountDAO a = new accountDAO();
        verifyDAO d = new verifyDAO();
        if (d.checkOTP(otp, c.getAccountid()) == true) {

            String receive = request.getParameter("findUser");
            long money = Integer.parseInt(request.getParameter("moneyTransfer").replaceAll(",", ""));

            account receive1 = a.acc(receive);
            long balanceReceive = Integer.parseInt(receive1.getAddress());

            if (money > balance) {
                request.setAttribute("error", "The balance in your account is not enough!");
                request.getRequestDispatcher("balance").forward(request, response);
            } else {
                m.insertTransfer(money, c.getAccountid(), receive1.getAccountid());
                double now = balance - money;
                double now1 = balanceReceive + money;
                a.updateMoney(now, c.getAccountid());
                a.updateMoney(now1, receive1.getAccountid());
                
                response.sendRedirect("balance");
            }

        } else {
            request.setAttribute("error", "OTP invalid , please input again!");
            request.getRequestDispatcher("balance").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
