package controller.profileUser;

import dal.accountDAO;
import dal.messageDAO;
import dal.productDAO;
import dal.productImageDAO;
import dal.ratingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.message;
import model.product;
import model.productImage;
import model.weekDay;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "profileUser", urlPatterns = {"/profileUser"})
public class profileUser extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet profileUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet profileUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        productDAO p = new productDAO();
        accountDAO a = new accountDAO();
//        String id = request.getParameter("id");
        String id = "1"; /// tam thoi = 1 
        account ac1 = a.getAccount(Integer.parseInt(id));
        List<product> list = p.getAllListByID(1);// list gia dinh

        int page, numberPage = 4;
        int size = list.size();
        int num = (size % 4 == 0 ? (size / 4) : ((size / 4) + 1));
        String xpage = request.getParameter("page");

        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int e;
        int s;

        s = (page - 1) * numberPage;

        e = Math.min(page * numberPage, size);
        List<product> products = p.getListByPage(list, s, e);

        request.setAttribute("listFeed", products);// list phan trang
        request.setAttribute("page", page);
        request.setAttribute("num", num);

        productImageDAO p1 = new productImageDAO();
        List<productImage> list1 = p1.getAllList();

        
        ratingDAO d = new ratingDAO();
        List<weekDay> list12 =  d.listStar(1);
        double avg =0;
        double number =0;
        List<String> single =new ArrayList<>();
        List<String> single1 =new ArrayList<>();
        for (weekDay day : list12) {
            avg += Integer.parseInt(day.getDay())*Integer.parseInt(day.getRevenue());
            number +=Integer.parseInt(day.getRevenue());
           String a1 = day.getRevenue();
           single.add(a1);
        }
        
        for (String ss : single) {
            if(number==0){
                single1.add("0");
            }else{
                double aaa = Math.floor((Integer.parseInt(ss)/number)*100 );
                single1.add(aaa+"");
            }
        }
        
        
        
        if(number==0){
            avg = 0;
        }else{
           avg = avg/number;
        }
        
        
        
        
  Cookie[] cookies = request.getCookies();
        String favoriteListStr = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("favorite1".equals(cookie.getName())) {
                    favoriteListStr = cookie.getValue();
                    break;
                }
            }
        }


// Phân tích dữ liệu từ chuỗi favoriteListStr, nếu có
        List<product> favoriteProducts = new ArrayList<>();
        if (favoriteListStr != null && !favoriteListStr.isEmpty()) {
            String[] productEntries = favoriteListStr.split(":");

            // Đảm bảo rằng có đủ thông tin cho mỗi sản phẩm
            for(int i = 0; i < productEntries.length; i++) {
                product p11 = p.getProductbyID(productEntries[i]);
                favoriteProducts.add(p11);

            }

        }
       

// Gửi danh sách sản phẩm yêu thích đến JSP
        request.setAttribute("favoriteProducts", favoriteProducts);

        
        
        request.setAttribute("ListImage", list1);
        request.setAttribute("star", list12);//list sao 
        request.setAttribute("avg",avg );/// trung binh sao 
        request.setAttribute("single",single1 );/// mỗi sao bn người đánh giá
        request.setAttribute("number",number );// tổng người đánh giá 
        request.setAttribute("infoUser", ac1);
        request.getRequestDispatcher("profileUser.jsp").forward(request, response);
//        request.getRequestDispatcher("feedsProfile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
