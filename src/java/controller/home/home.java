/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.accountDAO;
import dal.messageDAO;
import dal.productDAO;
import dal.productImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.message;
import model.product;
import model.productImage;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "home", urlPatterns = {"/home"})
public class home extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet home</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet home at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        productDAO p = new productDAO();
        List<product> list = p.getAllList();

        int page, numberPage = 4;
        int size = list.size();
        int num = (size % 4 == 0 ? (size / 4) : ((size / 4) + 1));
        String xpage = request.getParameter("page");

        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int e;
        int s;

        s = (page - 1) * numberPage;

        e = Math.min(page * numberPage, size);
        List<product> products = p.getListByPage(list, s, e);

        
        request.setAttribute("listFeed", products);// list phan trang
        request.setAttribute("page", page);
        request.setAttribute("num", num);

        productImageDAO p1 = new productImageDAO();
        List<productImage> list1 = p1.getAllList();

//        request.setAttribute("listFeed", list);
        request.setAttribute("ListImage", list1);

        accountDAO a = new accountDAO();
        List<account> list2 = a.getAllList();

        messageDAO m = new messageDAO();
        List<message> list3 = m.getMessage();

        HttpSession session = request.getSession();
        account userOn = (account) session.getAttribute("account");
        List<message> list4 = m.listAccountMess(userOn.getAccountid());
//        List<message> list5 = m.lastMess();



        request.setAttribute("listMessage", list2);
        request.setAttribute("listMessage1", list3);
        request.setAttribute("listMessage2", list4);
        request.setAttribute("cumtomer", userOn.getAccountid());
        
        
        
        
                request.setAttribute("moneyCustomer",a.getAccount(userOn.getAccountid()) );

  Cookie[] cookies = request.getCookies();
        String favoriteListStr = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("favorite1".equals(cookie.getName())) {
                    favoriteListStr = cookie.getValue();
                    break;
                }
            }
        }


// Phân tích dữ liệu từ chuỗi favoriteListStr, nếu có
        List<product> favoriteProducts = new ArrayList<>();
        if (favoriteListStr != null && !favoriteListStr.isEmpty()) {
            String[] productEntries = favoriteListStr.split(":");

            // Đảm bảo rằng có đủ thông tin cho mỗi sản phẩm
            for(int i = 0; i < productEntries.length; i++) {
                product p11 = p.getProductbyID(productEntries[i]);
                favoriteProducts.add(p11);

            }

        }
       

// Gửi danh sách sản phẩm yêu thích đến JSP
        request.setAttribute("favoriteProducts", favoriteProducts);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//               request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
