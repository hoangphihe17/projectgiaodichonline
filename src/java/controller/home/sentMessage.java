package controller.home;

import dal.messageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Paths;
import model.account;

@WebServlet(name = "sentMessage", urlPatterns = {"/sentMessage"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // 2 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)

public class sentMessage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet sentMessage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet sentMessage at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        messageDAO m = new messageDAO();

        try {

            HttpSession session = request.getSession();
            account userOn = (account) session.getAttribute("account");
//            int boxChat = (int) session.getAttribute("boxChatId");

            String text = request.getParameter("messageText");
            int boxChat = Integer.parseInt(request.getParameter("boxChat"));

            String realPath = request.getServletContext().getRealPath("/image");
            Files.createDirectories(Paths.get(realPath)); // Tạo thư mục nếu chưa tồn tại
            PrintWriter out = response.getWriter();

            for (Part part : request.getParts()) {
                if (part != null && part.getSubmittedFileName() != null && text != null) {
                    String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                    part.write(Paths.get(realPath, filename).toString()); // Lưu file
//                                out.print(text+"--"+filename+"--"+boxChat+"--"+userOn.getAccountid());

                    m.addMess(boxChat, userOn.getAccountid(), filename, text);
                } else if (part == null && part.getSubmittedFileName() == null && text != null) {
                                      

                    m.addMess1(boxChat, userOn.getAccountid(), text);

                } else if (part != null && part.getSubmittedFileName() != null && text == null) {
                    String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                    part.write(Paths.get(realPath, filename).toString());
                    m.addMess2(boxChat, userOn.getAccountid(), filename);

                } else {
                   out.print("nhap gi di ma");

                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        
         response.setContentType("text/plain");
       
        response.getWriter().write("Message sent successed");
        response.sendRedirect("home");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
