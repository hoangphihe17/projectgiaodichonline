/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.commentDAO;
import dal.productDAO;
import dal.productImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.comment;
import model.product;
import model.productImage;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "postDetail", urlPatterns = {"/postDetail"})
public class postDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet postDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet postDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        productDAO daop = new productDAO();
        productImageDAO dao = new productImageDAO();
        product d = daop.getProductbyID(id);
        List<productImage> list = dao.listImageByProductId(id);
        commentDAO c = new commentDAO();
        List<comment> list1 = c.getCommentByProductID(id);

        request.setAttribute("listImage", list);
        request.setAttribute("comment", list1);

        request.setAttribute("p", d);

        Cookie[] cookies = request.getCookies();
        String favoriteListStr = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("favorite1".equals(cookie.getName())) {
                    favoriteListStr = cookie.getValue();
                    break;
                }
            }
        }

// Phân tích dữ liệu từ chuỗi favoriteListStr, nếu có
        List<product> favoriteProducts = new ArrayList<>();
        if (favoriteListStr != null && !favoriteListStr.isEmpty()) {
            String[] productEntries = favoriteListStr.split(":");

            // Đảm bảo rằng có đủ thông tin cho mỗi sản phẩm
            for (int i = 0; i < productEntries.length; i++) {
                product p11 = daop.getProductbyID(productEntries[i]);
                favoriteProducts.add(p11);

            }

        }

// Gửi danh sách sản phẩm yêu thích đến JSP
        request.setAttribute("favoriteProducts", favoriteProducts);

        request.getRequestDispatcher("postDetail.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        productDAO daop = new productDAO();
        productImageDAO dao = new productImageDAO();
        product d = daop.getProductbyID(id);
        List<productImage> list = dao.listImageByProductId(id);
        commentDAO c = new commentDAO();
        List<comment> list1 = c.getCommentByProductID(id);

        request.setAttribute("listImage", list);
        request.setAttribute("comment", list1);

        request.setAttribute("p", d);
        
        
        
          Cookie[] cookies = request.getCookies();
        String favoriteListStr = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("favorite1".equals(cookie.getName())) {
                    favoriteListStr = cookie.getValue();
                    break;
                }
            }
        }

// Phân tích dữ liệu từ chuỗi favoriteListStr, nếu có
        List<product> favoriteProducts = new ArrayList<>();
        if (favoriteListStr != null && !favoriteListStr.isEmpty()) {
            String[] productEntries = favoriteListStr.split(":");

            // Đảm bảo rằng có đủ thông tin cho mỗi sản phẩm
            for (int i = 0; i < productEntries.length; i++) {
                product p11 = daop.getProductbyID(productEntries[i]);
                favoriteProducts.add(p11);

            }

        }

// Gửi danh sách sản phẩm yêu thích đến JSP
        request.setAttribute("favoriteProducts", favoriteProducts);
        request.getRequestDispatcher("postDetail.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
