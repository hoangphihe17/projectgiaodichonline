/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.productDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.product;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "bookmark", urlPatterns = {"/bookmark"})
public class bookmark extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet bookmark</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet bookmark at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String productId = request.getParameter("productId");
        productDAO p = new productDAO();
        product h = p.getProductbyID(productId);
        Cookie favoriteCookie = null;

// Tìm cookie có tên là "favorites"
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("favorite1".equals(cookie.getName())) {
                    favoriteCookie = cookie;
                    break;
                }
            }
        }

        if (favoriteCookie == null) {
            favoriteCookie = new Cookie("favorite1", "");
        }
        PrintWriter out = response.getWriter();

        String favoriteList = favoriteCookie.getValue();
        if ("add".equals(action)) {
            // Thêm sản phẩm vào cookie
            if (!favoriteList.contains(productId)) {
                // Kiểm tra xem danh sách yêu thích có rỗng không để tránh thêm dấu phẩy không cần thiết
                if (!favoriteList.isEmpty()) {
                    favoriteList += ":"; // Thêm dấu phẩy nếu danh sách không rỗng
                }
                // Thay dấu cách bằng ký tự khác hoặc xử lý giá trị sao cho không có dấu cách
                favoriteList += productId;
                if (favoriteList.matches("^[:]+.*")) {
                    favoriteList = favoriteList.replaceAll("[:]+", "");
                }
                 if (favoriteList.matches("[:]+.*")) {
                    favoriteList = favoriteList.replaceAll("[:]+", ":");
                }

                favoriteCookie.setValue(favoriteList);
//                out.print(favoriteList);

            }
        } else if ("remove".equals(action)) {
            // Xóa sản phẩm khỏi cookie
            if (favoriteList.contains(productId)) {
                favoriteList = favoriteList.replace(productId, "");
                favoriteCookie.setValue(favoriteList);
            }
        }

// Cập nhật cookie
        response.addCookie(favoriteCookie);
        response.sendRedirect("home"); // Chuyển hướng người dùng đến trang yêu thích
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
