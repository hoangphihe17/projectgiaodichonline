/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.commentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "crudComment", urlPatterns = {"/crudComment"})
public class crudComment extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet crudComment</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet crudComment at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        commentDAO d = new commentDAO();
        String comment = request.getParameter("comment");
        String productid = request.getParameter("productid");

        HttpSession session = request.getSession();
        account a = (account) session.getAttribute("account");

    
            if (comment != null && productid != null) {
                d.addComment(comment, productid, 3);

            }

        /////// update comment
        String commentid = request.getParameter("id");
        String commentUpdate = request.getParameter("commentUpdate");
        try {
            if (commentid != null && commentUpdate != null) {
                d.updateComment(commentid, commentUpdate);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        
        ////// delete comment 
        String commentidDelete = request.getParameter("deleteComment");
        try {
            d.deleteComment(commentidDelete, "admin");

        } catch (Exception e) {
            System.out.println(e);
        }
        response.sendRedirect("postDetail?id="+productid);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
