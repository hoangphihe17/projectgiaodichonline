/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.accountDAO;
import dal.messageDAO;
import dal.productDAO;
import dal.productImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.account;
import model.message;
import model.product;
import model.productImage;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "home1", urlPatterns = {"/home1"})
public class home1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet home1</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet home1 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        productDAO p = new productDAO();
        List<product> list = p.getAllList();

        productImageDAO p1 = new productImageDAO();
        List<productImage> list1 = p1.getAllList();

        request.setAttribute("listFeed", list);
        request.setAttribute("ListImage", list1);

        accountDAO a = new accountDAO();
        List<account> list2 = a.getAllList();

        messageDAO m = new messageDAO();
        List<message> list3 = m.getMessage();

        HttpSession session = request.getSession();
        account userOn = (account) session.getAttribute("account");
        List<message> list4 = m.listAccountMess(userOn.getAccountid());
//        List<message> list5 = m.lastMess();

        request.setAttribute("listMessage", list2);
        request.setAttribute("listMessage1", list3);
        request.setAttribute("listMessage2", list4);
        request.setAttribute("cumtomer", userOn.getAccountid());

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boxChatId = request.getParameter("boxChat");
        HttpSession session = request.getSession();
// Kiểm tra xem attribute "boxChatId" đã tồn tại trong session chưa
        if (session.getAttribute("boxChatId") != null) {
            // Nếu có, xóa attribute này khỏi session
//            session.removeAttribute("boxChatId");
             session.setAttribute("boxChatId", boxChatId);

        } else {
            // Nếu chưa có, tạo mới attribute này với giá trị mới
            session.setAttribute("boxChatId", boxChatId);
            
        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("BoxChat ID " + boxChatId + " saved to session");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
