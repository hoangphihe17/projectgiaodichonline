USE [master]
GO
/****** Object:  Database [swp2k3]    Script Date: 3/6/2024 1:34:11 AM ******/
CREATE DATABASE [swp2k3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'swp2k3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\swp2k3.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'swp2k3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\swp2k3_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [swp2k3] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [swp2k3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [swp2k3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [swp2k3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [swp2k3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [swp2k3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [swp2k3] SET ARITHABORT OFF 
GO
ALTER DATABASE [swp2k3] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [swp2k3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [swp2k3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [swp2k3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [swp2k3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [swp2k3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [swp2k3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [swp2k3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [swp2k3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [swp2k3] SET  ENABLE_BROKER 
GO
ALTER DATABASE [swp2k3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [swp2k3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [swp2k3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [swp2k3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [swp2k3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [swp2k3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [swp2k3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [swp2k3] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [swp2k3] SET  MULTI_USER 
GO
ALTER DATABASE [swp2k3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [swp2k3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [swp2k3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [swp2k3] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [swp2k3] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [swp2k3] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [swp2k3] SET QUERY_STORE = ON
GO
ALTER DATABASE [swp2k3] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [swp2k3]
GO
/****** Object:  Table [dbo].[account]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[address] [nvarchar](max) NULL,
	[phone] [nvarchar](50) NULL,
	[avatar] [nvarchar](max) NULL,
	[role] [varchar](50) NULL,
	[band] [varchar](50) NULL,
	[code] [varchar](50) NULL,
	[status] [nvarchar](max) NULL,
	[sortDelete] [varchar](50) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[advertise]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[advertise](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[achieved] [int] NULL,
	[goal] [int] NULL,
	[status] [nvarchar](50) NULL,
	[sortDelete] [varchar](100) NULL,
	[productId] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[boxChat]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[boxChat](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user1] [int] NULL,
	[user2] [int] NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[category]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [nvarchar](250) NULL,
	[describe] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chatCommunity]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chatCommunity](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[message] [nvarchar](max) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [varchar](max) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
	[sortDelete] [varchar](100) NULL,
	[productId] [varchar](500) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[deposit]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[deposit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[money] [money] NULL,
	[total] [money] NULL,
	[content] [nvarchar](max) NULL,
	[createAt] [varchar](1) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[keyword]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[keyword](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[search] [nvarchar](max) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[message]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[boxChatid] [int] NULL,
	[Senderid] [int] NULL,
	[image] [nvarchar](max) NULL,
	[message] [nvarchar](max) NULL,
	[sortDelete] [varchar](100) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[order]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[price] [money] NULL,
	[quantity] [int] NULL,
	[note] [nvarchar](200) NULL,
	[orderdate] [varchar](100) NULL,
	[success] [nvarchar](200) NULL,
	[sortDelete] [nvarchar](200) NULL,
	[productId] [varchar](500) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[id] [varchar](500) NOT NULL,
	[productName] [nvarchar](250) NULL,
	[fee] [varchar](100) NULL,
	[describe] [nvarchar](max) NULL,
	[infomation] [nvarchar](max) NULL,
	[like] [int] NULL,
	[view] [int] NULL,
	[quantity] [int] NULL,
	[price] [money] NULL,
	[privateInfo] [nvarchar](max) NULL,
	[publicPrivate] [nvarchar](max) NULL,
	[status] [varchar](100) NULL,
	[sortDelete] [varchar](100) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
	[categoryId] [int] NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[productImages]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productImages](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[path] [varchar](255) NULL,
	[createdAt] [varchar](2048) NULL,
	[updatedAt] [varchar](2048) NULL,
	[productId] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rating]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rating](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[starNo] [int] NULL,
	[content] [varchar](max) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
	[productId] [varchar](500) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[report]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[report](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[describe] [varchar](max) NULL,
	[createdBy] [varchar](50) NULL,
	[createAt] [varchar](100) NULL,
	[updateAt] [varchar](100) NULL,
	[sortDelete] [varchar](100) NULL,
	[orderId] [int] NULL,
	[statusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[statusReport]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statusReport](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[verify]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[verify](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CCCDFront] [nvarchar](max) NULL,
	[CCCDBack] [nvarchar](max) NULL,
	[selfie] [nvarchar](max) NULL,
	[bank] [varchar](50) NULL,
	[numberBank] [nvarchar](50) NULL,
	[otp] [varchar](50) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[withdraw]    Script Date: 3/6/2024 1:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[withdraw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[money] [money] NULL,
	[total] [money] NULL,
	[content] [nvarchar](max) NULL,
	[bank] [varchar](50) NULL,
	[numberBank] [nvarchar](50) NULL,
	[otp] [varchar](50) NULL,
	[createAt] [varchar](1) NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[productImages] ADD  DEFAULT (NULL) FOR [name]
GO
ALTER TABLE [dbo].[productImages] ADD  DEFAULT (NULL) FOR [path]
GO
ALTER TABLE [dbo].[productImages] ADD  DEFAULT (NULL) FOR [createdAt]
GO
ALTER TABLE [dbo].[productImages] ADD  DEFAULT (NULL) FOR [updatedAt]
GO
ALTER TABLE [dbo].[advertise]  WITH CHECK ADD FOREIGN KEY([productId])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[boxChat]  WITH CHECK ADD FOREIGN KEY([user1])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[boxChat]  WITH CHECK ADD FOREIGN KEY([user2])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[chatCommunity]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[comment]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[comment]  WITH CHECK ADD FOREIGN KEY([productId])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[deposit]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[keyword]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD FOREIGN KEY([boxChatid])
REFERENCES [dbo].[boxChat] ([id])
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD FOREIGN KEY([Senderid])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[order]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[order]  WITH CHECK ADD FOREIGN KEY([productId])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD FOREIGN KEY([categoryId])
REFERENCES [dbo].[category] ([id])
GO
ALTER TABLE [dbo].[productImages]  WITH CHECK ADD FOREIGN KEY([productId])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD FOREIGN KEY([productId])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[report]  WITH CHECK ADD FOREIGN KEY([orderId])
REFERENCES [dbo].[order] ([id])
GO
ALTER TABLE [dbo].[report]  WITH CHECK ADD FOREIGN KEY([statusId])
REFERENCES [dbo].[statusReport] ([id])
GO
ALTER TABLE [dbo].[verify]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[withdraw]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([id])
GO
USE [master]
GO
ALTER DATABASE [swp2k3] SET  READ_WRITE 
GO
