
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Page</title>
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');

            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
            }

            /*            body {
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            min-height: 100vh;
                            background: #17A2B8;
                        }*/

            ::selection {
                color: #fff;
                background: #17A2B8;
            }

            .s_wrapper {
                width: 400px;
                background: #fff;
                border-radius: 10px;
                padding: 10px 14px 18px;
                box-shadow: 0 12px 35px rgba(0, 0, 0, 0.1);
            }

            .s_wrapper header h2 {
                font-size: 15px;
                font-weight: 600;
            }

            .s_wrapper header p {
                margin-top: 5px;
                font-size: 10px;
            }

            .s_price-input {
                width: 100%;
                display: flex;
                margin: 11px 0 16px;
            }

            .s_price-input .s_field {
                display: flex;
                width: 100%;
                height: 45px;
                align-items: center;
            }

            .s_field input {
                width: 100%;
                height: 80%;
                outline: none;
                font-size: 15px;
                margin-left: 12px;
                border-radius: 5px;
                text-align: center;
                border: 1px solid #999;
                -moz-appearance: textfield;
            }

            input[type="number"]::-webkit-outer-spin-button,
            input[type="number"]::-webkit-inner-spin-button {
                -webkit-appearance: none;
            }

            .s_price-input .s_separator {
                width: 130px;
                display: flex;
                font-size: 19px;
                align-items: center;
                justify-content: center;
            }

            .s_slider {
                height: 5px;
                position: relative;
                background: #ddd;
                border-radius: 5px;
            }

            .s_slider .s_progress {
                height: 100%;
                left: 25%;
                right: 25%;
                position: absolute;
                border-radius: 5px;
                background: #17A2B8;
            }

            .s_range-input {
                position: relative;
            }

            .s_range-input input {
                position: absolute;
                width: 100%;
                height: 5px;
                top: -5px;
                background: none;
                pointer-events: none;
                -webkit-appearance: none;
                -moz-appearance: none;
            }

            input[type="range"]::-webkit-slider-thumb {
                height: 17px;
                width: 17px;
                border-radius: 50%;
                background: #17A2B8;
                pointer-events: auto;
                -webkit-appearance: none;
                box-shadow: 0 0 6px rgba(0, 0, 0, 0.05);
            }

            input[type="range"]::-moz-range-thumb {
                height: 17px;
                width: 17px;
                border: none;
                border-radius: 50%;
                background: #17A2B8;
                pointer-events: auto;
                -moz-appearance: none;
                box-shadow: 0 0 6px rgba(0, 0, 0, 0.05);
            }

            .s_box_search {
                position: relative;
                animation: shadow-animation 2s infinite alternate;
            }

            @keyframes shadow-animation {
                0% {
                    box-shadow: 0 0 5px #ff0000, 0 0 5px #00ff00, 0 0 5px #0000ff;
                }

                35% {
                    box-shadow: 0 0 5px #93dd11, 0 0 5px #0d346e, 0 0 5px #c12828;
                }

                70% {
                    box-shadow: 0 0 5px #4c6fd7, 0 0 5px #d38e8e, 0 0 5px #d8ce08;
                }

                100% {
                    box-shadow: 0 0 5px #ff0000, 0 0 5px #00ff00, 0 0 5px #0000ff;
                }
            }

            .s_search-container {
                position: relative;
                border-radius: 20px;
                background: #FFF;
                display: flex;
                gap: 5px; /* Khoảng cách giữa các phần tử */
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
            }

            .s_search-box {
                border: none;
                outline: none;
                padding: 10px;
                border-radius: 20px;
                width: 100%;
            }

            .s_search-btn {
                width: 137px;
                border: none;
                outline: none;
                cursor: pointer;
                border-radius: 20px;
                background-color: #00C4FF;
                color: white;
            }

            .s_toggle-btn {
                border: none;
                outline: none;
                cursor: pointer;
                background: none;
                font-size: 24px; /* Kích thước ba chấm */
            }


        </style>
    </head>
    <body>
        <div style="display: block">

            <form id="search-form" action="search" method="get">
                <div style="display: block;">
                    <div style="position: relative;width: 400px;" class="s_search-container">

                        <input type="text" class="s_search-box" name="key" placeholder="Search..." onchange="sendSearchRequest()">

                        <div class="s_toggle-btn">
                            <img style="width: 30px;margin-top: 10px;" src="image/goiy.jpg">
                        </div>

                        <button class="s_search-btn">Search</button>

                        <div style="position: absolute; left:404px;z-index: 1;border: 2px dashed green; color: black;" class="s_wrapper">
                            <header>
                                <h2>Price Range</h2>
                                <p>Use slider or enter min and max price</p>
                            </header>
                            <div class="s_price-input">
                                <div class="s_field">
                                    <span style="font-size: 15px;">Min</span>
                                    <input type="number" class="s_input-min" value="2500000">
                                </div>
                                <div class="separator">-</div>
                                <div class="s_field">
                                    <span style="font-size: 15px;">Max</span>
                                    <input type="number" class="s_input-max" value="7500000">
                                </div>
                            </div>
                            <div class="s_slider">
                                <div class="s_progress"></div>
                            </div>
                            <div class="s_range-input">
                                <input type="range" id="min-price" name="min" class="s_range-min" min="0" max="10000000" value="2500000" step="100" onchange="sendSearchRequest()">
                                <input type="range" id="max-price" name="max" class="s_range-max" min="0" max="10000000" value="7500000" step="100" onchange="sendSearchRequest()">


                            </div>
                        </div>
                    </div>





                </div>
            </form>

            <div id="searchResults" class="list_search_home" style="height: 290px;position: absolute">
                <c:forEach var="p" items="${list}">



                    <a href="postDetail?id=${p.id}" target="_blank" style="text-decoration: none; color: black;"> 

                        <div class="s_box_search" style="display: flex; width: 100%;background: #FFF;border-radius: 25px;width: 400px;padding: 8px;margin-top: 6px;">
                            <img  style="width: 35px ;height: 35px; ;margin-right: 15px;" src="image/goiy.jpg">
                            <span style="width: 70%;margin-top: 4px;"> ${p.productName} </span>
                            <p style="margin-top: 4px;" > 
                                <!--9.999.999-->
                                <fmt:formatNumber value="${p.price}" pattern="###,###.##" />

                            </p>
                        </div>
                    </a>

                </c:forEach>


            </div>



        </div>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.3.js"
        integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
        <script>


                                    function updateOverflow() {
                                        var boxSearchElements = document.querySelectorAll('.list_search_home .s_box_search');
                                        var listSearchHome = document.querySelector('.list_search_home');

                                        if (boxSearchElements.length > 5) {
                                            listSearchHome.style.overflowY = 'scroll';
                                        } else {
                                            listSearchHome.style.overflowY = 'auto';
                                        }
                                    }

                                    $(document).ready(function () {
                                        function sendSearchRequest() {
                                            var key = $('.s_search-box').val();
                                            var minPrice = $('.s_input-min').val();
                                            var maxPrice = $('.s_input-max').val();

                                            $.get('search', {key: key, min: minPrice, max: maxPrice}, function (html) {
                                                var $newListMessage2 = $(html);
                                                $('#searchResults').html($newListMessage2.find('#searchResults').html());

//                                                $('#searchResults').html(html);
                                            }).fail(function (error) {
                                                console.error('Error:', error);
                                            });
                                        }

                                        // Đính kèm sự kiện 'input' cho các trường nhập liệu để cập nhật kết quả tìm kiếm
                                        $('.s_search-box').on('input', sendSearchRequest);
                                        $('.s_input-min, .s_input-max, .s_range-min, .s_range-max').on('input', function () {
                                            // Cập nhật giá trị cho các trường nhập và thanh trượt giá
                                            var minVal = $('.s_range-min').val();
                                            var maxVal = $('.s_range-max').val();
                                            $('.s_input-min').val(minVal);
                                            $('.s_input-max').val(maxVal);
                                            sendSearchRequest();
                                        });
                                    });


                                    const rangeInput = document.querySelectorAll(".s_range-input input"),
                                            priceInput = document.querySelectorAll(".s_price-input input"),
                                            range = document.querySelector(".s_slider .s_progress");
                                    let priceGap = 1000;

                                    priceInput.forEach(input => {
                                        input.addEventListener("input", e => {
                                            let minPrice = parseInt(priceInput[0].value),
                                                    maxPrice = parseInt(priceInput[1].value);

                                            if ((maxPrice - minPrice >= priceGap) && maxPrice <= rangeInput[1].max) {
                                                if (e.target.className === "s_input-min") {
                                                    rangeInput[0].value = minPrice;
                                                    range.style.left = ((minPrice / rangeInput[0].max) * 100) + "%";
                                                } else {
                                                    rangeInput[1].value = maxPrice;
                                                    range.style.right = 100 - (maxPrice / rangeInput[1].max) * 100 + "%";
                                                }
                                            }
                                        });
                                    });

                                    rangeInput.forEach(input => {
                                        input.addEventListener("input", e => {
                                            let minVal = parseInt(rangeInput[0].value),
                                                    maxVal = parseInt(rangeInput[1].value);

                                            if ((maxVal - minVal) < priceGap) {
                                                if (e.target.className === "s_range-min") {
                                                    rangeInput[0].value = maxVal - priceGap;
                                                } else {
                                                    rangeInput[1].value = minVal + priceGap;
                                                }
                                            } else {
                                                priceInput[0].value = minVal;
                                                priceInput[1].value = maxVal;
                                                range.style.left = ((minVal / rangeInput[0].max) * 100) + "%";
                                                range.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + "%";
                                            }
                                        });
                                    });

                                    document.addEventListener('DOMContentLoaded', function () {
                                        // Chức năng toggle hiển thị wrapper
                                        document.querySelector('.s_toggle-btn').addEventListener('click', function () {
                                            var wrapper = document.querySelector('.s_wrapper');
                                            var isDisplayed = wrapper.style.display === 'block';
                                            wrapper.style.display = isDisplayed ? 'none' : 'block'; // Toggle display
                                        });

                                        // Kích ban đầu, wrapper không hiển thị
                                        document.querySelector('.s_wrapper').style.display = 'none';
                                    });



                                    window.addEventListener('DOMContentLoaded', updateOverflow);
                                    window.addEventListener('resize', updateOverflow);



                                    document.addEventListener('DOMContentLoaded', function () {
                                        var spans = document.querySelectorAll('.s_box_search span');
                                        spans.forEach(function (span) {
                                            var text = span.textContent.trim();
                                            if (text.length > 5) {
                                                span.textContent = text.substring(0, 18) + '...';
                                            }
                                        });
                                    });
        </script>


    </body>
</html>
