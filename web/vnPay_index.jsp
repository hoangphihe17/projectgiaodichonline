<%-- 
    Document   : index
    Created on : Feb 28, 2024, 10:19:19 PM
    Author     : Truong cun
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Cổng thanh toán VNPAY DEMO</title>
        <!-- Bootstrap core CSS -->
        <link href="/vnpay_jsp/assets/bootstrap.min.css" rel="stylesheet"/>
       
        <link href="../assets2/jumbotron-narrow.css" rel="stylesheet">      
        <script src="../assets/jquery-1.11.3.min.js"></script>

       

    </head>
    <body>
      
         <div class="container">
           <div class="header clearfix">

                <h3 class="text-muted">VNPAY DEMO</h3>
            </div>
                <div class="form-group">
                    <button onclick="pay()">Giao dịch thanh toán</button><br>
                </div>
                <div class="form-group">
                    <button onclick="querydr()">API truy vấn kết quả thanh toán</button><br>
                </div>
                <div class="form-group">
                    <button onclick="refund()">API hoàn tiền giao dịch</button><br>
                </div>
            <p>
                &nbsp;
            </p>
            <footer class="footer">
                <p>&copy; VNPAY 2020</p>
            </footer>
        </div> 
        <script>
             function pay() {
              window.location.href = "vnpay_pay.jsp";
            }
            function querydr() {
              window.location.href = "vnpay_querydr.jsp";
            }
             function refund() {
              window.location.href = "vnpay_refund.jsp";
            }
        </script>
    </body>
</html>
