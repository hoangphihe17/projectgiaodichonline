
    
    
    
      document.querySelector('.feeds').addEventListener('click', function(event) {
        // Check if the privacy toggle or back arrow is clicked and find its closest feed container
        if (event.target.matches('.privacy_123') || event.target.closest('.privacy_123')) {
            const mainContainer = event.target.closest('.feed').querySelector('.container_123');
            mainContainer.classList.add('active');
        } else if (event.target.matches('.arrow-back_123') || event.target.closest('.arrow-back_123')) {
            const mainContainer = event.target.closest('.feed').querySelector('.container_123');
            mainContainer.classList.remove('active');
        }

        // Handle audience list item clicks
        if (event.target.matches('.list_123 li') || event.target.closest('.list_123 li')) {
            const listItem = event.target.matches('.list_123 li') ? event.target : event.target.closest('.list_123 li');
            const listItems = listItem.parentNode.querySelectorAll('li');

            // Remove active class from all and add to clicked one
            listItems.forEach(item => item.classList.remove('active'));
            listItem.classList.add('active');

            // Update privacy indicator based on selection
            updatePrivacyIndicator(listItem);
        }

    });
  
  
  
 $(".upload-area_123").click(function(){
        // Tìm input tương ứng với phần tử được nhấp vào
        var input = $(this).closest('.upload-container').find('input[name="uFileImage"]');
        // Kích hoạt input
        input.trigger('click');
    });

    $('input[name="uFileImage"]').change(function(event) {
        if (event.target.files) {
            let filesAmount = event.target.files.length;
            // Tìm phần tử ".upload-img_123" tương ứng
            var uploadImgContainer = $(event.target).closest('.upload-container').find('.upload-img_123');
            uploadImgContainer.html(""); // Xóa nội dung cũ

            const validExtensions = ['image/jpeg', 'image/jpg', 'image/png'];

            for (let i = 0; i < filesAmount; i++) {
                const file = event.target.files[i];
                if (validExtensions.includes(file.type)) {
                    let reader = new FileReader();

                    reader.onload = function (event) {
//                         console.log(`File ` + event.target.result +` dumaaaa.`);
                        let html = `
                            <div class="uploaded-img">
                                <img src="${event.target.result}">
                                <button type="button" class="remove-btn">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        `;
                        uploadImgContainer.append(html); // Thêm ảnh vào phần tử ".upload-img_123"
                    };

                    reader.readAsDataURL(file);
                } else {
                    // Alert if the file is not an image
                    alert(`File "${file.name}" is not a valid image.`);
                }
            }

            // Cập nhật số lượng file đã upload
            $(event.target).siblings('.upload-info_123').find('.upload-info-value_123').text(filesAmount);
        }
    });

    // Xử lý sự kiện xóa ảnh
    $(document).on('click', '.remove-btn', function(event){
        $(this).closest('.uploaded-img').remove();
    });




