
document.querySelectorAll('.heartIcon').forEach(function (heartIcon) {
    heartIcon.addEventListener('click', function () {
        const heartIconInner = heartIcon.querySelector('i');

        // Kiểm tra và chuyển đổi class
        if (heartIconInner.classList.contains('uil-heart')) {
            heartIconInner.classList.remove('uil-heart');
            heartIconInner.classList.add('fa', 'fa-solid', 'fa-heart');
        } else {
            heartIconInner.classList.remove('fa', 'fa-solid', 'fa-heart');
            heartIconInner.classList.add('uil-heart');
        }

    });
});


document.querySelectorAll('.bookmark').forEach(function (bookmarkIcon) {
    bookmarkIcon.addEventListener('click', function (event) {
        event.preventDefault(); // Ngăn chặn hành vi mặc định của nút
        
        const bookmarkIconInner = bookmarkIcon.querySelector('i');
        const productId = bookmarkIcon.getAttribute('data-product-id');
        
        // Kiểm tra và chuyển đổi class
        if (bookmarkIconInner.classList.contains('uil-bookmark')) {
            bookmarkIconInner.classList.remove('uil-bookmark');
            bookmarkIconInner.classList.add('fa', 'fa-solid', 'fa-bookmark');
            createToast_123('success', 'Thêm vào yêu thích thành công !');
            // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
            updateBookmarkStatus(productId, 'add');
        } else {
            bookmarkIconInner.classList.remove('fa', 'fa-solid', 'fa-bookmark');
            bookmarkIconInner.classList.add('uil-bookmark');
            createToast_123('success', 'Đã xóa khỏi yêu thích');
            // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
            updateBookmarkStatus(productId, 'remove');
        }
    });
});

function updateBookmarkStatus(productId, action) {
    // Thực hiện yêu cầu AJAX để cập nhật trạng thái trên máy chủ
    // Ví dụ:
    fetch('bookmark?action=' + action + '&productId=' + productId)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            // Xử lý kết quả trả về từ máy chủ (nếu cần)
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
        });
}




//document.querySelectorAll('.bookmark').forEach(function (bookmarkIcon) {
//    bookmarkIcon.addEventListener('click', function () {
//        const bookmarkIconInner = bookmarkIcon.querySelector('i');
// const productId = bookmarkIcon.getAttribute('data-product-id');
//        // Kiểm tra và chuyển đổi class
//        if (bookmarkIconInner.classList.contains('uil-bookmark')) {
//            bookmarkIconInner.classList.remove('uil-bookmark');
//            bookmarkIconInner.classList.add('fa', 'fa-solid', 'fa-bookmark');
//            createToast_123('success', 'Thêm vào yêu thích thành công !');
//             window.location.href = "bookmark?action=add&productId=" + productId;
//        } else {
//            bookmarkIconInner.classList.remove('fa', 'fa-solid', 'fa-bookmark');
//            bookmarkIconInner.classList.add('uil-bookmark');
//            createToast_123('success', 'Đã xóa khỏi yêu thích');
//             window.location.href = "bookmark?action=remove&productId=" + productId;
//
//        }
//
//    });
//});




document.addEventListener('DOMContentLoaded', function () {



    const editButtons_123 = document.querySelectorAll('.feeds .feed .head span');

// Sự kiện click để hiển thị hoặc ẩn .wrapper_111 khi nhấp vào .edit
    editButtons_123.forEach(function (editButton) {
        editButton.addEventListener('click', function (event) {
            const wrapper_111 = event.currentTarget.closest('.feed').querySelector('.wrapper_111');
            if (wrapper_111.style.display === 'block') {
                wrapper_111.style.display = 'none';
            } else {
                wrapper_111.style.display = 'block';
                const copy = wrapper_111.querySelector('.copyProduct');
                const copyButton = wrapper_111.querySelector('.copyProduct');
                const contentInput = wrapper_111.querySelector('input[name=copyProduct]');

                copyButton.addEventListener('click', function () {
                    // Chọn toàn bộ nội dung trong input
                    contentInput.select();

                    // Sử dụng API Clipboard mới để sao chép nội dung
                    navigator.clipboard.writeText(contentInput.value).then(function () {
                        console.log('Nội dung đã được sao chép');
                    }).catch(function (error) {
                        console.error('Lỗi khi sao chép: ', error);
                    });
                });



            }
            // Ngừng sự kiện click từ lan truyền lên để tránh bấm vào nơi khác đóng ngay lập tức
            event.stopPropagation();
        });
    });






// SIDEBAR
    const menuItems = document.querySelectorAll('.menu-item');
    const messages = document.querySelector('.messages');
    const message = document.querySelectorAll('.message');
    const messageSearch = document.querySelector('#message-search');


//theme
    const theme = document.querySelector('#theme');
    const themeModal = document.querySelector('.customize-theme');
    const dm = document.querySelector('#dm');

    const messagesNotification = document.querySelector
            ('#messagse-notification');


    const changeActiveItem = () => {
        menuItems.forEach(item => {

            item.classList.remove('active');

        });
    };


    menuItems.forEach(item => {
        item.addEventListener('click', () => {
            changeActiveItem();
            item.classList.add('active');
            if (item.id !== 'notifications') {

                document.querySelector('.notification-popup').style.display = 'none';

            } else {
                document.querySelector('.notification-popup').style.display = 'block';
                document.querySelector('#notifications .notification-count').style.display = 'none';

                if (item.id !== 'theme') {
                    themeModal.style.display = 'none';

                }

            }

        });
    });

    theme.addEventListener('click', () => {

        themeModal.style.display = 'grid';


    });

    dm.addEventListener('click', () => {

        themeModal.style.display = 'none';


    });

    // searches chats
    const searchMessage = () => {
        const val = messageSearch.value.toLowerCase();
        message.forEach(user => {
            let name = user.querySelector('h5').textContent.toLowerCase();
            if (name.indexOf(val) !== -1) {
                user.style.display = 'flex';
            } else {

                user.style.display = 'none';
            }
        });
    };

// search chat
    messageSearch.addEventListener('keyup', searchMessage);





// hightlight messages card when messages menu item is clicked

    messagesNotification.addEventListener('click', () => {
        messages.style.boxShadow = '0 0 1rem var(--color-primary) ';
        ///    messages.style.boxShadow = '0 0 1rem yellow';

        messagesNotification.querySelector('.notification-count').style.display = 'none';

        setTimeout(() => {
            messages.style.boxShadow = 'none';
        }, 2000);
    });



    var root = document.documentElement;
    const fontSizes = document.querySelectorAll('.choose-size span');

    const changeActiveSize = () => {
        fontSizes.forEach(item => {


            item.classList.remove('active');


        });
    };



    /// ------font-----------




    fontSizes.forEach(size => {
        let fontSize;
        size.addEventListener('click', () => {
            if (size.classList.contains('font-size-1')) {
                changeActiveSize();

                size.classList.add('active');

                fontSize = '12px';
                root.style.setProperty('--sticky-top-left', '5.4rem');
                root.style.setProperty('--sticky-top-right', '5.4rem');
            } else if (size.classList.contains('font-size-2')) {
                changeActiveSize();

                size.classList.add('active');

                fontSize = '14px';
                root.style.setProperty('--sticky-top-left', '5.4rem');
                root.style.setProperty('--sticky-top-right', '-7rem');
            } else if (size.classList.contains('font-size-3')) {
                changeActiveSize();

                size.classList.add('active');

                fontSize = '16px';
                root.style.setProperty('--sticky-top-left', '-2rem');
                root.style.setProperty('--sticky-top-right', '-17rem');
            } else if (size.classList.contains('font-size-4')) {
                changeActiveSize();

                size.classList.add('active');

                fontSize = '18px';
                root.style.setProperty('--sticky-top-left', '-5rem');
                root.style.setProperty('--sticky-top-right', '-25rem');
            } else if (size.classList.contains('font-size-5')) {
                fontSize = '20px';
                changeActiveSize();

                size.classList.add('active');

                root.style.setProperty('--sticky-top-left', '-12rem');
                root.style.setProperty('--sticky-top-right', '-35rem');
            }

            // Change font size
            document.querySelector('html').style.fontSize = fontSize;
        });
    });



/// ------color-----------

    const colorPalette = document.querySelectorAll('.choose-color span');

    const changActiveColor = () => {
        colorPalette.forEach(colorPick => {
            colorPick.classList.remove('active');
        });
    };


    const box1 = document.querySelector('.container .left');
    const box2 = document.querySelectorAll('.container .middle .stories .story');
    const box3 = document.querySelector('.container .middle .create-post');
    const box4 = document.querySelectorAll('.container .middle .feeds .feed');
    const messages1 = document.querySelector('.messages');
    const box6 = document.querySelector('.container .right .friend-requests .request');
    const box7 = document.querySelector('.body .headd .search-bar');
    const box8 = document.querySelector('.headd .create .profile-photo');



// change primary colors
    colorPalette.forEach(color => {
        color.addEventListener('click', () => {
            let primaryHue;
            changActiveColor();
            if (color.classList.contains('color-1')) {
                primaryHue = 252;
            } else if (color.classList.contains('color-2')) {
                primaryHue = 52;
            } else if (color.classList.contains('color-3')) {
                primaryHue = 352;
            } else if (color.classList.contains('color-4')) {
                primaryHue
                        = 152;
            } else if (color.classList.contains('color-5')) {
                primaryHue = 202;
            }
            color.classList.add('active');

            root.style.setProperty('--primary-color-hue', primaryHue);



            box2.forEach(ok => {
                ok.style.boxShadow = '0 0 1rem var(--color-primary) ';
            });


            box4.forEach(ok => {
                ok.style.boxShadow = '0 0 1rem var(--color-primary) ';

            });


            box1.style.boxShadow = '0 0 1rem var(--color-primary) ';

            box3.style.boxShadow = '0 0 1rem var(--color-primary) ';

            messages1.style.boxShadow = '0 0 1rem var(--color-primary) ';
            box6.style.boxShadow = '0 0 1rem var(--color-primary) ';
            box7.style.boxShadow = '0 0 1rem var(--color-primary) ';
            box8.style.boxShadow = '0 0 1rem var(--color-primary) ';



            setTimeout(() => {
                box1.style.boxShadow = 'none';
                // box2.style.boxShadow = 'none';
                box3.style.boxShadow = 'none';
                // box4.style.boxShadow = 'none';
                messages1.style.boxShadow = 'none';
                box6.style.boxShadow = 'none';
                box7.style.boxShadow = 'none';
                box8.style.boxShadow = 'none';

                box2.forEach(ok => {
                    ok.style.boxShadow = 'none';
                });


                box4.forEach(ok => {
                    ok.style.boxShadow = 'none';

                });
            }, 2000);


        });
    });



////==== change backgound



    const bg1 = document.querySelector('.bg-1');
    const bg2 = document.querySelector('.bg-2');
    const bg3 = document.querySelector('.bg-3');



    let lightColorLightness;
    let whiteColorLightness;
    let darkColorLightness;

    const changeBG = () => {
        root.style.setProperty('--light-color-lightness', lightColorLightness);
        root.style.setProperty('--white-color-lightness', whiteColorLightness);
        root.style.setProperty('--dark-color-lightness', darkColorLightness);

    };



    bg1.addEventListener('click', () => {

        darkColorLightness = '95%';
        whiteColorLightness = '20%';
        lightColorLightness = '15%';
        // add active class
        bg1.classList.add('active');
        // remove active class from the others
        bg2.classList.remove('active');
        bg3.classList.remove('active');
        changeBG();
        window.location.reload();
    });


    bg2.addEventListener('click', () => {

        darkColorLightness = '95%';
        whiteColorLightness = '20%';
        lightColorLightness = '15%';

        // add active class
        bg2.classList.add('active');
        // remove active class from the others
        bg1.classList.remove('active');
        bg3.classList.remove('active');
        changeBG();

    });

    bg3.addEventListener('click', () => {
        darkColorLightness = '95%';
        whiteColorLightness = '10%';
        lightColorLightness = '0%';

        // add active class
        bg3.classList.add('active');
        // remove active class from others
        bg1.classList.remove('active');
        bg2.classList.remove('active');
        changeBG();
    });



    const Chossemessage = document.querySelectorAll('.khang');
    const box_chat = document.querySelector('.box_chat');
    const card = document.querySelector('.card');

    Chossemessage.forEach(ok => {
        ok.addEventListener('click', () => {
            if (box_chat.style.display === 'none' || box_chat.style.display === '') {
                box_chat.style.display = 'block';
                card.style.boxShadow = '0 0 1rem var(--color-primary)';
            } else {
                box_chat.style.display = 'none';
                card.style.boxShadow = 'none';
            }

            setTimeout(() => {
                card.style.boxShadow = 'none';
            }, 2000);
        });
    });




    const container = document.querySelector(".postBox .container123"),
            privacy = container.querySelector(".post .privacy"),
            arrowBack = container.querySelector(".audience .arrow-back");

    privacy.addEventListener("click", () => {
        container.classList.add("active");
    });

    arrowBack.addEventListener("click", () => {
        container.classList.remove("active");
    });



    const chooseActive = document.querySelectorAll(".audience .list1 li");



    const removeActive = () => {
        chooseActive.forEach(item => {
            item.classList.remove('active');
        });
    };


    changeIcon = container.querySelectorAll(".post .privacy i");

    chooseActive.forEach(size => {
        size.addEventListener("click", () => {
            removeActive(); // Gọi hàm removeActive trước khi thêm class 'active' cho phần tử mới
            size.classList.add('active');
            const existingClasses = Array.from(changeIcon[0].classList);

            // Xóa tất cả các class trước khi thêm class mới
            existingClasses.forEach(className => {
                changeIcon[0].classList.remove(className);
            });

            haha();

        });
    });

    changeText = container.querySelector(".post .privacy span");
    huhu = container.querySelector(".postBox .container123 .wrapper123 .post .privacy input");



// Thêm đoạn mã JavaScript để xử lý sự kiện onsubmit của form
    function submitForm() {
        var publicPrivateValue = document.getElementById("publicPrivateSpan").innerText.trim();
        document.getElementById("publicPrivateInput").value = publicPrivateValue;
        return true; // Tiếp tục gửi form
    }



    const haha = () => {
        chooseActive.forEach((item, index) => {

            if (item.classList.contains('active') && index === 0) {
                changeIcon[0].classList.add('fas', 'fa-globe');
                changeText.innerText = 'Public';
                huhu.value = 'Public';

            } else if (item.classList.contains('active') && index === 1) {
                changeIcon[0].classList.add('fas', 'fa-user-friends');
                changeText.innerText = 'Friends';
                huhu.value = 'Friends';
            } else if (item.classList.contains('active') && index === 2) {
                changeIcon[0].classList.add('fas', 'fa-user');
                changeText.innerText = 'Specific';
                huhu.value = 'Specific';

            } else if (item.classList.contains('active') && index === 3) {
                changeIcon[0].classList.add('fas', 'fa-lock');
                changeText.innerText = 'Private';
                huhu.value = 'Private';

            } else if (item.classList.contains('active') && index === 4) {

                changeIcon[0].classList.add('fas', 'fa-cog');
                changeText.innerText = 'Custom';
                huhu.value = 'Custom';
            }

        });
    };








    const timePostElements = document.querySelectorAll('.container .middle .feeds .feed .head .user .ingo small:nth-child(3)');
    const timePostInputs = document.querySelectorAll('.container .middle .feeds .feed .head .user .ingo input');

    var now = new Date();

    const timeOfPost = () => {
        for (let i = 0; i < timePostElements.length; i++) {
            const postedDate = new Date(timePostInputs[i].value);
            const timeDifference = Math.abs(now - postedDate);
            const timeInSeconds = timeDifference / 1000;

            if (timeInSeconds < 60) {
                timePostElements[i].innerHTML = Math.floor(timeInSeconds) + " second ago";
            } else if (timeInSeconds < 3600) {
                const minutes = Math.floor(timeInSeconds / 60);
                timePostElements[i].innerHTML = minutes + "  minutes ago";
            } else if (timeInSeconds < 86400) {
                const hours = Math.floor(timeInSeconds / 3600);
                timePostElements[i].innerHTML = hours + " hours ago";
            } else {
                const days = Math.floor(timeInSeconds / 86400);
                timePostElements[i].innerHTML = days + " days ago";
            }
        }
    };

    timeOfPost();











//                                    const heartIcons = document.querySelectorAll('.heartIcon');
//
//                                    heartIcons.forEach(function (heartIcon) {
//                                        let isLiked = false;
//
//                                        heartIcon.addEventListener('click', function () {
//                                            const heartIconInner = heartIcon.querySelector('i');
//
//                                            if (isLiked) {
//                                                // Khi đã like, chuyển về biểu tượng ban đầu
//
//                                                heartIcon.innerHTML = '<i class="uil uil-heart"></i>';
//                                                isLiked = false;
//                                            } else {
//                                                // Khi chưa like, hiển thị biểu tượng heart mới
//
//                                                heartIcon.innerHTML = '<i class="fa-solid fa-heart"></i>';
//                                                isLiked = true;
//                                            }
//                                        });
//                                    });


//                                    const bookMark = document.querySelectorAll('.bookmark');
//
//                                    bookMark.forEach(function (heartIcon) {
//                                        let isLiked = false;
//
//                                        heartIcon.addEventListener('click', function () {
//                                            const heartIconInner = heartIcon.querySelector('i');
//
//                                            if (isLiked) {
//                                                // Khi đã like, chuyển về biểu tượng ban đầu
//
//                                                heartIcon.innerHTML = '<i class="uil uil-bookmark"></i>';
//                                                isLiked = false;
//                                            } else {
//                                                // Khi chưa like, hiển thị biểu tượng heart mới
//
//                                                heartIcon.innerHTML = '<i class="fa-solid fa-bookmark"></i>';
//                                                isLiked = true;
//                                            }
//                                        });
//                                    });







    function sendMessageId(boxChatId) {
        $.ajax({
            url: "/swp391/home1",
            type: "post",
            data: {
                boxChat: boxChatId
            },
            success: function (data) {
                // Xử lý dữ liệu nhận được từ server tại đây
                console.log(data);
            },
            error: function (xhr) {
                // Xử lý lỗi tại đây
                console.error("Error: ", xhr);
            }
        });
    }



//
//const editButtons = document.querySelectorAll('.feeds .feed .head span');
//
//// Sự kiện click để hiển thị hoặc ẩn .wrapper_111 khi nhấp vào .edit
//editButtons.forEach(function (editButton) {
//    editButton.addEventListener('click', function (event) {
//        const wrapper_111 = event.currentTarget.closest('.feed').querySelector('.wrapper_111');
//        if (wrapper_111.style.display === 'block') {
//            wrapper_111.style.display = 'none';
//        } else {
//            wrapper_111.style.display = 'block';
//        }
//        // Ngừng sự kiện click từ lan truyền lên để tránh bấm vào nơi khác đóng ngay lập tức
//        event.stopPropagation();
//    });
//});
//








});







$(document).ready(function () {

    $('#action_menu_btn').click(function () {
        $('.action_menu').toggle();
    });

    $(".upload-area").click(function () {
        $('#upload-input').trigger('click');
    });

    $('#upload-input').change(event => {
        if (event.target.files) {
            let filesAmount = event.target.files.length;
            $('.upload-img').html("");
            const validExtensions = ['image/jpeg', 'image/jpg', 'image/png'];

            for (let i = 0; i < filesAmount; i++) {
                const file = event.target.files[i];
                if (validExtensions.includes(file.type)) {
                    let reader = new FileReader();

                    reader.onload = function (event) {
                        let html = `
                          <div class="uploaded-img">
                              <img src="${event.target.result}">
                              <button type="button" class="remove-btn">
                                  <i class="fas fa-times"></i>
                              </button>
                          </div>
                      `;
                        $(".upload-img").append(html);
                    };

                    reader.readAsDataURL(file);
                } else {
                    // Alert if the file is not an image
                    alert(`File "${file.name}" is not a valid image.`);
                }
            }

            $('.upload-info-value').text(filesAmount);
            $('.upload-img').css('padding', "20px");
        }
    });

    $(window).click(function (event) {
        if ($(event.target).hasClass('remove-btn')) {
            $(event.target).parent().remove();
        } else if ($(event.target).parent().hasClass('remove-btn')) {
            $(event.target).parent().parent().remove();
        }
    });








});





// display post

const createPost = document.querySelector('.create-post');
const postBox = document.querySelector('.postBox');

createPost.addEventListener('click', () => {


    $('.postBox').toggle();/// bật tắt kiểu display : none or block

    postBox.style.boxShadow = '0 0 1rem var(--color-primary) ';


});


const postOf = document.querySelector('.postBox .container123 .wrapper123 .post .post_of');

postOf.addEventListener('click', () => {


    $('.postBox').toggle();/// bật tắt kiểu display : none or block

    // postBox.style.boxShadow = '0 0 1rem var(--color-primary) ';
    ///    messages.style.boxShadow = '0 0 1rem yellow';

    // setTimeout(() => {
    //     postBox.style.boxShadow = 'none';
    // }, 2000);

});








//------------- post













CKEDITOR.ClassicEditor.create(document.getElementById("editor"), {
    toolbar: {
        items: [
            'exportPDF', 'exportWord', '|',
            'findAndReplace', 'selectAll', '|',
            'heading', '|',
            'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
            'bulletedList', 'numberedList', 'todoList', '|',
            'outdent', 'indent', '|',
            'undo', 'redo',
            '-',
            'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
            'alignment', '|',
            'link', 'uploadImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', 'htmlEmbed', '|',
            'specialCharacters', 'horizontalLine', 'pageBreak', '|',
            'textPartLanguage', '|',
            'sourceEditing'
        ],
        shouldNotGroupWhenFull: true
    },
    // Changing the language of the interface requires loading the language file using the <script> tag.
    // language: 'es',
    list: {
        properties: {
            styles: true,
            startIndex: true,
            reversed: true
        }
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
    heading: {
        options: [
            {model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph'},
            {model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1'},
            {model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2'},
            {model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3'},
            {model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4'},
            {model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5'},
            {model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6'}
        ]
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
    placeholder: 'Welcome to CKEditor 5!',
    // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
    fontFamily: {
        options: [
            'default',
            'Arial, Helvetica, sans-serif',
            'Courier New, Courier, monospace',
            'Georgia, serif',
            'Lucida Sans Unicode, Lucida Grande, sans-serif',
            'Tahoma, Geneva, sans-serif',
            'Times New Roman, Times, serif',
            'Trebuchet MS, Helvetica, sans-serif',
            'Verdana, Geneva, sans-serif'
        ],
        supportAllValues: true
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
    fontSize: {
        options: [10, 12, 14, 'default', 18, 20, 22],
        supportAllValues: true
    },
    // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
    // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
    htmlSupport: {
        allow: [
            {
                name: /.*/,
                attributes: true,
                classes: true,
                styles: true
            }
        ]
    },
    // Be careful with enabling previews
    // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
    htmlEmbed: {
        showPreviews: true
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
    link: {
        decorators: {
            addTargetToExternalLinks: true,
            defaultProtocol: 'https://',
            toggleDownloadable: {
                mode: 'manual',
                label: 'Downloadable',
                attributes: {
                    download: 'file'
                }
            }
        }
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
    mention: {
        feeds: [
            {
                marker: '@',
                feed: [
                    '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                    '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                    '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                    '@sugar', '@sweet', '@topping', '@wafer'
                ],
                minimumCharacters: 1
            }
        ]
    },

    removePlugins: [

        'AIAssistant',
        'CKBox',
        'CKFinder',
        'EasyImage',

        'RealTimeCollaborativeComments',
        'RealTimeCollaborativeTrackChanges',
        'RealTimeCollaborativeRevisionHistory',
        'PresenceList',
        'Comments',
        'TrackChanges',
        'TrackChangesData',
        'RevisionHistory',
        'Pagination',
        'WProofreader',

        'MathType',

        'SlashCommand',
        'Template',
        'DocumentOutline',
        'FormatPainter',
        'TableOfContents',
        'PasteFromOfficeEnhanced',
        'CaseChange'
    ]
});





//const messOf = document.querySelector('.card .msg_head .mess_Of');
//
//messOf.addEventListener('click', () => {
//
//
//    $('.box_chat').toggle();
//
//
//});


document.addEventListener('DOMContentLoaded', function () {
    const messOf = document.querySelector('.card .msg_head .mess_Of');
    if (messOf) {
        messOf.addEventListener('click', function () {
            $('.box_chat').toggle();
        });
    } else {
        console.error('Element not found');
    }
});










