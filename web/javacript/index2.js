//document.addEventListener('DOMContentLoaded', function () {
//    const feedsContainer = document.querySelector('.feeds');
//    if (feedsContainer) {
//        feedsContainer.addEventListener('click', function (event) {
//            // Simplified handling by targeting closer elements directly
//            let mainContainer;
//            if (event.target.matches('.privacy_123, .privacy_123 *')) {
//                mainContainer = event.target.closest('.feed').querySelector('.container_123');
//                mainContainer.classList.toggle('active', event.target.closest('.privacy_123') !== null);
//            } else if (event.target.matches('.arrow-back_123, .arrow-back_123 *')) {
//                mainContainer = event.target.closest('.feed').querySelector('.container_123');
//                mainContainer.classList.remove('active');
//            }
//
//            if (event.target.matches('.list_123 li, .list_123 li *')) {
//                const listItem = event.target.closest('.list_123 li');
//                listItem.parentNode.querySelectorAll('li').forEach(item => item.classList.remove('active'));
//                listItem.classList.add('active');
//                updatePrivacyIndicator(listItem);
//            }
//
//            if (event.target.matches('.upload-area_123, .upload-area_123 *')) {
//                event.target.closest('.upload-area_123').querySelector('.upload-input_123').click();
//            }
//
//            // This remove button event is now exclusively handled here
//            if (event.target.matches('.remove-btn, .remove-btn *')) {
//                event.target.closest('.uploaded-img').remove();
//            }
//        });
//    }
//
//    function updatePrivacyIndicator(listItem) {
//        // Function implementation remains unchanged
//    }
//});
//
//
//


    
    
    
      document.querySelector('.feeds').addEventListener('click', function(event) {
        // Check if the privacy toggle or back arrow is clicked and find its closest feed container
        if (event.target.matches('.privacy_123') || event.target.closest('.privacy_123')) {
            const mainContainer = event.target.closest('.feed').querySelector('.container_123');
            mainContainer.classList.add('active');
        } else if (event.target.matches('.arrow-back_123') || event.target.closest('.arrow-back_123')) {
            const mainContainer = event.target.closest('.feed').querySelector('.container_123');
            mainContainer.classList.remove('active');
        }

        // Handle audience list item clicks
        if (event.target.matches('.list_123 li') || event.target.closest('.list_123 li')) {
            const listItem = event.target.matches('.list_123 li') ? event.target : event.target.closest('.list_123 li');
            const listItems = listItem.parentNode.querySelectorAll('li');

            // Remove active class from all and add to clicked one
            listItems.forEach(item => item.classList.remove('active'));
            listItem.classList.add('active');

            // Update privacy indicator based on selection
            updatePrivacyIndicator(listItem);
        }

//        // Handle click for image upload
//        if (event.target.matches('.upload-area_123') || event.target.closest('.upload-area_123')) {
//            event.target.closest('.upload-area_123').querySelector('.upload-input_123').click();
//        }
//
//        // Handle remove image click
//        if (event.target.matches('.remove-btn, .remove-btn *')) {
//            const removeButton = event.target.matches('.remove-btn') ? event.target : event.target.closest('.remove-btn');
//            removeButton.parentNode.remove();
//        } 15/02/2024
    });
    
//
//
//$(document).ready(function () {
//    // Handle upload-related interactions with jQuery for consistency
//    $('body').on('change', '.upload-input', function () {
//        const container = $(this).closest('.upload-container');
//        const files = this.files;
//        const fileCount = files.length;
//        let allowedExtensions = ['image/jpeg', 'image/jpg', 'image/png'];
//
//        container.find('.upload-img_123').empty(); // Use jQuery method for consistency
//
//        for (let i = 0; i < fileCount; i++) {
//            let file = files[i];
//            if (allowedExtensions.includes(file.type)) {
//                let reader = new FileReader();
//                reader.onload = function (e) {
//                    let imgMarkup = `
//                        <div class="uploaded-img">
//                            <img src="${e.target.result}">
//                            <button type="button" class="remove-btn">
//                                <i class="fas fa-times"></i>
//                            </button>
//                        </div>
//                    `;
//                    container.find('.upload-img_123').append(imgMarkup);
//                };
//                reader.readAsDataURL(file);
//            } else {
//                alert(`File "${file.name}" is not a valid image.`);
//            }
//        }
//
//        container.find('.upload-info-value_123').text(fileCount);
//    });
//});





      
//
//
//
//const heartIcons = document.querySelectorAll('.heartIcon');
//
//heartIcons.forEach(function (heartIcon) {
//    let isLiked = false;
//
//    heartIcon.addEventListener('click', function () {
////    const heartIconInner = heartIcon.querySelector('i');
//
//        if (isLiked) {
//            // Khi đã like, chuyển về biểu tượng ban đầu
//
//            heartIcon.innerHTML = '<i class="uil uil-heart"></i>';
//            isLiked = false;
//        } else {
//            // Khi chưa like, hiển thị biểu tượng heart mới
//
//            heartIcon.innerHTML = '<i class="fa-solid fa-heart"></i>';
//            isLiked = true;
//        }
//    });
//});
//
//
//
//
//const bookMark = document.querySelectorAll('.bookmark');
//
//bookMark.forEach(function (heartIcon) {
//    let isLiked = false;
//
//    heartIcon.addEventListener('click', function () {
////    const heartIconInner = heartIcon.querySelector('i');
//
//        if (isLiked) {
//            // Khi đã like, chuyển về biểu tượng ban đầu
//
//            heartIcon.innerHTML = '<i class="uil uil-bookmark"></i>';
//            isLiked = false;
//        } else {
//            // Khi chưa like, hiển thị biểu tượng heart mới
//
//            heartIcon.innerHTML = '<i class="fa-solid fa-bookmark"></i>';
//            isLiked = true;
//        }
//    });
//});
//    



 $(".upload-area_123").click(function(){
        // Tìm input tương ứng với phần tử được nhấp vào
        var input = $(this).closest('.upload-container').find('input[name="uFileImage"]');
        // Kích hoạt input
        input.trigger('click');
    });

    $('input[name="uFileImage"]').change(function(event) {
        if (event.target.files) {
            let filesAmount = event.target.files.length;
            // Tìm phần tử ".upload-img_123" tương ứng
            var uploadImgContainer = $(event.target).closest('.upload-container').find('.upload-img_123');
            uploadImgContainer.html(""); // Xóa nội dung cũ

            const validExtensions = ['image/jpeg', 'image/jpg', 'image/png'];

            for (let i = 0; i < filesAmount; i++) {
                const file = event.target.files[i];
                if (validExtensions.includes(file.type)) {
                    let reader = new FileReader();

                    reader.onload = function (event) {
//                         console.log(`File ` + event.target.result +` dumaaaa.`);
                        let html = `
                            <div class="uploaded-img">
                                <img src="${event.target.result}">
                                <button type="button" class="remove-btn">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        `;
                        uploadImgContainer.append(html); // Thêm ảnh vào phần tử ".upload-img_123"
                    };

                    reader.readAsDataURL(file);
                } else {
                    // Alert if the file is not an image
                    alert(`File "${file.name}" is not a valid image.`);
                }
            }

            // Cập nhật số lượng file đã upload
            $(event.target).siblings('.upload-info_123').find('.upload-info-value_123').text(filesAmount);
        }
    });

    // Xử lý sự kiện xóa ảnh
    $(document).on('click', '.remove-btn', function(event){
        $(this).closest('.uploaded-img').remove();
    });




