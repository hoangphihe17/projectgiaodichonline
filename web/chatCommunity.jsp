<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Professional Chat Form</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
        }

        .primary-container {
            width: 315px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }

        .primary-header {
            background-color: #0084ff;
            color: #fff;
            padding: 15px;
            text-align: center;
            font-weight: bold;
        }

        .primary-box {
            height: 300px;
            overflow-y: scroll;
            padding: 10px;
        }

        .primary-message {
            background-color: #f2f2f2;
            padding: 10px;
            margin-bottom: 10px;
            border-radius: 8px;
            display: flex;
            max-width: 93%;
        }

        .primary-message .primary-avatar {
            width: 30px;
            height: 30px;
            /* border-radius: 50%; */
            overflow: hidden;
            display: inline-block;
            margin-right: 3px;
            width: -webkit-fill-available;
            max-width: 13%;

        }

        .primary-message .primary-text {
            display: inline-block;
            vertical-align: top;
        }

        /* .primary-input-container {
            padding: 10px;
            display: flex;
            align-items: center;
            background-color: #f0f0f0;
        } */
        .primary-container {
    position: fixed;
    right: 20px;
    bottom: 60px; /* Adjust this value to position the form above the trigger */
    z-index: 100; /* Ensure it's above other content */
    display: none; /* Initially hidden */
    /* Other styles remain unchanged */
}


        .primary-input-container input[type="text"] {
            flex: 1;
            padding: 8px;
            border-radius: 20px;
            border: 1px solid #ccc;
            outline: none;
        }

        .primary-input-container .button_social {
            background-color: #0084ff;
            color: #fff;
            border: none;
            padding: 8px 20px;
            border-radius: 20px;
           
            cursor: pointer;
            outline: none;
        }

        #primary-input {
            flex-grow: 1;
            padding: 10px;
            border: 2px solid #22a7f0;
            border-radius: 5px;
            color:black;
          
            
        }

        .button_social {
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            background-color: #22a7f0;
            color: white;
            font-weight: bold;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        .button_social:hover {
            background-color: #19b5fe;
        }

        #message-container {

            bottom: 70px;
            /* Adjust based on form height */
            left: 0;
            width: 100%;
            height: calc(100% - 90px);
            /* Adjust based on form height */
            overflow: hidden;
        }

        .message_social1 {
            display: block;
            margin: 0 0 5px 10px;
            background-color: #222;
            color: #fff;
            border-radius: 5px;
            padding: 10px;
            width: max-content;
            word-wrap: break-word;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
            animation: slideIn 0.5s ease forwards;
            opacity: 0;
        }

        @keyframes slideIn {
            from {
                transform: translateX(100%);
                opacity: 0;
            }

            to {
                transform: translateX(0);
                opacity: 1;
            }
        }

        @keyframes slideInAndOut {
            0% {
                transform: translateX(100%);
                opacity: 0;
            }

            10% {
                transform: translateX(0);
                opacity: 1;
            }

            90% {
                transform: translateX(0);
                opacity: 1;
            }

            100% {
                transform: translateX(-100%);
                opacity: 0;
            }
        }


        #message-container1 {
            padding: 20px;
            bottom: 60px;
            /* Adjust based on form height */
            top: 0;
            overflow-y: scroll;
        }

        .message_social {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
            border-radius: 20px;
            background-color: #f1f0f0;
            color: #333;
            padding: 10px;
            max-width: 60%;
        }

        .message_social.sent_social {
            background-color: #0084ff;
            color: #fff;
            margin-left: auto;
        }
    </style>
</head>

<body>


    <!-- Chat trigger button -->
<div class="chat-trigger" style="position: fixed; right: 20px; bottom: 80px; cursor: pointer;"><img src="assets/img/icons/misc/aviato.png"></div>



    <div id="message-container"></div>


    <form style="width: fit-content;" id="chat-form">
        <div class="primary-container">
            <div class="primary-header"> Chat community </div>
            <div class="primary-box" id="primary-box"></div>
            <div style="display: flex;" class="primary-input-container">
                <!-- id="chat-input"  -->
                <input style="width: 75%;" type="text" id="primary-input" maxlength="150" placeholder="Type your message...">
                <button class="button_social" style="width: 25%;" type="submit">Send</button>
            </div>
        </div>
    </form>

    <script>




        var messageQueue = [];
        var isMessageShowing = false;

        document.getElementById('chat-form').addEventListener('submit', function (event) {
            event.preventDefault();
            var chatInput = document.getElementById('primary-input');
            var text = chatInput.value.trim();
            if (text) {
                messageQueue.push(text);

                var message = chatInput.value;
                if (message.trim() === "") {
                    return;
                }
                var primaryBox = document.getElementById("primary-box");
                var messageElement = document.createElement("div");
                messageElement.classList.add("primary-message");
                messageElement.innerHTML = `
                    <div class="primary-avatar">
                        <img style="width:30px;" src="image/acb.png" alt="Avatar">
                    </div>
                    <div class="primary-text">`+message+`</div>`;
                primaryBox.appendChild(messageElement);
                primaryBox.scrollTop = primaryBox.scrollHeight;


                chatInput.value = '';
                if (!isMessageShowing) {
                    displayNextMessage();
                }
            }


            var messageContainer = document.getElementById('message-container1');

            event.preventDefault();
            if (text) {
                var newMessage = document.createElement('div');
                newMessage.classList.add('message_social', 'sent_social');
                newMessage.textContent = text;
                messageContainer.appendChild(newMessage);
                chatInput.value = '';
                messageContainer.scrollTop = messageContainer.scrollHeight; // Scroll to the bottom
            }






            // var messageInput = document.getElementById("primary-input");
              

        });

        function displayNextMessage() {
            if (messageQueue.length === 0) {
                isMessageShowing = false;
                return;
            }

            isMessageShowing = true;
            var messageContainer = document.getElementById('message-container');
            var newMessage = document.createElement('div');
            newMessage.classList.add('message_social1');
            newMessage.textContent = messageQueue.shift();
            messageContainer.appendChild(newMessage);

            // Ensure the message starts off-screen and scrolls into view
            var startOffset = window.innerWidth; // Start off-screen to the right
            var endOffset = -newMessage.offsetWidth; // End off-screen to the left

            // Calculate duration based on message length to ensure it scrolls at a consistent speed
            var scrollSpeed = 150; // Pixels per second
            var scrollDuration = (startOffset - endOffset) / scrollSpeed;

            // Apply the animation
            var animation = newMessage.animate([
                { transform: `translateX(${startOffset}px)` },
                { transform: `translateX(${endOffset}px)` }
            ], {
                duration: scrollDuration * 1000, // Convert to milliseconds
                easing: 'linear',
                fill: 'forwards'
            });

            animation.onfinish = function () {
                newMessage.remove();
                displayNextMessage(); // Proceed to the next message if available
            };


        }



        document.addEventListener('DOMContentLoaded', function() {
    var chatTrigger = document.querySelector('.chat-trigger');
    var chatForm = document.querySelector('.primary-container');

    chatTrigger.addEventListener('click', function() {
        // Toggle chat form visibility
        if (chatForm.style.display === 'none' || chatForm.style.display === '') {
            chatForm.style.display = 'block';
        } else {
            chatForm.style.display = 'none';
        }
    });
});





    </script>
</body>

</html>